include Necro.Util
open Necro.TypedAST
open Necro.Interpretation

let smap_diff m1 m2 = SMap.fold (fun s _ m -> SMap.remove s m) m2 m1

let smap_union = SMap.union (fun _ _ a -> Some a)

let smap_list_union = List.fold_left smap_union SMap.empty

(** Like List.map2, but don't fail if the lists don't have the same length *)
let rec zipWith f l1 l2 =
  match (l1, l2) with
  | [], _ ->
      []
  | _, [] ->
      []
  | h1 :: t1, h2 :: t2 ->
      f h1 h2 :: zipWith f t1 t2

(* Get the list of variables in a pattern *)
let pattern_variables : pattern -> typ SMap.t =
  let rec aux accu p =
    match p.pdesc with
    | PWild ->
        accu
    | PVar x ->
        SMap.add x p.ptyp accu
    | PTuple l ->
        List.fold_left aux accu l
    | PConstr (_, p) ->
        aux accu p
    | PRecord l ->
        List.fold_left (fun accu (_, p) -> aux accu p) accu l
    | POr (p, _) ->
        aux accu p
    | PType (p, _) ->
        aux accu p
  in
  aux SMap.empty

let free_variables_skel, free_variables_term =
  let interp : (typ SMap.t, typ SMap.t) interpretation =
    { var_interp=
        (fun v ->
          match v with
          | TopLevel _ ->
              SMap.empty
          | LetBound (x, ty) ->
              SMap.singleton x ty )
    ; constr_interp= (fun _ t -> t)
    ; tuple_interp= (fun l -> smap_list_union l)
    ; function_interp= (fun p s -> smap_diff s (pattern_variables p))
    ; field_interp= (fun t _ -> t)
    ; nth_interp= (fun t _ _ -> t)
    ; record_interp= (fun l -> List.map snd l |> smap_list_union)
    ; recset_interp=
        (fun t l -> List.map snd l |> smap_list_union |> smap_union t)
    ; merge_interp= (fun _ l -> smap_list_union l)
    ; match_interp=
        (fun s psl ->
          List.map (fun (pt, s) -> smap_diff s (pattern_variables pt)) psl
          |> smap_list_union |> smap_union s )
    ; letin_interp=
        (fun _ p s1 s2 -> smap_union s1 (smap_diff s2 (pattern_variables p)))
    ; exists_interp= (fun _ -> SMap.empty)
    ; return_interp= (fun t -> t)
    ; apply_interp= (fun f x -> smap_union f x) }
  in
  (interpret_skeleton interp, interpret_term interp)

let all_toplevel ss =
  let constructors =
    List.map (fun cs -> cs.cs_name) @@ Necro.Skeleton.get_all_constructors ss
  in
  let toplevel =
    List.filter_map
      (function
        | Necro.ParsedAST.TypeDecl s | TermDecl s | BinderDecl s ->
            Some s
        | Comment _ ->
            None )
      ss.ss_order
  in
  constructors @ toplevel

let used_variables_skel, used_variables_term =
  let interp : (typ SMap.t, typ SMap.t) interpretation =
    { var_interp=
        (fun v ->
          match v with
          | TopLevel _ ->
              SMap.empty
          | LetBound (x, ty) ->
              SMap.singleton x ty )
    ; constr_interp= (fun _ t -> t)
    ; tuple_interp= (fun l -> smap_list_union l)
    ; function_interp= (fun _ s -> s)
    ; field_interp= (fun t _ -> t)
    ; nth_interp= (fun t _ _ -> t)
    ; record_interp= (fun l -> List.map snd l |> smap_list_union)
    ; recset_interp=
        (fun t l -> List.map snd l |> smap_list_union |> smap_union t)
    ; merge_interp= (fun _ l -> smap_list_union l)
    ; match_interp=
        (fun s psl -> psl |> List.map snd |> smap_list_union |> smap_union s)
    ; letin_interp= (fun _ _ s1 s2 -> smap_union s1 s2)
    ; exists_interp= (fun _ -> SMap.empty)
    ; return_interp= (fun t -> t)
    ; apply_interp= (fun f x -> smap_union f x) }
  in
  (interpret_skeleton interp, interpret_term interp)

(* let with_path s = {called= Ident s; fully_qualified= Ident s} *)

let rec type_variables typ =
  match typ.tydesc with
  | Variable s ->
      SSet.singleton s
  | Arrow (typ_in, typ_out) ->
      SSet.union (type_variables typ_in) (type_variables typ_out)
  | UserType (_, typs) | Product typs ->
      typs |> List.map type_variables |> sset_list_union

let pattern_type p = p.ptyp

let rec untype_pattern p =
  let pdesc =
    match p.pdesc with
    | PWild | PVar _ ->
        p.pdesc
    | PConstr (i, p) ->
        PConstr (i, untype_pattern p)
    | PTuple ps ->
        PTuple (List.map untype_pattern ps)
    | PRecord l ->
        PRecord (List.map (fun (f, p) -> (f, untype_pattern p)) l)
    | POr (p1, p2) ->
        POr (untype_pattern p1, untype_pattern p2)
    | PType (p, _) ->
        p.pdesc
  in
  {p with pdesc}

let rec replace_in_pattern p1 name p2 =
  let aux p = replace_in_pattern p name p2 in
  match p1.pdesc with
  | PVar x when String.equal x name ->
      p2
  | PWild | PVar _ ->
      p1
  | PConstr (i, p) ->
      {p1 with pdesc= PConstr (i, aux p)}
  | PTuple ps ->
      {p1 with pdesc= PTuple (List.map aux ps)}
  | PRecord l ->
      {p1 with pdesc= PRecord (List.map (fun (f, p) -> (f, aux p)) l)}
  | POr (pl, pr) ->
      {p1 with pdesc= POr (aux pl, aux pr)}
  | PType (p, _) ->
      p

(* Fairly optimistic function at the moment *)
let flatten_matching s =
  match s.sdesc with
  | Match (matched, pss1) ->
      let pss =
        List.concat_map
          (fun (p1, s1) ->
            match s1.sdesc with
            | Match
                ({sdesc= Return {tdesc= TVar (LetBound (name2, _)); _}; _}, pss2)
              ->
                let free_vars =
                  smap_list_union
                  @@ List.map
                       (fun (p2, s2) ->
                         smap_diff (free_variables_skel s2)
                           (pattern_variables p2) )
                       pss2
                in
                if
                  (* [name2] is used and cannot be removed (we need [as] pattern) *)
                  SMap.mem name2 free_vars
                  (* The matched term is not bind by the upper match *)
                  || not (SMap.mem name2 (pattern_variables p1))
                then [(p1, s1)] (* Do not change the branch *)
                else
                  List.map
                    (fun (p2, s2) -> (replace_in_pattern p1 name2 p2, s2))
                    pss2
            | _ ->
                [(p1, s1)] )
          pss1
      in
      {s with sdesc= Match (matched, pss)}
  | _ ->
      s

let accum_to_list f set = List.of_seq @@ Seq.map f @@ SSet.to_seq set

let without_path ((x, _) : 'a with_path) : 'a = x

module Printer = struct
  let rec many pp fmt = function
    | [] ->
        ()
    | [x] ->
        pp fmt x
    | x :: xs ->
        Printf.fprintf fmt "%a, %a" pp x (many pp) xs

  let list pp fmt xs = Printf.fprintf fmt "[%a]" (many pp) xs

  let option pp fmt = function
    | None ->
        Printf.fprintf fmt "None"
    | Some elt ->
        Printf.fprintf fmt "(Some %a)" pp elt

  let string fmt s = Printf.fprintf fmt "\"%s\"" s

  let with_path pp fmt (elt, path) =
    Printf.fprintf fmt "(WithPath %a %a)" pp elt (option string) path

  let loc fmt _ = Printf.fprintf fmt "<loc>"

  let rec typ fmt ty =
    Printf.fprintf fmt "{tydesc= %a; tyloc= %a}" typ_desc ty.tydesc loc ty.tyloc

  and typ_desc fmt = function
    | Variable s ->
        Printf.fprintf fmt "(Var %s)" s
    | UserType (swp, ts) ->
        Printf.fprintf fmt "(UserType %a %a)" (with_path string) swp (list typ)
          ts
    | Arrow (t1, t2) ->
        Printf.fprintf fmt "(Arrow %a %a)" typ t1 typ t2
    | Product ts ->
        Printf.fprintf fmt "(Product %a)" (list typ) ts
end

let get_ok e h = match e with Error e -> h e | Ok a -> a

let exit_with_error e =
  Printf.fprintf stderr "Error:\n\t%s\n" e ;
  exit 1

let for_each l f =
  (Fun.flip List.map) l f (* Eta Reduction looks like a hard problem *)

let curry f x y = f (x, y)

let uncurry f (x, y) = f x y

let fold_left_i f acc l =
  fst @@ List.fold_left (fun (acc, n) x -> (f acc n x, n + 1)) (acc, 0) l

let rec split_when f l =
  match l with
  | [] ->
      ([], [])
  | hd :: _ when f hd ->
      ([], l)
  | hd :: tl ->
      let init, tail = split_when f tl in
      (hd :: init, tail)

let rec unzip l =
  match l with
  | [] ->
      ([], [])
  | (x, y) :: tl ->
      let xs, ys = unzip tl in
      (x :: xs, y :: ys)
