(** This module exports a bunch of useful functions to create Skel ASTs. It
    check the well-typing of the arguments and tries to infer types when
    possible. *)

open Necro
open TypedAST
module SUtil = Skel2am__Util

let with_path x = (x, None)

module Typ = struct
  (** Check if two types are equal up to location. *)
  let rec eql t1 t2 : bool = eql_desc t1.tydesc t2.tydesc

  and eql_desc td1 td2 : bool =
    Printf.fprintf stderr "[AST] Checking types\n[AST]   %a\n[AST]   %a\n"
      SUtil.Printer.typ_desc td1 SUtil.Printer.typ_desc td2 ;
    match (td1, td2) with
    | Variable s1, Variable s2 ->
        s1 = s2
    | UserType (swp1, ts1), UserType (swp2, ts2) ->
        swp1 = swp2
        && List.length ts1 = List.length ts2
        && List.for_all2 eql ts1 ts2
    | UserType _, _ | _, UserType _ ->
        Printf.fprintf stderr
          "[WARNING] I do not know if these two types are equal\n" ;
        true
    | Arrow (tin1, tout1), Arrow (tin2, tout2) ->
        eql tin1 tin2 && eql tout1 tout2
    | Product ts1, Product ts2 ->
        List.length ts1 = List.length ts2 && List.for_all2 eql ts1 ts2
    | _ ->
        false

  let mk tydesc = {tydesc; tyloc= Util.ghost_loc}

  let var name = mk @@ Variable name

  let user_param name param = mk @@ UserType (with_path name, param)

  let user_param' name param = mk @@ UserType (name, param)

  let user name = user_param name []

  let arrow t1 t2 = mk @@ Arrow (t1, t2)

  let product ts = match ts with [ty] -> ty | _ -> mk @@ Product ts

  let add_comp ty c =
    match ty.tydesc with
    | Product typs ->
        {ty with tydesc= Product (typs @ [c])}
    | _ ->
        {ty with tydesc= Product [ty; c]}
end

module Pat = struct
  let rec check_type (p : pattern_desc) (ty : typ) : bool =
    match (p, ty.tydesc) with
    | PWild, _ ->
        true
    | PVar _, _ ->
        true
    | PConstr (((_, path), (name1, typs1)), _), UserType (name2, typs2) ->
        (name1, path) = name2 && List.for_all2 Typ.eql typs1 typs2
    | PTuple ps, Product tys ->
        List.for_all2
          (fun p ty -> Typ.eql p.ptyp ty && check_type p.pdesc p.ptyp)
          ps tys
    | PRecord _, _ ->
        true
    | POr (p1, p2), ty ->
        check_type p1.pdesc p1.ptyp
        && check_type p2.pdesc p2.ptyp
        && Typ.eql_desc p1.ptyp.tydesc ty
        && Typ.eql_desc p2.ptyp.tydesc ty
    | PType (p, pty), ty ->
        check_type p.pdesc p.ptyp && Typ.eql_desc pty.tydesc ty
    | _ ->
        false

  exception MkError of pattern_desc * typ

  let mk (p : pattern_desc) (ty : typ) : pattern =
    if check_type p ty then {pdesc= p; ploc= Util.ghost_loc; ptyp= ty}
    else raise @@ MkError (p, ty)

  let wild typ : pattern = mk PWild typ

  let var name typ : pattern = mk (PVar name) typ

  let constr (((_, data), (type_name, type_arg)) as c) p : pattern =
    mk (PConstr (c, p)) @@ Typ.user_param' (type_name, data) type_arg

  let tuple ps : pattern =
    match ps with
    | [p] ->
        p
    | _ ->
        mk (PTuple ps) (Typ.product @@ List.map (fun p -> p.ptyp) ps)

  let or_ p1 p2 : pattern = mk (POr (p1, p2)) @@ p1.ptyp

  let typed p : pattern = mk (PType (p, p.ptyp)) p.ptyp

  let add_comp p c : pattern =
    match p.pdesc with
    | PTuple ps ->
        mk (PTuple (ps @ [c])) @@ Typ.add_comp p.ptyp c.ptyp
    | _ ->
        mk (PTuple [p; c]) @@ Typ.add_comp p.ptyp c.ptyp
end

module Term = struct
  let rec check_type (tdesc : term_desc) (ty : typ) : bool =
    match (tdesc, ty.tydesc) with
    | TVar (LetBound (_, ty1)), ty2 ->
        Typ.eql_desc ty1.tydesc ty2
    | TVar (TopLevel (_, (_, _), _, ty1)), ty2 ->
        Typ.eql_desc ty1.tydesc ty2
    | TConstr (((_, path), (name1, typs1)), _), UserType (name2, typs2) ->
        (name1, path) = name2 && List.for_all2 Typ.eql typs1 typs2
    | TTuple ts, Product tys ->
        List.for_all2
          (fun t ty -> Typ.eql t.ttyp ty && check_type t.tdesc t.ttyp)
          ts tys
    | TFunc (p, s), Arrow (ty_in, ty_out) ->
        Pat.check_type p.pdesc ty_in && check_skel s.sdesc ty_out
    | TNth (t, tys, i), ty ->
        t.ttyp.tydesc = Product tys && (List.nth tys (i - 1)).tydesc = ty
    | _ ->
        false

  and check_skel (sdesc : skeleton_desc) (ty : typ) : bool =
    match (sdesc, ty.tydesc) with
    | Branching (ty1, ss), ty2 ->
        Typ.eql_desc ty1.tydesc ty2
        && List.for_all
             (fun s -> check_skel s.sdesc ty1 && Typ.eql_desc s.styp.tydesc ty2)
             ss
    | Match (s, pss), ty ->
        check_skel s.sdesc s.styp
        && List.for_all
             (fun (p, s') ->
               Pat.check_type p.pdesc p.ptyp
               && Typ.eql p.ptyp s.styp && check_skel s.sdesc s.styp
               && Typ.eql_desc s'.styp.tydesc ty )
             pss
    | LetIn (NoBind, p, s1, s2), ty ->
        Pat.check_type p.pdesc p.ptyp
        && check_skel s1.sdesc s1.styp
        && check_skel s2.sdesc s2.styp
        && Typ.eql p.ptyp s1.styp
        && Typ.eql_desc s2.styp.tydesc ty
    | LetIn (_, _, _, _), _ ->
        (* TODO: rules for let%bind *)
        true
    | Exists typ, ty ->
        Typ.eql_desc typ.tydesc ty
    | Return t, _ ->
        check_type t.tdesc ty
    | Apply (t1, t2), _ ->
        Typ.eql_desc t1.ttyp.tydesc @@ Arrow (t2.ttyp, ty)

  exception MkError of term_desc * typ

  let mk (tdesc : term_desc) ttyp : term =
    if check_type tdesc ttyp then {tdesc; ttyp; tloc= Util.ghost_loc}
    else raise @@ MkError (tdesc, ttyp)

  let letbound v ty : term = mk (TVar (LetBound (v, ty))) ty

  let toplevel kd name tys ty : term =
    mk (TVar (TopLevel (kd, name, tys, ty))) ty

  let constr (((_, data), (type_name, type_arg)) as c) t =
    mk (TConstr (c, t)) @@ Typ.user_param' (type_name, data) type_arg

  let tuple (ts : term list) : term =
    match ts with
    | [t] ->
        t
    | _ ->
        mk (TTuple ts) (Typ.product @@ List.map (fun p -> p.ttyp) ts)

  let func (p : pattern) (s : skeleton) : term =
    mk (TFunc (p, s)) @@ Typ.arrow p.ptyp s.styp

  let add_comp t c : term =
    match (t.tdesc, t.ttyp.tydesc) with
    | TTuple ts, Product typs ->
        mk (TTuple (ts @ [c])) @@ Typ.product (typs @ [c.ttyp])
    | _ ->
        tuple [t; c]

  (** [nth t i] take the [i]th element of the tuple [t], starting at 1 (as skel
      does) *)
  let nth t i =
    match t.ttyp.tydesc with
    | Product ts ->
        mk (TNth (t, ts, i)) (List.nth ts (i - 1))
    | _ when i = 1 ->
        Printf.fprintf stderr "[AST] WARNING, Indexing Non-Tuple Term\n" ;
        t
    | _ ->
        failwith "Cannot get field of a non tuple term"
end

module Skel = struct
  exception MkError of skeleton_desc * typ

  let mk sdesc styp : skeleton =
    if Term.check_skel sdesc styp then {sdesc; styp; sloc= Util.ghost_loc}
    else raise @@ MkError (sdesc, styp)

  let empty_branch (typ : typ) : skeleton = mk (Branching (typ, [])) typ

  let branching (pss : skeleton list) : skeleton =
    match pss with
    | [] ->
        mk (Branching (Typ.product [], pss)) (Typ.product [])
    | s :: _ ->
        mk (Branching (s.styp, pss)) s.styp

  let ret t = mk (Return t) t.ttyp

  let matching' (s : skeleton) (pss : (pattern * skeleton) list) : skeleton =
    match pss with
    | [] ->
        branching []
    | _ -> (
      match pss with
      | [] ->
          mk (Match (s, pss)) @@ Typ.product []
      | (_, s0) :: _ ->
          mk (Match (s, pss)) s0.styp )

  let matching (t : term) (pss : (pattern * skeleton) list) : skeleton =
    matching' (ret t) pss

  let letin' (b : bind) (p : pattern) (s1 : skeleton) (s2 : skeleton) : skeleton
      =
    mk (LetIn (b, p, s1, s2)) s2.styp

  let letin (p : pattern) (s1 : skeleton) (s2 : skeleton) : skeleton =
    match (p.pdesc, s1.sdesc, s2.sdesc) with
    (* let _ = s1 in s2 => x *)
    | PWild, _, _ ->
        s2
    (* let x = t in x => t *)
    | PVar x, _, Return {tdesc= TVar (LetBound (y, _)); _} when x = y ->
        s1
    (* let x = y in s => s[x/y] *)
    | PVar var, Return t, _ ->
        (* replace x y s2 *)
        Substitution.replace_in_skel var t s2
    | _ ->
        letin' NoBind p s1 s2

  let apply t1 t2 =
    match t1.ttyp.tydesc with
    | Arrow (_, typ_out) ->
        mk (Apply (t1, t2)) typ_out
    | _ ->
        failwith "Cannot apply a non function type"
end
