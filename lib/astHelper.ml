open Necro
open TypedAST

let letin p s1 s2 =
  match (p.pdesc, s2) with
  | PVar x, {sdesc= Return {tdesc= TVar (LetBound (y, _)); _}; _} when x = y ->
      s1
  | _ ->
      {sloc= Util.ghost_loc; styp= s2.styp; sdesc= LetIn (NoBind, p, s1, s2)}

let typ tydesc = {tydesc; tyloc= Util.ghost_loc}

let pattern (pdesc : pattern_desc) (ptyp : typ) =
  {pdesc; ptyp; ploc= Util.ghost_loc}

let matching s pss =
  match pss with
  | [] ->
      {sloc= Util.ghost_loc; styp= typ @@ Product []; sdesc= Match (s, pss)}
  | (_, s0) :: _ ->
      {sloc= Util.ghost_loc; styp= s0.styp; sdesc= Match (s, pss)}

let apply t1 t2 =
  let[@warning "-8"] (Arrow (_, typ_out)) = t1.ttyp.tydesc in
  {sloc= Util.ghost_loc; styp= typ_out; sdesc= Apply (t1, t2)}

let return t = {sloc= Util.ghost_loc; styp= t.ttyp; sdesc= Return t}

let letbound v ty =
  {tloc= Util.ghost_loc; ttyp= ty; tdesc= TVar (LetBound (v, ty))}

let func p s =
  { tloc= Util.ghost_loc
  ; ttyp= typ @@ Arrow (Skel2am__Util.pattern_type p, s.styp)
  ; tdesc= TFunc (p, s) }

let ptype p =
  let ptyp = Skel2am__Util.pattern_type p in
  pattern (PType (p, ptyp)) ptyp

let ttuple ts =
  { tloc= Util.ghost_loc
  ; ttyp= typ @@ Product (List.map (fun t -> t.ttyp) ts)
  ; tdesc= TTuple ts }

let add_component_type ty c =
  match ty.tydesc with
  | Product typs ->
      {ty with tydesc= Product (typs @ [c])}
  | _ ->
      {ty with tydesc= Product [ty; c]}

let add_component_pattern p c =
  match p.pdesc with
  | PTuple ps ->
      {p with pdesc= PTuple (ps @ [c]); ptyp= add_component_type p.ptyp c.ptyp}
  | _ ->
      {p with pdesc= PTuple [p; c]; ptyp= add_component_type p.ptyp c.ptyp}

let add_component_term t c =
  match (t.tdesc, t.ttyp.tydesc) with
  | TTuple ts, Product typs ->
      {t with tdesc= TTuple (ts @ [c]); ttyp= typ @@ Product (typs @ [c.ttyp])}
  | _ ->
      ttuple [t; c]
