open Necro.Util

module type MONAD = sig
  type 'a t

  val fmap : ('a -> 'b) -> 'a t -> 'b t

  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t

  val return : 'a -> 'a t

  val bind : 'a t -> ('a -> 'b t) -> 'b t
end

module Identity = struct
  type 'a t = 'a

  let fmap f x = f x

  let ( <*> ) f x = f x

  let return x = x

  let bind x f = f x

  let run_identity x = x
end

module Reader (R : sig
  type t
end) =
struct
  type 'a t = R.t -> 'a

  let fmap f g r = f @@ g r

  let ( <*> ) f g r = (f r) (g r)

  let return x _ = x

  let bind m f r = f (m r) r

  let ask () r = r

  let asks f r = f r

  let local f m r = m (f r)

  let reader g = g

  let run_reader m r = m r
end

module Writer (W : sig
  type t

  val mappend : t -> t -> t

  val mempty : t
end) =
struct
  type 'a t = 'a * W.t

  let fmap f (a, w) = (f a, w)

  let ( <*> ) (f, w1) (x, w2) = (f x, W.mappend w1 w2)

  let return x = (x, W.mempty)

  let bind (x, w) f =
    let y, w' = f x in
    (y, W.mappend w w')

  let tell w = ((), w)

  let listen (a, w) = ((a, w), w)

  let writer (a, w) = (a, w)

  let run_writer (a, w) = (a, w)
end

module State (S : sig
  type t
end) =
struct
  type 'a t = S.t -> 'a * S.t

  let fmap f m s =
    let a, s' = m s in
    (f a, s')

  let ( <*> ) mf mx s =
    let f, s = mf s in
    let x, s = mx s in
    (f x, s)

  let return x s = (x, s)

  let bind w f s =
    let v, s = w s in
    f v s

  let gets f s = (f s, s)

  let get () = gets Fun.id

  let put s _s = ((), s)

  let modify f s = ((), f s)

  let run_state w s = w s
end

module Utils (M : MONAD) = struct
  include M

  module Infix = struct
    let ( <$> ) = M.fmap

    let ( let* ) = M.bind

    let ( >>= ) = M.bind

    let ( >> ) x y = x >>= fun _ -> y
  end

  open Infix

  let join mmx = mmx >>= Fun.id

  let rec fold_leftM f acc l =
    match l with
    | [] ->
        return acc
    | x :: xs ->
        let* w = f acc x in
        (fold_leftM [@tailcall]) f w xs

  let void m = m >> return ()

  let mapM f l =
    List.rev
    <$> fold_leftM (fun acc x -> f x >>= fun y -> return (y :: acc)) [] l

  let mapM_ f l = void @@ mapM f l

  let forM l f = mapM f l

  let forM_ l f = mapM_ f l

  let mmapM f m =
    SMap.fold
      (fun key v acc ->
        let* fv = f v in
        let* acc in
        return @@ SMap.add key fv acc )
      m (return SMap.empty)

  let omapM f om =
    match om with
    | Some v ->
        let* mv = f v in
        return @@ Some mv
    | None ->
        return None

  let sequence lmx = mapM Fun.id lmx

  let sequence_ lmx = void @@ sequence lmx

  let whenM b m = if b then m else return ()
end
