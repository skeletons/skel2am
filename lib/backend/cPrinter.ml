open CAST

let rec list ?(void = "") sep ty fmt l =
  match l with
  | [] ->
      Printf.fprintf fmt "%s" void
  | [elt] ->
      ty fmt elt
  | elt :: elts ->
      Printf.fprintf fmt "%a%s%a" ty elt sep (list ~void:"" sep ty) elts

let str fmt s = Printf.fprintf fmt "%s" s

let rec c_type fmt typ =
  match typ with
  | CtypeVoid ->
      Printf.fprintf fmt "void"
  | CtypeInt ->
      Printf.fprintf fmt "int"
  | CtypeChar ->
      Printf.fprintf fmt "char"
  | CtypePtr typ ->
      Printf.fprintf fmt "%a*" c_type typ
  | CtypeStruct name ->
      Printf.fprintf fmt "struct %s" name
  | CtypeEnum s ->
      Printf.fprintf fmt "enum %s" s
  | CtypeAlias s ->
      str fmt s

let const fmt c =
  match c with
  | Int n ->
      Printf.fprintf fmt "%d" n
  | Char c ->
      Printf.fprintf fmt "\'%c\'" c
  | String s ->
      Printf.fprintf fmt "\"%s\"" s

let rec expression fmt e =
  match e with
  | Const c ->
      const fmt c
  | Call (name, args) ->
      Printf.fprintf fmt "%s(%a)" name (list ", " expression) args
  | Var name ->
      str fmt name
  | Field (l, name) ->
      Printf.fprintf fmt "%a.%s" expression l name
  | PtrField (l, name) ->
      Printf.fprintf fmt "%a->%s" expression l name
  | Deref e ->
      Printf.fprintf fmt "*%a" expression e
  | Cast (typ, e) ->
      Printf.fprintf fmt "((%a) %a)" c_type typ expression e

let rec lhs fmt name =
  match name with
  | LhsVar name ->
      Printf.fprintf fmt "%s" name
  | LhsField (l, name) ->
      Printf.fprintf fmt "%a.%s" lhs l name
  | LhsPtrField (l, name) ->
      Printf.fprintf fmt "%a->%s" lhs l name

let rec case fmt (e, b) =
  Printf.fprintf fmt "case %a: {\n\t%a\n\t}" expression e block b

and statement fmt stmt =
  match stmt with
  | Decl {name; c_type= typ; value= Some value} ->
      Printf.fprintf fmt "%a %s = %a;" c_type typ name expression value
  | Decl {name; c_type= typ; value= None} ->
      Printf.fprintf fmt "%a %s;" c_type typ name
  | Assign (name, value) ->
      Printf.fprintf fmt "%a = %a;" lhs name expression value
  | Switch (e, cs) ->
      Printf.fprintf fmt "switch (%a) {\n\t%a\n}\n" expression e
        (list "\n\t" case) cs
  | Return (Some value) ->
      Printf.fprintf fmt "return %a;" expression value
  | Return None ->
      Printf.fprintf fmt "return;"

and block fmt = list "\n\t" statement fmt

let parameter fmt (typ, name) = Printf.fprintf fmt "%a %s" c_type typ name

let fun_def fmt fd =
  Printf.fprintf fmt "\n%a %s(%a) {\n\t%a\n}\n" c_type fd.c_type fd.name
    (list ~void:"void" ", " parameter)
    fd.parameters (list "\n\t" statement) fd.body

let fun_decl fmt (fd : fun_decl) =
  Printf.fprintf fmt "%a %s(%a);" c_type fd.c_type fd.name (list ", " c_type)
    fd.parameters

let struct_field fmt (typ, name) = Printf.fprintf fmt "%a %s;" c_type typ name

let struct_def fmt (sd : struct_def) =
  Printf.fprintf fmt "struct %s {\n\t%a\n};\n" sd.name
    (list "\n\t" struct_field) sd.fields

let enum_def fmt (ed : enum_def) =
  Printf.fprintf fmt "enum %s {\n\t%a\n};\n" ed.name (list ",\n\t" str)
    ed.fields

let toplevel_declaration fmt tld =
  match tld with
  | FunDef fd ->
      fun_def fmt fd
  | FunDecl fd ->
      fun_decl fmt fd
  | StructDef sd ->
      struct_def fmt sd
  | StructDecl s ->
      Printf.fprintf fmt "struct %s;\n" s
  | EnumDef ed ->
      enum_def fmt ed
  | TypeDef (typ, alias) ->
      Printf.fprintf fmt "typedef %a %s;\n" c_type typ alias
  | PPDirective s ->
      Printf.fprintf fmt "%s\n" s

let program = list "\n" toplevel_declaration
