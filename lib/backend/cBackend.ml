open Necro
open TypedAST
open Util
open CAST

module R = struct
  type t = skeletal_semantics
end

module ReaderR = Monad.Reader (R)

open Monad.Utils (ReaderR)

open ReaderR
open Infix
(* open R *)

let void_ptr = CtypePtr CtypeVoid

let skelify name = "skel_" ^ name

let constructor_input_types typ =
  match typ.tydesc with Product typs -> typs | _ -> [typ]

let struct_from_constructor tag_type_name name cs =
  let struct_name = skelify (name ^ cs.cs_name) in
  let parameters =
    List.mapi (fun n _ -> (void_ptr, "data" ^ string_of_int n))
    @@ constructor_input_types cs.cs_input_type
  in
  let struct_type = CtypeStruct struct_name in
  [ StructDef
      {name= struct_name; fields= (CtypeEnum tag_type_name, "tag") :: parameters}
  ; FunDef
      { name= "mk_" ^ struct_name
      ; c_type= CtypePtr struct_type
      ; parameters
      ; body=
          [ Decl
              { name= "res"
              ; c_type= CtypePtr struct_type
              ; value=
                  Some
                    (Call
                       ( "malloc"
                       , [Call ("sizeof", [Var ("struct " ^ struct_name)])] ) )
              }
          ; Assign (LhsPtrField (LhsVar "res", "tag"), Var (skelify cs.cs_name))
          ]
          @ List.map
              (fun (_, name) ->
                Assign (LhsPtrField (LhsVar "res", name), Var name) )
              parameters
          @ [Return (Option.some @@ Var "res")] } ]

let compile_variant_type (name, css) =
  let tag_type_name = skelify @@ name in
  let enum_def =
    EnumDef
      {name= tag_type_name; fields= List.map (fun cs -> skelify cs.cs_name) css}
  in
  return
  @@ enum_def
     :: List.concat_map (struct_from_constructor tag_type_name name) css

let make_prototype (name, typ) =
  let arity =
    match typ.tydesc with Product typs -> List.length typs | _ -> 1
  in
  return
  @@ FunDecl
       { name= skelify name
       ; c_type= void_ptr
       ; parameters= List.init arity (fun _ -> void_ptr) }

let rec compile_argument td =
  match td with
  | TVar (LetBound (name, _)) ->
      return @@ Var name
  | TConstr (((called, _), (typ_name, _)), arg) ->
      let* args = compile_arguments arg in
      let constr_name = "mk_" ^ skelify typ_name ^ called in
      return @@ Call (constr_name, args)
  | TVar (TopLevel _) ->
      (* Top level constant *)
      return @@ Var "NULL"
  | _ ->
      Printf.fprintf stderr "Cannot compile argument %s\n"
        (Printer.string_of_term ~indent:0
           {tdesc= td; ttyp= AstHelper.typ @@ Product []; tloc= Util.ghost_loc} ) ;
      exit 1

and compile_arguments t =
  match t.tdesc with
  | TTuple ts ->
      mapM (fun t -> compile_argument t.tdesc) ts
  | _ ->
      (* How can this function be missing from stdlib ? *)
      (fun x -> [x]) <$> compile_argument t.tdesc

let rec compile_skeleton s =
  match s.sdesc with
  | Apply ({tdesc= TVar (TopLevel (_, (called, _), _, _)); _}, arg) ->
      let* args = compile_arguments arg in
      let function_name = skelify @@ called in
      return @@ [Return (Option.some @@ Call (function_name, args))]
  | Match
      ( { sdesc=
            Return
              { tdesc=
                  TVar (LetBound (name, {tydesc= UserType ((called, _), _); _}))
              ; _ }
        ; _ }
      , pss ) ->
      let enum_type = CtypeEnum (skelify called) in
      let* cases =
        mapM
          (function
            | ( { pdesc= PConstr (((called, _), (typ, _)), {pdesc= PTuple ps; _})
                ; _ }
              , s ) ->
                let[@warning "-8"] args =
                  List.map (fun {pdesc= PVar name; _} -> name) ps
                in
                let* s = compile_skeleton s in
                let constr_name = called in
                let struct_name = skelify @@ typ ^ constr_name in
                return
                  ( Var (skelify constr_name)
                  , List.mapi
                      (fun n var_name ->
                        Decl
                          { name= var_name
                          ; c_type= void_ptr
                          ; value=
                              Option.some
                              @@ PtrField
                                   ( Cast
                                       ( CtypePtr (CtypeStruct struct_name)
                                       , Var name )
                                   , "data" ^ string_of_int n ) } )
                      args
                    @ s )
            | _ ->
                failwith "Pattern is not a constructor" )
          pss
      in
      return [Switch (Deref (Cast (CtypePtr enum_type, Var name)), cases)]
  | Return {tdesc= TVar (LetBound (name, _)); _} ->
      return [Return (Some (Var name))]
  | Return {tdesc= TConstr (((called, _), (typ_name, _)), arg); _} ->
      let* args = compile_arguments arg in
      let constr_name = "mk_" ^ skelify @@ typ_name ^ called in
      return @@ [Return (Option.some @@ Call (constr_name, args))]
  | LetIn
      ( _
      , {pdesc= PVar name; _}
      , { sdesc= Apply ({tdesc= TVar (TopLevel (_, (called, _), _, _)); _}, args)
        ; _ }
      , s2 ) ->
      let* arguments = compile_arguments args in
      let function_name = skelify called in
      let* body = compile_skeleton s2 in
      return
      @@ Decl
           { name
           ; c_type= void_ptr
           ; value= Some (Call (function_name, arguments)) }
         :: body
  | LetIn
      ( _
      , {pdesc= PConstr (((pcalled, _), (typ, _)), {pdesc= PTuple ps; _}); _}
      , { sdesc= Apply ({tdesc= TVar (TopLevel (_, (called, _), _, _)); _}, args)
        ; _ }
      , s2 ) ->
      let constr_name = pcalled in
      let struct_name = skelify @@ typ ^ constr_name in
      let[@warning "-8"] pargs =
        List.map (fun {pdesc= PVar name; _} -> name) ps
      in
      let l =
        List.mapi
          (fun n var_name ->
            Decl
              { name= var_name
              ; c_type= void_ptr
              ; value=
                  Option.some
                  @@ PtrField
                       ( Cast (CtypePtr (CtypeStruct struct_name), Var "tmp")
                       , "data" ^ string_of_int n ) } )
          pargs
      in
      let* arguments = compile_arguments args in
      let function_name = skelify called in
      let* body = compile_skeleton s2 in
      return
      @@ Decl
           { name= "tmp"
           ; c_type= void_ptr
           ; value= Some (Call (function_name, arguments)) }
         :: l
      @ body
  | _ ->
      return []

let compile_pattern_parameter p =
  match p.pdesc with
  | PType ({pdesc= PVar name; _}, _) ->
      (void_ptr, name)
  | _ ->
      Printf.fprintf stderr "Cannot compile pattern (%s) to C\n"
        (Printer.string_of_pattern p) ;
      exit 1

let compile_specified_function (name, p, s) =
  let parameters =
    match p.pdesc with
    | PTuple ps ->
        List.map compile_pattern_parameter ps
    | _ ->
        [compile_pattern_parameter p]
  in
  let* body = compile_skeleton s in
  return @@ FunDef {name= skelify name; c_type= void_ptr; parameters; body}

let compile_ss () =
  let* ss = ask () in
  let variant_types =
    SMap.fold
      (fun name (_, td) l ->
        match td with TDVariant (_, css) -> (name, css) :: l | _ -> l )
      ss.ss_types []
  in
  let all_functions =
    SMap.fold
      (fun name (_, (_, typ, t_opt)) l ->
        match (typ.tydesc, t_opt) with
        | Arrow (typ_in, _), _ ->
            (name, typ_in) :: l
        | _ ->
            l )
      ss.ss_terms []
  in
  let specified_functions =
    SMap.fold
      (fun name (_, (_, typ, t_opt)) l ->
        match (typ.tydesc, t_opt) with
        | Arrow _, Some {tdesc= TFunc (p, s); _} ->
            (name, p, s) :: l
        | _ ->
            l )
      ss.ss_terms []
  in
  let* c_variant_types =
    List.concat <$> mapM compile_variant_type variant_types
  in
  let* c_specified_functions =
    mapM compile_specified_function specified_functions
  in
  let* c_prototypes = mapM make_prototype all_functions in
  return
  @@ (PPDirective "#include <stdlib.h>" :: c_variant_types)
  @ c_prototypes @ c_specified_functions

let compile ss = run_reader (compile_ss ()) ss
