type c_type =
  | CtypeVoid
  | CtypeInt
  | CtypeChar
  | CtypePtr of c_type
  | CtypeStruct of string
  | CtypeEnum of string
  | CtypeAlias of string

type const = Int of int | Char of char | String of string

type expr =
  | Const of const
  | Call of string * expr list
  | Field of expr * string
  | PtrField of expr * string
  | Deref of expr
  | Var of string
  | Cast of c_type * expr

type lhs = LhsVar of string | LhsField of lhs * string | LhsPtrField of lhs * string

type case = expr * block

and statement =
  | Decl of {name: string; c_type: c_type; value: expr option}
  | Assign of lhs * expr
  | Switch of expr * case list
  | Return of expr option

and block = statement list

type struct_decl = string

type struct_def = {name: string; fields: (c_type * string) list}

type enum_def = {name: string; fields: string list}

type fun_decl = {name: string; c_type: c_type; parameters: c_type list}

type fun_def =
  {name: string; c_type: c_type; parameters: (c_type * string) list; body: block}

type toplevel_declaration =
  | FunDef of fun_def
  | FunDecl of fun_decl
  | StructDef of struct_def
  | StructDecl of struct_decl
  | EnumDef of enum_def
  | TypeDef of c_type * string
  | PPDirective of string

type program = toplevel_declaration list
