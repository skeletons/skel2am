(** Defunctionalize A Skel Program. *)

(* For now, we assume that the program is a
   cps transformed natural semantics *)

module SUtil = Skel2am__Util
open Necro.TypedAST
open Necro
open Util
open Ast

(** Used to differentiate function domains Store the types of toplevel functions
    (arrow types). As different types will result in different ADT, we provide a
    [compare] function to instanciate the [SMap.Make] functor. *)
module FuncType = struct
  (** An arrow type [typ_in -> typ_out] *)
  type t = {typ_in: typ; typ_out: typ}

  (* TODO: Need a better compare functions here *)
  (* Stdlib.compare is broken *)
  let compare ft1 ft2 =
    let t1 = Typ.arrow ft1.typ_in ft1.typ_out in
    let t2 = Typ.arrow ft2.typ_in ft2.typ_out in
    Stdlib.compare (Printer.string_of_type t1) (Printer.string_of_type t2)

  let ty_vars ft =
    SUtil.accum_to_list Typ.var
    @@ SUtil.type_variables
    @@ Typ.arrow ft.typ_in ft.typ_out
end

(** State of the mapper. Used to collect the closures, sorted by types. Each
    [FuncType.t] is mapped to a list of construtors, One for each function
    encountered during the mapping. *)
module FuncMap = struct
  (** Data constructor, represent a function of type [FuncType.t]. *)
  type constr =
    { c_pattern: pattern  (** Argument of the function *)
    ; c_body: skeleton  (** Body of the function *)
    ; c_free_vars: typ Util.SMap.t
          (** Free variables of the function, with their types *)
    ; c_name: string  (** Name of the constructor, must be unique. *) }

  (** A [defunction] is a defunctionalized function. Each defunction will
      produce an ADT with one constructor per anonymous function of its type*)
  type defunction =
    { df_name: string  (** Name of the defunction (ie. the resulting type) *)
    ; df_apply: string  (** Name of the associated apply function *)
    ; df_constructors: constr list  (** List of constructors *) }

  (** Map from types to defunctions *)
  module FMap = Map.Make (FuncType)

  (** State of the monad, [counter] is used to generate fresh names *)
  type t = {func_map: defunction FMap.t; counter: int}

  (* getters *)
  let func_map t = t.func_map

  let counter t = t.counter

  let incr t = {t with counter= 1 + t.counter}

  let initial ss =
    ignore ss ;
    {func_map= FMap.empty; counter= 0}
end

(* Create the State Monad and import useful functions *)
module StateFuncMap = Monad.State (FuncMap)

open Monad.Utils (StateFuncMap)

open Infix
open StateFuncMap
open FuncType
open FuncMap

(* Create a mapper wrapped in the State Monad *)
module DefunMapper = Mapper.Make (StateFuncMap)
open DefunMapper

(** Return a fresh integer *)
let incr_and_get () = modify incr >> gets counter

(** Add a new defunction to the state *)
let add_defunction func_type defunction =
  modify @@ fun s -> {s with func_map= FMap.add func_type defunction s.func_map}

(** [fresh_defunction ()] builds a new defunction with fresh names for [df_name]
    and [df_apply]. *)
let fresh_defunction () =
  let* c = incr_and_get () in
  let df_name = Printf.sprintf "arrow_%d" c in
  let df_apply = Printf.sprintf "apply_%d" c in
  return {df_name; df_apply; df_constructors= []}

(** [create_defunction func_type] builds a new defunction of type [func_type],
    adds it to the state and return it. *)
let create_defunction func_type =
  let* defunction = fresh_defunction () in
  let* () = add_defunction func_type defunction in
  return defunction

(** Return the defunction associated with [func_type]. If no such defunction
    exists, create an empty one and return it *)
let get_defunction func_type =
  let* s = get () in
  match FMap.find_opt func_type s.func_map with
  | Some defunction ->
      return defunction
  | None ->
      create_defunction func_type

let fresh_constr c_free_vars c_pattern c_body =
  let* c = incr_and_get () in
  let c_name = Printf.sprintf "Constr%d" c in
  return {c_pattern; c_body; c_free_vars; c_name}

(** [tell c_free_vars c_pattern c_body func_type] must be called when an
    anonymous function is found. The associated constructor will be created and
    returned. *)
let tell free_vars pattern body func_type =
  let* constr = fresh_constr free_vars pattern body in
  let* defunction = get_defunction func_type in
  let defunction =
    {defunction with df_constructors= constr :: defunction.df_constructors}
  in
  let* () = add_defunction func_type defunction in
  (* Get the type variables of [func_type] to build the skel constructor *)
  let ty_vars = FuncType.ty_vars func_type in
  return (with_path constr.c_name, (defunction.df_name, ty_vars))

(* == Definition of the mapper functions == *)

(** Remove the arrows of a type by adding entries to the state. *)
let typ this typ =
  match typ.tydesc with
  | Arrow (typ_in, typ_out) ->
      let* typ_in = this.typ this typ_in in
      let* typ_out = this.typ this typ_out in
      let func_type = {typ_in; typ_out} in
      let* defunction = get_defunction func_type in
      let ty_vars = FuncType.ty_vars func_type in
      return @@ Typ.user_param defunction.df_name ty_vars
  | _ ->
      default.typ this typ

(** Translate functions to constructors *)
let term this term =
  match (term.tdesc, term.ttyp.tydesc) with
  (* \x -> e => Lambda (free e \ {x}) *)
  | TFunc (parameter, body), Arrow (typ_in, typ_out) ->
      let* typ_in = this.typ this typ_in in
      let* typ_out = this.typ this typ_out in
      let func_type = {typ_in; typ_out} in
      let* parameter = this.pattern this parameter in
      let* body = this.skeleton this body in
      let free_vars = SUtil.free_variables_term @@ Term.func parameter body in
      (* Create the constructor *)
      let* c = tell free_vars parameter body func_type in
      let vars =
        List.map (fun (key, typ) -> Term.letbound key typ)
        @@ SMap.bindings free_vars
      in
      return @@ Term.constr c @@ Term.tuple vars
  | TVar (TopLevel (Nonspecified, _, _, _)), _ ->
      return term
  | TVar (TopLevel (Specified, name, tys, _)), Arrow (tin, tout) ->
      (* Update the type of the specified toplevel functions (we must keep the topmost arrow) *)
      let* tys = mapM (this.typ this) tys in
      let* tin = this.typ this tin in
      let* tout = this.typ this tout in
      return @@ Term.toplevel Specified name tys @@ Typ.arrow tin tout
  | _ ->
      default.term this term

(** Translate application to apply-call *)
let skeleton this s =
  match s.sdesc with
  (* f arg => apply (f, arg) *)
  | Apply
      ( ( { tdesc= TVar (LetBound _)
          ; ttyp= {tydesc= Arrow (typ_in, typ_out); _}
          ; _ } as t1 )
      , t2 ) ->
      let* t1 = this.term this t1 in
      let* t2 = this.term this t2 in
      let func_type = {typ_in; typ_out} in
      let* defunction = get_defunction func_type in
      let ty_vars = FuncType.ty_vars func_type in
      let apply_type = Typ.arrow (Typ.product [t1.ttyp; t2.ttyp]) typ_out in
      let apply =
        Term.toplevel Specified
          (with_path defunction.df_apply)
          ty_vars apply_type
      in
      return @@ Skel.apply apply @@ Term.tuple [t1; t2]
  | LetIn ((TermBind (Specified, _, _, _) as tb), p, s1, s2) ->
      let* s2 = this.skeleton this s2 in
      return @@ Skel.letin' tb p s1 s2
  | _ ->
      default.skeleton this s

(** Used to cancel the translation of the toplevel arrows *)
let skipper_term_definition mapper this (c, td) =
  match td with
  | ty_args, {tydesc= Arrow (typ_in, typ_out); _}, top ->
      let flag_opt = Option.map String.trim c in
      if
        List.mem flag_opt
          [ Some "__return__"
          ; Some "__bind__"
          ; Some "__effect__"
          ; Some "__atomic__" ]
        || top = None
      then return (c, td)
      else
        let* typ_in = mapper.typ mapper typ_in in
        let* typ_out = mapper.typ mapper typ_out in
        default.term_definition this
          (c, (ty_args, Typ.arrow typ_in typ_out, top))
  | _ ->
      default.term_definition this (c, td)

let skipper_term_desc mapper this td =
  match td with
  | TFunc (_, _) ->
      default.term_desc mapper td
  | _ ->
      default.term_desc this td

let function_skipper mapper =
  { default with
    term_definition= skipper_term_definition mapper
  ; term_desc= skipper_term_desc mapper }

let defun_mapper = function_skipper {default with term; skeleton; typ}

(* == Main translation functions == *)
let produce_types defunction ty_vars ss =
  let constructors =
    List.map
      (fun constr ->
        { cs_name= constr.c_name
        ; cs_type_args= ty_vars
        ; cs_input_type=
            Typ.product @@ List.map snd @@ SMap.bindings constr.c_free_vars
        ; cs_variant_type= defunction.df_name
        ; cs_comment= None } )
      defunction.df_constructors
  in
  SMap.add defunction.df_name
    (None, TDVariant (ty_vars, constructors))
    ss.ss_types

let produce_alternatives defunction ty_vars =
  defunction.df_constructors
  |> List.map (fun constr ->
         let p =
           Pat.tuple
             [ Pat.constr
                 ( with_path constr.c_name
                 , (defunction.df_name, List.map Typ.var ty_vars) )
               @@ Pat.tuple
                    ( List.map (fun (var, typ) -> Pat.var var typ)
                    @@ SMap.bindings @@ constr.c_free_vars )
             ; constr.c_pattern ]
         in
         let s = constr.c_body in
         (p, s) )

let produce_terms func_type defunction ty_vars ss =
  let type_name =
    Typ.user_param defunction.df_name @@ List.map Typ.var ty_vars
  in
  let apply_type =
    Typ.arrow (Typ.product [type_name; func_type.typ_in]) func_type.typ_out
  in
  let alternatives = produce_alternatives defunction ty_vars in
  SMap.add defunction.df_apply
    ( None
    , ( ty_vars
      , apply_type
      , Option.some
        @@ Term.func
             ( Pat.typed
             @@ Pat.mk (PVar "fun_arg")
                  (Typ.product [type_name; func_type.typ_in]) )
             (Skel.matching
                (Term.letbound "fun_arg"
                   (Typ.product [type_name; func_type.typ_in]) )
                alternatives ) ) )
    ss.ss_terms

let produce_defunction func_type defunction ss =
  let ty_vars =
    SSet.to_list @@ SUtil.type_variables
    @@ Typ.arrow func_type.typ_in func_type.typ_out
  in
  let ss_types = produce_types defunction ty_vars ss in
  let ss_terms = produce_terms func_type defunction ty_vars ss in
  { ss with
    ss_types
  ; ss_terms
  ; ss_order=
      ParsedAST.TypeDecl defunction.df_name
      :: ParsedAST.TermDecl defunction.df_apply :: ss.ss_order }

(** Add all the defuntions in [func_map] to the skeletal semantics [ss] *)
let expand_ss ss func_map = FMap.fold produce_defunction func_map ss

(** Defun a Skeletal Semantics *)
let defun_ss ss =
  let ss, state = run_state (run defun_mapper ss) @@ initial ss in
  expand_ss ss state.func_map
