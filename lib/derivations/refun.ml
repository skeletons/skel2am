(* Find all matchs, sort by type of the matched skeleton. *)
(* Keep the types that appear only once *)
(* each branch of the match give one function *)

open Necro
open TypedAST
open Util

(** [rename name var s] replaces all occurences of [name] by [var] in [s],
    variables of [var] should not conflict with variables of s *)
let rename name var s =
  let open Mapper.Default in
  let skeleton_desc this sd =
    match sd with
    | LetIn (b, p, s1, s2) ->
        let vars_p = Skel2am__Util.pattern_variables p in
        if SMap.mem name vars_p then LetIn (b, p, this.skeleton this s1, s2)
        else LetIn (b, p, this.skeleton this s1, this.skeleton this s2)
    | Match (s, pss) ->
        let s = this.skeleton this s in
        let pss =
          List.map
            (fun (p, s) ->
              let vars_p = Skel2am__Util.pattern_variables p in
              let s =
                if SMap.mem name vars_p then s else this.skeleton this s
              in
              (p, s) )
            pss
        in
        Match (s, pss)
    | _ ->
        default.skeleton_desc this sd
  in
  let term this t =
    match t.tdesc with
    | TFunc (p, s) ->
        let vars_p = Skel2am__Util.pattern_variables p in
        if SMap.mem name vars_p then t
        else
          let tdesc = TFunc (p, this.skeleton this s) in
          {t with tdesc}
    | TVar (LetBound (n, _)) when n = name ->
        var
    | _ ->
        default.term this t
  in
  let mapper = {default_mapper with skeleton_desc; term} in
  mapper.skeleton mapper s

module Collector = struct
  module W = struct
    type info =
      { return_type: typ
      ; free_vars: (string * typ) list
      ; constructors: (string list * skeleton) SMap.t }

    type t = info Option.t SMap.t

    let mempty = SMap.empty

    let mappend useless argument =
      (* OCaml is such a beautiful language *)
      SMap.union
        (fun _ x y ->
          match (x, y) with
          | Some _, None ->
              Some x
          | None, Some _ ->
              Some y
          | _ ->
              Some None )
        useless argument
  end

  module WriterW = Monad.Writer (W)
  include WriterW
  include W
  include Monad.Utils (WriterW)
  include Infix
  module WriterMapper = Mapper.Make (WriterW)
  include WriterMapper

  let skeleton this s =
    match s.sdesc with
    | Match (s1, pss) ->
        let typ_s = s1.styp in
        let* () =
          match typ_s.tydesc with
          | UserType ((called, _), _) ->
              let free_vars =
                SMap.bindings
                @@ List.fold_left
                     (fun acc (p, s) ->
                       Skel2am__Util.(
                         smap_union acc
                         @@ smap_diff (free_variables_skel s)
                              (pattern_variables p) ) )
                     SMap.empty pss
              in
              let[@warning "-8"] constructors =
                SMap.of_seq
                @@ Seq.map
                     (fun ({pdesc= PConstr (((called, _), _), p); _}, s) ->
                       ( called
                       , ( List.map fst @@ SMap.bindings
                           @@ Skel2am__Util.pattern_variables p
                         , s ) ) )
                @@ List.to_seq pss
              in
              tell
              @@ SMap.singleton called
                   (Some {return_type= s.styp; free_vars; constructors})
          | _ ->
              return ()
        in
        default.skeleton this s
    | LetIn (_, {pdesc= PConstr (((constr_name, _), _), p); _}, s1, s2) ->
        let typ_s = s1.styp in
        let* () =
          match typ_s.tydesc with
          | UserType ((called, _), _) ->
              let free_vars =
                SMap.bindings
                @@ Skel2am__Util.(
                     smap_diff (free_variables_skel s2) (pattern_variables p) )
              in
              let[@warning "-8"] constructors =
                SMap.singleton constr_name
                  ( List.map fst @@ SMap.bindings
                    @@ Skel2am__Util.pattern_variables p
                  , s2 )
              in
              tell
              @@ SMap.singleton called
                   (Some {return_type= s.styp; free_vars; constructors})
          | _ ->
              return ()
        in
        default.skeleton this s
    | _ ->
        default.skeleton this s

  let collect ss = run_writer @@ run {default with skeleton} ss
end

module RemoveApply = struct
  let collect_apply ss =
    SMap.fold
      (fun name (_, (_, _, t)) acc ->
        match t with
        | Some
            { tdesc=
                TFunc
                  ( p
                  , ( {sdesc= Apply ({tdesc= TVar (LetBound _); _}, _); _} as
                      body ) )
            ; _ } ->
            let[@warning "-8"] vars =
              match p.pdesc with
              | PType ({pdesc= PVar x; _}, _) ->
                  [x]
              | PTuple ps ->
                  List.map
                    (fun {pdesc= PType ({pdesc= PVar x; _}, _); _} -> x)
                    ps
            in
            SMap.add name (vars, body) acc
        | _ ->
            acc )
      ss.ss_terms SMap.empty

  open Mapper.Default

  let skeleton r this s =
    match s.sdesc with
    | Apply
        ( {tdesc= TVar (TopLevel (_, (called, _), _, _)); _}
        , {tdesc= TTuple args; _} )
      when SMap.mem called r ->
        let vars, body = SMap.find called r in
        List.fold_left2 (fun body t var -> rename var t body) body args vars
    | _ ->
        default.skeleton this s

  let remove_apply ss =
    let ts = collect_apply ss in
    let ss = run {default_mapper with skeleton= skeleton ts} ss in
    { ss with
      ss_order=
        List.filter
          (fun dk ->
            match dk with
            | ParsedAST.TermDecl name when SMap.mem name ts ->
                false
            | _ ->
                true )
          ss.ss_order
    ; ss_terms= SMap.fold (fun s _ ss -> SMap.remove s ss) ts ss.ss_terms }
end

open Collector
open RemoveApply

module R = struct
  type t = info SMap.t
end

module ReaderR = Monad.Reader (R)

open Monad.Utils (ReaderR)

open ReaderR
open Infix
(* open R *)

module ReaderMapper = Mapper.Make (ReaderR)
open ReaderMapper

let skeleton this s =
  match s.sdesc with
  | Match ({styp= {tydesc= UserType ((called, _), _); _}; sdesc= Return t; _}, _)
    -> (
      let* r = ask () in
      let type_name = called in
      match SMap.find_opt type_name r with
      | None ->
          default.skeleton this s
      | Some info ->
          let* t = this.term this t in
          let* styp = this.typ this s.styp in
          let arg =
            match info.free_vars with
            | [] ->
                { tdesc= TTuple []
                ; ttyp= AstHelper.typ @@ Product []
                ; tloc= Util.ghost_loc }
            | [(x, typ)] ->
                AstHelper.letbound x typ
            | _ ->
                { tdesc=
                    TTuple
                      (List.map
                         (fun (x, ty) -> AstHelper.letbound x ty)
                         info.free_vars )
                ; ttyp= AstHelper.typ @@ Product []
                ; tloc= Util.ghost_loc }
          in
          return {s with sdesc= Apply (t, arg); styp} )
  | _ ->
      default.skeleton this s

let fresh =
  let n = ref 0 in
  fun () ->
    incr n ;
    Printf.sprintf "arg%d" !n

let term_desc this t =
  let* r = ask () in
  match t with
  | TConstr (((called, _), (type_name, _)), {tdesc= TTuple arg; _}) -> (
    match SMap.find_opt type_name r with
    | None ->
        default.term_desc this t
    | Some info ->
        let match_arg, body = SMap.find called info.constructors in
        let body =
          List.fold_left
            (fun s (x, t) -> rename x t s)
            body
            (List.map2 (fun x y -> (x, y)) match_arg arg)
        in
        let args = List.map (fun _ -> fresh ()) info.free_vars in
        let* body =
          this.skeleton this
          @@ List.fold_left
               (fun s (x, t) -> rename x t s)
               body
               (List.map2
                  (fun (x, ty) y -> (x, AstHelper.letbound y ty))
                  info.free_vars args )
        in
        let pattern =
          match info.free_vars with
          | [(_, ty)] ->
              AstHelper.pattern (PVar (List.hd args)) ty
          | _ ->
              AstHelper.pattern
                (PTuple
                   (List.map2
                      (fun x ptyp -> AstHelper.pattern (PVar x) ptyp)
                      args
                      (List.map snd info.free_vars) ) )
                (AstHelper.typ @@ Product (List.map snd info.free_vars))
        in
        return @@ TFunc (pattern, body) )
  | _ ->
      default.term_desc this t

let get_typ_in info =
  match info.free_vars with
  | [] ->
      AstHelper.typ @@ Product []
  | [(_, ty)] ->
      ty
  | _ ->
      AstHelper.typ @@ Product (List.map snd info.free_vars)

let typ this t =
  match t.tydesc with
  | UserType ((called, _), _) -> (
      let* r = ask () in
      let type_name = called in
      match SMap.find_opt type_name r with
      | None ->
          default.typ this t
      | Some info ->
          (* This can cause an infinite loop if the set of types is
             not refunctionalizable *)
          let typ_in = get_typ_in info in
          let typ_out = info.return_type in
          let* tydesc = this.typ_desc this (Arrow (typ_in, typ_out)) in
          return @@ AstHelper.typ tydesc )
  | _ ->
      default.typ this t

let refun ss ts =
  let mapper = {default with skeleton; term_desc; typ} in
  run_reader (run mapper ss) ts

let refun_ss ss =
  let ss, ts = collect ss in
  let ts = SMap.filter_map (Fun.flip Fun.const) ts in
  let ts =
    SMap.filter
      (fun s _ ->
        let c, _ = SMap.find s ss.ss_types in
        match c with
        | Some s when String.trim s = "__protected__" ->
            false
        | _ ->
            true )
      ts
  in
  let ss = refun ss ts in
  let ss =
    SMap.fold
      (fun s _ ss ->
        { ss with
          ss_order=
            List.filter
              (fun dk ->
                match dk with
                | ParsedAST.TypeDecl name when name = s ->
                    false
                | _ ->
                    true )
              ss.ss_order
        ; ss_types= SMap.remove s ss.ss_types } )
      ts ss
  in
  remove_apply ss
