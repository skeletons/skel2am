(** This derivation removes all complex pattern constructs, such as field
    matching, nested tuples in function arguments, first order tuples. It also
    expands pattern matching to match on a single constructor at a time. *)

(* TODO: make this statement true *)

open Necro
open TypedAST
open Util
open Ast
module SUtil = Skel2am__Util

(** Dirty for the moment *)
let counter =
  let c = ref 0 in
  fun () ->
    incr c ;
    !c

(** Find the default branch of a match *)
let rec default_branch pss typ =
  match pss with
  | [] ->
      (Skel.empty_branch typ, [])
  | ({pdesc= PWild; _}, s) :: _ ->
      (s, [])
  | ps :: pss ->
      let default, pss = default_branch pss typ in
      (default, ps :: pss)

(** [rename var term s] replaces all occurences of the variable [var] by the
    term [term] in the skeleton [s], variables of [term] should not conflict
    with variables of [s] *)
let rename var term s = Substitution.replace_in_skel var term s

type pattern_group = (pattern list * skeleton) list

(** [sort_by_constructor psss] break down the pattern_group [psss] into
    sub-groups where each element come from the same constructor match. The
    sub-patterns of the constructor are prepended to the list of pattern to
    match. *)
let sort_by_constructor psss =
  let add k (p, s) acc =
    SMap.update k
      (fun v ->
        (* We insert at the end of the list to ensure sort stability *)
        Option.some
        @@ Option.fold ~none:[(p, s)] ~some:(fun pg -> pg @ [(p, s)]) v )
      acc
  in
  SMap.bindings
  @@ List.fold_left
       (fun acc (ps, s) ->
         match ps with
         | {pdesc= PConstr (((c_name, _), _), p); _} :: ps' ->
             add c_name
               ((match p.pdesc with PTuple ps -> ps | _ -> [p]) @ ps', s)
               acc
         | _ ->
             acc )
       SMap.empty psss

let only_variables =
  List.for_all (function {pdesc= PVar _ | PWild; _} -> true | _ -> false)

let only_constructors =
  List.for_all (function {pdesc= PConstr (_, _); _} -> true | _ -> false)

let matching_tuples = function {tydesc= Product _; _} -> true | _ -> false

let extract_homogeneous_kind psss =
  let is_var = function {pdesc= PVar _ | PWild; _} -> true | _ -> false in
  let is_constr = function {pdesc= PConstr _; _} -> true | _ -> false in
  match psss with
  | (p :: _, _) :: _ when is_var p ->
      SUtil.split_when (fun (ps, _) -> is_constr (List.hd ps)) psss
  | (p :: _, _) :: _ when is_constr p ->
      SUtil.split_when (fun (ps, _) -> is_var (List.hd ps)) psss
  | _ ->
      assert false

(** [get_constructor_count ss ty_name] return the number of constructor of the
    type called [ty_name] in the skeletal semantics [ss]. *)
let get_constructor_count ss ty_name =
  let[@warning "-8"] _, TDVariant (_, css) = SMap.find ty_name ss.ss_types in
  List.length css

(** [get_input_type ss c_name ty_name] returns the list of types of the
    arguments of the constructor [c_name] of type [ty_name] in the skeletal
    semantics [ss]. *)
let get_input_type ss c_name ty_name =
  let[@warning "-8"] _, TDVariant (_, css) = SMap.find ty_name ss.ss_types in
  let cs = List.find (fun cs -> cs.cs_name = c_name) css in
  match cs.cs_input_type with {tydesc= Product l; _} -> l | ty -> [ty]

(** [extract_variables input_type group] extract a set of variable names in
    order to have the most meaningfull matching as possible. *)
let extract_variables input_type group =
  (* TODO: fresh_names *)
  match group with
  | [(ps, _)] ->
      (* Only one alternative, use the existing variable names *)
      SUtil.zipWith
        (fun p ty ->
          match p.pdesc with
          | PVar name ->
              ((name, ty), Pat.var name ty)
          | PWild ->
              ((Printf.sprintf "tmp%d" @@ counter (), ty), Pat.wild ty)
          | _ ->
              assert false )
        ps input_type
  | _ ->
      (* More than one constructor, new names everywhere *)
      (* TODO: Maybe check for the same variable name used in all constructors *)
      SUtil.for_each input_type
      @@ fun ty ->
      let name = Printf.sprintf "tmp%d" @@ counter () in
      ((name, ty), Pat.var name ty)

(** Main pattern matching desctruction function. We assume that:
    - [vars] is a list of variable to match, with their types
    - [psss] is a list of tuples (ps, s), of type pattern list * skeleton.
      [length ps = length vars]
    - [default] is the default expression used if all patterns fail to match *)
let rec match_explode ss vars psss default =
  match vars with
  | [] -> (
    (* No variables, the first alternative is taken if it exists *)
    match psss with
    | [] ->
        default
    | (_, s) :: _ ->
        s )
  | (var, typ) :: vars ->
      (* Matching the first variable var *)
      if psss = [] then (* There is no alternative to select *)
        default
      else
        (* Extract the first pattern of each alternative, corresponding to the first
         variable var *)
        let ps = List.map (fun (ps, _) -> List.hd ps) psss in
        if only_variables ps then explode_variables var typ vars ss psss default
        else if only_constructors ps then
          explode_constructors var typ vars ss psss default
        else if matching_tuples typ then
          explode_tuples var typ vars ss psss default
        else explode_mixed var typ vars ss psss default

(** [explode_variables var typ vars ss psss default ] is a specialization of
    [match_explode] where all the patterns for [var] (of type [typ]) in [psss]
    are variables *)
and explode_variables var typ vars ss psss default =
  (* let bind each variable *)
  let psss =
    List.map
      (fun (ps, s) ->
        match ps with
        | p :: _ ->
            (List.tl ps, Skel.letin p (Skel.ret @@ Term.letbound var typ) s)
        | [] ->
            failwith "[explode_variables] A pattern is missing" )
      psss
  in
  (* Recursively explode the pattern matching *)
  match_explode ss vars psss default

(** [explode_constructors] is called if all the patterns are constructors *)
and explode_constructors var typ vars ss psss default =
  (* This is compiled to one alternative per constructor with submatches *)
  (* First, get the type of the the matched variable and sort the alternatives
     per constructor *)
  let[@warning "-8"] (UserType ((type_name, _), type_var)) = typ.tydesc in
  let sorted_psss = sort_by_constructor psss in
  let pss =
    SUtil.for_each sorted_psss
    @@ fun (c_name, group) ->
    let input_type = get_input_type ss c_name type_name in
    let tmp_vars, patterns =
      SUtil.unzip @@ extract_variables input_type group
    in
    ( Pat.constr ((c_name, None), (type_name, type_var)) (Pat.tuple patterns)
    , match_explode ss (tmp_vars @ vars) group default )
  in
  let constructor_count = get_constructor_count ss type_name in
  let default_case =
    (* If not all the constructors are present, add the default case *)
    if List.length sorted_psss <> constructor_count then
      [(Pat.wild typ, default)]
    else []
  in
  Skel.matching (Term.letbound var typ) (pss @ default_case)

(** [explode_tuples] handle the compilation of tuples' matching, tuples are
    lists of patterns. *)
and explode_tuples var typ vars ss psss default =
  let[@warning "-8"] (Product input_type) = typ.tydesc in
  (* match_explode expects variables, so we create fresh ones *)
  let tmp_vars =
    SUtil.for_each input_type (fun ty ->
        (Printf.sprintf "tmp%d" @@ counter (), ty) )
  in
  (* In order to remove these bad names, we replace tmp<i> by c.i where c is
     the matched variable*)
  let shadow s =
    SUtil.fold_left_i
      (fun s n (pname, ptyp) ->
        Skel.letin (Pat.var pname ptyp)
          (Skel.ret @@ Term.nth (Term.letbound var typ) (n + 1))
          s )
      s tmp_vars
  in
  shadow
  @@ match_explode ss (tmp_vars @ vars)
       ( SUtil.for_each psss
       @@ fun (ps, s) ->
       let[@warning "-8"] ({pdesc= PTuple ps'; _} :: ps) = ps in
       (ps' @ ps, s) )
       default

(** [exploded_mixed] is the implementation of the mixture rule, the default case
    is used to split the matching in sub parts that satisfy the variable rule or
    the constructor rule. *)
and explode_mixed var typ vars ss psss default =
  let init_psss, tail_psss = extract_homogeneous_kind psss in
  let vars = (var, typ) :: vars in
  match_explode ss vars init_psss (match_explode ss vars tail_psss default)

(* Create the Reader Monad and import useful functions *)
module R = struct
  type flag = Atomic | Main | Nothing

  type t = {function_info: flag SMap.t; ss: skeletal_semantics}
end

module ReaderR = Monad.Reader (R)

open Monad.Utils (ReaderR)

open ReaderR
open Infix
open R

(* Create the Mapper *)
module DesugarMapper = Mapper.Make (ReaderR)
open DesugarMapper

(** [flatten_pattern p] removes patterns binding a variable to a tuple. Such
    patterns are splitted in subvariables for each component of the tuple. A
    substitution is returned if needed *)
let flatten_pattern p =
  match p.pdesc with
  | PType ({pdesc= PVar name; _}, {tydesc= Product typs; _}) ->
      let vars = List.mapi (fun n ty -> (name ^ string_of_int n, ty)) typs in
      let pat =
        List.map (fun (name, ty) -> Pat.typed @@ Pat.var name ty) vars
      in
      (Pat.tuple pat, Some (name, vars))
  | _ ->
      (p, None)

(** [apply_subst sub s] *)
let apply_subst sub s =
  match sub with
  | None ->
      s
  | Some (old_name, new_names) ->
      let open Mapper.Default in
      let term this t =
        match t.tdesc with
        | TVar (LetBound (name, _)) ->
            if name = old_name then
              Term.tuple
                (List.map (fun (v, ty) -> Term.letbound v ty) new_names)
            else t
        | TNth ({tdesc= TVar (LetBound (name, _)); _}, _, n) ->
            if name = old_name then
              let name, ty = List.nth new_names (n - 1) in
              Term.letbound name ty
            else t
        | _ ->
            default.term this t
      in
      let mapper = {default_mapper with term} in
      mapper.skeleton mapper s

(** Flatten function arguments *)
let term_definition this (c, td) =
  let ty_vars, typ, t_opt = td in
  match t_opt with
  | Some {tdesc= TFunc (p, s); _} ->
      let p, subst = flatten_pattern p in
      let* s = apply_subst subst <$> this.skeleton this s in
      default.term_definition this
        (c, (ty_vars, typ, Option.some (Term.func p s)))
  | _ ->
      default.term_definition this (c, td)

let skeleton this s =
  match s.sdesc with
  | Match ({sdesc= Return {tdesc= TVar (LetBound (var, ty)); _}; _}, pss) ->
      let* ss = asks (fun r -> r.ss) in
      let default, pss = default_branch pss s.styp in
      return
      @@ match_explode ss
           [(var, ty)]
           (List.map (fun (p, s) -> ([p], s)) @@ pss)
           default
  | _ ->
      default.skeleton this s

let mapper = {default with term_definition; skeleton}

let desugar_ss ss =
  let function_info =
    SMap.map
      (fun (c, td) ->
        let _, typ, t_opt = td in
        match (t_opt, typ.tydesc) with
        | Some _, Arrow _ ->
            if Option.map String.trim c = Some "__atomic__" then Atomic
            else if Option.map String.trim c = Some "__main__" then Main
            else Nothing
        | _ ->
            Atomic )
      ss.ss_terms
  in
  run_reader (run mapper ss) {function_info; ss}
