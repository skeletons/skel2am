(* From CPS to Direct style *)

open Necro.Util
open Necro.TypedAST

module R = struct
  type t = {output_type: typ; continuation_name: string; ss: skeletal_semantics}
end

module ReaderR = Monad.Reader (R)

open Monad.Utils (ReaderR)

open ReaderR
open Infix
open R
module DirectMapper = Mapper.Make (ReaderR)
open DirectMapper

let is_atomic c = Option.map String.trim c = Some "__atomic__"

let last l = List.hd @@ List.rev l

let init l = List.rev @@ List.tl @@ List.rev l

let term_definition this td =
  let* c, (ty_vars, _, t_opt) = return td in
  match t_opt with
  | Some
      ( { tdesc= TFunc ({pdesc= PTuple ps; _}, s)
        ; ttyp= {tydesc= Arrow ({tydesc= Product tys; _}, omega); _}
        ; _ } as t )
    when not (is_atomic c) ->
      let[@warning "-8"] (PType ({pdesc= PVar k; _}, _)) = (last ps).pdesc in
      let* s =
        local
          (fun r -> {r with output_type= omega; continuation_name= k})
          (this.skeleton this s)
      in
      let p =
        let ps = init ps in
        match ps with
        | [p] ->
            p
        | _ ->
            AstHelper.pattern (PTuple ps)
            @@ AstHelper.typ
            @@ Product (List.map (fun p -> p.ptyp) ps)
      in
      let typ =
        let tys = init tys in
        match tys with [ty] -> ty | _ -> AstHelper.typ @@ Product tys
      in
      let[@warning "-8"] (Arrow (typ_out, _)) = (last tys).tydesc in
      let typ = AstHelper.typ @@ Arrow (typ, typ_out) in
      return (c, (ty_vars, typ, Some {t with tdesc= TFunc (p, s); ttyp= typ}))
  | _ ->
      default.term_definition this td

let term this t =
  let* r = ask () in
  match t.tdesc with
  | TVar (TopLevel (Specified, ((name, _) as fun_name), typ_list, _)) ->
      let c, _ = SMap.find name r.ss.ss_terms in
      if not (is_atomic c) then
        let[@warning "-8"] (Arrow ({tydesc= Product typs; _}, typ_out)) =
          t.ttyp.tydesc
        in
        let typ =
          match init typs with [typ] -> typ | l -> AstHelper.typ @@ Product l
        in
        return
          { t with
            tdesc= TVar (TopLevel (Specified, fun_name, typ_list, typ))
          ; ttyp= AstHelper.typ @@ Arrow (typ, typ_out) }
      else default.term this t
  | _ ->
      default.term this t

let skeleton this s =
  let* r = ask () in
  match s.sdesc with
  | Apply (({tdesc= TVar (TopLevel _); _} as t1), {tdesc= TTuple ts; _}) -> (
      let* t1 = default.term this t1 in
      match (last ts).tdesc with
      | TFunc (p, s) ->
          let* p = this.pattern this p in
          let* s = this.skeleton this s in
          let args = match init ts with [t] -> t | l -> AstHelper.ttuple l in
          return AstHelper.(letin p (apply t1 args) s)
      | TVar (LetBound (k, _)) when k = r.continuation_name ->
          let args = match init ts with [t] -> t | l -> AstHelper.ttuple l in
          return AstHelper.(apply t1 args)
      | _ ->
          default.skeleton this s )
  | Apply ({tdesc= TVar (LetBound (k, _)); _}, t2) when k = r.continuation_name
    ->
      let* t2 = this.term this t2 in
      return @@ AstHelper.return t2
  | LetIn (NoBind, p, s1, s2) ->
      (* The letin function can remove useless let bindings *)
      let* p = this.pattern this p in
      let* s1 = this.skeleton this s1 in
      let* s2 = this.skeleton this s2 in
      return @@ AstHelper.letin p s1 s2
  | _ ->
      default.skeleton this s

let direct_mapper = {default with term_definition; skeleton; term}

let direct_ss ss =
  run_reader (run direct_mapper ss)
    {output_type= AstHelper.typ @@ Product []; continuation_name= "unknown"; ss}
