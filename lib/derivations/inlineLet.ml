(** Inline Monadic Let Bindings *)

open Necro
open TypedAST
module StateCounter = Monad.State (Int)
open StateCounter

open Monad.Utils (StateCounter)

open Infix

let incr_and_get () = modify (( + ) 1) >> get ()

open Mapper.Make (StateCounter)

let skeleton_desc this sd =
  match sd with
  | LetIn (TermBind (tk, name, ty_vars, typ'), p, s1, s2)
  | LetIn (SymbolBind (_, tk, name, ty_vars, typ'), p, s1, s2) ->
      let* n = incr_and_get () in
      let tmp = Printf.sprintf "_tmp%d" n in
      let* p = this.pattern this p in
      let* s1 = this.skeleton this s1 in
      let* s2 = this.skeleton this s2 in
      return
      @@ AstHelper.(
           LetIn
             ( NoBind
             , AstHelper.pattern (PVar tmp) s1.styp
             , s1
             , apply
                 { tdesc= TVar (TopLevel (tk, name, ty_vars, typ'))
                 ; tloc= Util.ghost_loc
                 ; ttyp= typ' }
                 (ttuple [letbound tmp s1.styp; func p s2]) ) )
  | _ ->
      default.skeleton_desc this sd

let inline_let_mapper = {default with skeleton_desc}

let inline_let_ss ss = fst @@ run_state (run inline_let_mapper ss) 0
