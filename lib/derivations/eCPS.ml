(** CPS-Transform Monadic Evaluators *)

open Necro.TypedAST
open Necro
open Util
module SUtil = Skel2am__Util
open Ast

(** Read-only state of the mapper *)
module Cont = struct
  (** The continuation starts as [Identity] and then becomes a [Composition].
      This is used to avoid useless let-bindings *)
  type cont = Variable of string | Function of pattern * skeleton

  (** The continuation type used in the transformation *)
  type continuation =
    { ccont: cont  (** A representation of the remaining computation *)
    ; ctyp_in: typ  (** Input type of the continuation*) }

  let dummy_cont =
    {ccont= Variable "__dummy_continuation__"; ctyp_in= Typ.user "value"}

  let cont_return monad return typ =
    { ccont=
        Function
          ( Pat.var "x" typ
          , Skel.apply
              (Term.toplevel Specified (with_path return) [typ]
                 (Typ.arrow typ @@ Typ.user_param monad [typ]) )
            @@ Term.letbound "x" typ )
    ; ctyp_in= typ }

  (** The information needed to translate an effectful
      [{let p =%? eff in body} cont] *)
  type lift_info =
    { eff: skeleton  (** Argument of the effect *)
    ; pat: pattern  (** Result of the effect *)
    ; body: skeleton  (** Local continuation of the effect *)
    ; cont: continuation  (** Global continuation of the effect *)
    ; translate: skeleton -> continuation -> skeleton
          (** Traduction function (open recursion) *) }

  (** The traduction pattern of effects (lift_eff functions) *)
  type lift_function = lift_info -> skeleton

  (** This state will never change during the transformation *)
  type static_state =
    { return_type: typ
    ; monad_name: string
    ; return: string
    ; bind: string
    ; effects: SSet.t
    ; lifts: lift_function SMap.t }

  (** Read-only state of the mapper *)
  type t = {static: static_state; cont: continuation}

  let cont_type r = Typ.arrow r.cont.ctyp_in r.static.return_type
end

open Cont

let get_return_type_of_main ss =
  let t =
    SMap.fold
      (fun _ (c, (_, typ, _)) acc ->
        let is_main = Option.map String.trim c = Some "__main__" in
        match typ.tydesc with
        | Arrow (_, typ_out) when is_main ->
            Ok typ_out
        | _ when is_main ->
            Error "Main term is no a function."
        | _ ->
            acc )
      ss.ss_terms (Error "Main function is missing.")
  in
  SUtil.get_ok t SUtil.exit_with_error

let flag flg str = String.trim str = flg

let has_flag flg doc_string = Option.map (flag flg) doc_string = Some true

let get_monad_name ss =
  let monad_name =
    SMap.fold
      (fun name (doc_string, _) acc ->
        if has_flag "__monad__" doc_string then Ok name else acc )
      ss.ss_types (Error "__monad__ flag is missing")
  in
  SUtil.get_ok monad_name SUtil.exit_with_error

let get_name_with_flag flg ss =
  let monad_name =
    SMap.fold
      (fun name (doc_string, _) acc ->
        if has_flag flg doc_string then Ok name else acc )
      ss.ss_terms
      (Result.error @@ flg ^ " flag is missing")
  in
  SUtil.get_ok monad_name SUtil.exit_with_error

let collect_effects ss =
  SMap.fold
    (fun name (doc_string, _) acc ->
      if Option.map (flag "__effect__") doc_string = Some true then
        SSet.add name acc
      else acc )
    ss.ss_terms SSet.empty

let remove_monad ty =
  match ty.tydesc with
  | UserType (monad, [ty]) when monad = ("m", None) ->
      ty
  | _ ->
      ty

let ask_binder = TermBind (Specified, with_path "ask", [], Typ.product [])

let local_binder = TermBind (Specified, with_path "local", [], Typ.product [])

let lift_ask dinfo =
  Skel.letin' ask_binder dinfo.pat dinfo.eff
    (dinfo.translate dinfo.body dinfo.cont)

let lift_local dinfo =
  (* TODO: fresh name *)
  let initial_env = Pat.var "initial_env" (Typ.user "env") in
  let result_type = remove_monad dinfo.body.styp in
  let result = Pat.var "__result__" result_type in
  let unit = Pat.tuple [] in
  Skel.letin' ask_binder initial_env (Skel.ret @@ Term.tuple [])
  @@ Skel.letin' local_binder unit dinfo.eff
  @@ dinfo.translate dinfo.body
       { ccont=
           Function
             ( result
             , Skel.letin' local_binder unit
                 (Skel.ret @@ Term.letbound "initial_env" (Typ.user "env"))
               @@ dinfo.translate
                    ( Skel.apply
                        (Term.toplevel Specified (with_path "return")
                           [result_type]
                           ( Typ.arrow result_type
                           @@ Typ.user_param "m" [result_type] ) )
                    @@ Term.letbound "__result__" result_type )
                    dinfo.cont )
       ; ctyp_in= dinfo.pat.ptyp }

let collect_static_information ss =
  let return_type_of_main = get_return_type_of_main ss in
  let monad_name = get_monad_name ss in
  let return_type = return_type_of_main in
  let return = get_name_with_flag "__return__" ss in
  let bind = get_name_with_flag "__bind__" ss in
  let effects = collect_effects ss in
  let lifts = SMap.of_list [("ask", lift_ask); ("local", lift_local)] in
  {return_type; monad_name; return; bind; effects; lifts}

let initial_env ss =
  let static = collect_static_information ss in
  let cont = dummy_cont in
  {static; cont}

(* Create the Reader Monad over Cont and import useful functions *)
module ReaderCont = Monad.Reader (Cont)

open Monad.Utils (ReaderCont)

open ReaderCont
open Infix

let with_continuation f m =
  local (fun ({cont; _} as t) -> {t with cont= f cont}) m

(* Create a mapper wrapped in the Reader Monad *)
module ECPSMapper = Mapper.Make (ReaderCont)
open ECPSMapper

(** CPS-transform a term_definition. For Functions of type [ty_in -> ty_out]:
    - Change the return type to [ctyp_out]
    - Add an argument [kont] of type [ty_out -> ctyp_out] *)
let term_definition this (doc_string, td) =
  let* r = ask () in
  let type_args, typ, t_opt = td in
  match (t_opt, typ.tydesc) with
  | _
    when has_flag "__effect__" doc_string
         || has_flag "__return__" doc_string
         || has_flag "__bind__" doc_string
         || has_flag "__atomic__" doc_string ->
      (* Atomic function, leave it unchanged *)
      return (doc_string, td)
  | Some {tdesc= TFunc (p, s); _}, Arrow (_, ty_out)
    when has_flag "__main__" doc_string ->
      (* Main function, transform with identity continuation *)
      let* p = this.pattern this p in
      let* s =
        with_continuation (fun _ ->
            cont_return r.static.monad_name r.static.return
              (remove_monad ty_out) )
        @@ this.skeleton this s
      in
      return (doc_string, (type_args, typ, Option.some @@ Term.func p s))
  | Some {tdesc= TFunc (p, s); _}, Arrow (ty_in, ty_out) ->
      (* Top Level monadic function to CPS *)
      let* p = this.pattern this p in
      let used_variables = SUtil.used_variables_skel s in
      let continuation =
        Util.fresh (fun name -> not @@ Util.SMap.mem name used_variables) "k"
      in
      let ty_out = remove_monad ty_out in
      let* s =
        with_continuation (fun _ ->
            {ccont= Variable continuation; ctyp_in= ty_out} )
        @@ this.skeleton this s
      in
      let cont_type = Typ.arrow ty_out r.static.return_type in
      return
        ( doc_string
        , ( type_args
          , Typ.arrow (Typ.add_comp ty_in cont_type) r.static.return_type
          , Option.some
            @@ Term.func
                 (Pat.add_comp p @@ Pat.typed @@ Pat.var continuation cont_type)
                 s ) )
  | _ ->
      default.term_definition this (doc_string, td)

let skeleton this s =
  let* r = ask () in
  match s.sdesc with
  (* [[let p =%bind w in m]] k = [[w]] (\p -> [[m]] k) *)
  | LetIn (TermBind (Specified, name, _, _), p, s1, s2)
    when SUtil.without_path name = r.static.bind ->
      let* p = this.pattern this p in
      let* s2 = this.skeleton this s2 in
      with_continuation (fun _ -> {ccont= Function (p, s2); ctyp_in= p.ptyp})
      @@ this.skeleton this s1
  (* [[let p =%eff e in s]] k = let p %eff e in [[s]] (\v -> discard_eff (); k v) *)
  | LetIn (TermBind (Specified, name, _, _), pat, eff, body) ->
      Printf.fprintf stderr "[CPS] Found effect: %s\n"
      @@ SUtil.without_path name ;
      let lift_function = SMap.find (SUtil.without_path name) r.static.lifts in
      return
      @@ lift_function
           { eff
           ; pat
           ; body
           ; cont= r.cont
           ; translate=
               (fun s k -> run_reader (this.skeleton this s) {r with cont= k})
           }
  (* [[return x]] k = k x *)
  | Apply ({tdesc= TVar (TopLevel (Specified, (name, _), _, _)); _}, t)
    when name = r.static.return -> (
    match r.cont.ccont with
    | Variable continuation ->
        return @@ Skel.apply (Term.letbound continuation (cont_type r)) t
    | Function (p, body) ->
        return @@ Skel.letin p (Skel.ret t) body )
  (* [[t1 t2]] k  =  t1 (t2, k) *)
  | Apply (({tdesc= TVar (TopLevel (Specified, (name, _), _, _)); _} as t1), t2)
    when not
           ( SSet.mem name r.static.effects
           || name = r.static.bind || name = "run_reader" ) ->
      let* t1 = this.term this t1 in
      let* t2 = this.term this t2 in
      let cont_type = cont_type r in
      (* The extra parameter of the function *)
      let cont_term =
        match r.cont.ccont with
        | Variable continuation ->
            Term.letbound continuation cont_type
        | Function (p, s) ->
            Term.func p s
      in
      return @@ Skel.apply t1 @@ Term.add_comp t2 cont_term
  | _ ->
      default.skeleton this s

let term this term =
  match term.tdesc with
  | TVar (TopLevel (Specified, name, typs, {tydesc= Arrow (ty_in, ty_out); _}))
    ->
      let* r = ask () in
      let ttyp =
        Typ.arrow
          ( Typ.add_comp ty_in
          @@ Typ.arrow (remove_monad ty_out) r.static.return_type )
          r.static.return_type
      in
      return @@ Term.toplevel Specified name typs ttyp
  | _ ->
      default.term this term

let ecps_mapper = {default with skeleton; term; term_definition}

let ecps_ss ss = ReaderCont.run_reader (run ecps_mapper ss) (initial_env ss)
