(** Make an abstract machine small step *)

open Necro
open TypedAST
open Util
open Ast
module SUtil = Skel2am__Util

module S = struct
  type atomicity = Atomic | ToTranslate of string (* Constructor name *)

  type t =
    { configuration_name: string  (** The name of the [configuration] type *)
    ; function_name: string
    ; function_info: atomicity SMap.t
    ; constructors: constructor_signature list
          (** The generated constructors *)
    ; branches: (pattern * skeleton) list
          (** The generated branches of the [step] function *) }
end

(* Create the State Monad over S and import useful functions *)
module StateS = Monad.State (S)

open Monad.Utils (StateS)

open StateS
open Infix
open S

let remove_monad ty =
  match ty.tydesc with
  | UserType (monad, [ty]) when monad = ("m", None) ->
      ty
  | _ ->
      ty

let get_configuration_type () =
  let* s = get () in
  return @@ Typ.user s.configuration_name

(** Add a constructor the the list.
    - [cs_name] is the expected constructor name,
    - [cs_input_type] is the type of the argument,
    - [p] is the argument,
    - [s] the body of the function. *)
let tell_constructor cs_name cs_input_type p body =
  let* st = get () in
  let cs =
    { cs_name
    ; cs_comment= None
    ; cs_input_type
    ; cs_variant_type= st.configuration_name
    ; cs_type_args= [] }
  in
  let branch =
    let p = SUtil.untype_pattern p in
    (Pat.constr ((cs.cs_name, None), (st.configuration_name, [])) p, body)
  in
  modify (fun s ->
      {s with constructors= cs :: s.constructors; branches= branch :: s.branches} )

(* Create a mapper wrapped in the State Monad *)
module SmallStepMapper = Mapper.Make (StateS)
open SmallStepMapper

(** Used to update [function_name] *)
let skeletal_semantics this ss =
  let* ss_terms =
    mmapM (fun (function_name, c_term_def) ->
        let* () = modify (fun s -> {s with function_name}) in
        this.term_definition this c_term_def )
    @@ SMap.mapi (fun a b -> (a, b)) ss.ss_terms
  in
  return {ss with ss_terms}

(** Generate the constructors and branches *)
let term_definition this (c, td) =
  let _, typ, t_opt = td in
  let* s = get () in
  let atomicity = SMap.find s.function_name s.function_info in
  match (t_opt, typ.tydesc, atomicity) with
  | _, _, Atomic ->
      return (c, td)
  | Some {tdesc= TFunc (p, s); _}, Arrow (typ_in, typ_out), ToTranslate cs_name
    ->
      let* s = this.skeleton this s in
      let* () = tell_constructor cs_name typ_in p s in
      (* Main function, create a Stop constructor with an argument of type typ_out *)
      let* () =
        let* config_type = get_configuration_type () in
        whenM (Option.map String.trim c = Some "__main__")
        @@ tell_constructor "Stop" (remove_monad typ_out) (Pat.wild typ_out)
        @@ Skel.ret
        @@ Term.letbound "config" config_type
      in
      (* The returned pair does not matter as it will be removed later *)
      return (c, td)
  | _ ->
      return (c, td)

(* TODO: change this comment *)

(** Change the type of the skeleton. Note that only the "main" skeleton is
    explored *)
let skeleton this s =
  match s.sdesc with
  | Apply
      ( { tdesc= TVar (TopLevel (_, (name, _), _, _))
        ; ttyp= {tydesc= Arrow (_, typ_out); _}
        ; _ }
      , args ) -> (
      let* r = get () in
      let atomicity = SMap.find name r.function_info in
      match atomicity with
      | Atomic when name = "return" ->
          (* The monad must disappear *)
          return @@ Skel.ret
          @@ Term.constr (("Stop", None), (r.configuration_name, []))
          @@ args
      | Atomic ->
          (* Atomic call, use the Stop constructor *)
          return
          @@ Skel.letin (Pat.var "res" typ_out) s
               ( Skel.ret
               @@ Term.constr (("Stop", None), (r.configuration_name, []))
               @@ Term.letbound "res" typ_out )
      | ToTranslate c_name ->
          (* Non-atomic call, use the right constructor *)
          return @@ Skel.ret
          @@ Term.constr ((c_name, None), (r.configuration_name, [])) args )
  | LetIn (NoBind, p, s1, s2) ->
      (* s1 must be an atomic call *)
      let* s2 = this.skeleton this s2 in
      return @@ Skel.letin p s1 s2
  | LetIn (TermBind (Specified, name, _, _), pat, {sdesc= Return eff; _}, body)
    ->
      let* eff = this.term this eff in
      let* body = this.skeleton this body in
      let trigger =
        Term.toplevel Specified
          ("trigger_" ^ fst name, None)
          []
          (Typ.arrow eff.ttyp pat.ptyp)
      in
      return @@ Skel.letin pat (Skel.apply trigger eff) body
  | Match (s, pss) ->
      (* s must be a trivial skeleton (term or atomic apply) *)
      let* pss =
        mapM
          (fun (p, s) ->
            let* s = this.skeleton this s in
            return (p, s) )
          pss
      in
      return @@ Skel.matching' s pss
  | Return t ->
      (* Same as atomic call but inlined (we have a term directly) *)
      let* r = get () in
      let name = "Stop" in
      return @@ Skel.ret
      @@ Term.constr ((name, None), (r.configuration_name, [])) t
  | _ ->
      (* For Branch and Exists, just translate the subskeletons *)
      default.skeleton this s

let small_step_mapper =
  {default with skeletal_semantics; term_definition; skeleton}

(** Make a small step semantics from an abstract machine *)
let small_step_ss ss =
  (* The type of the abstract machine configuration *)
  let all_variables = Skel2am__Util.all_toplevel ss in
  let configuration_name =
    Util.fresh (Fun.negate @@ Fun.flip List.mem all_variables) "configuration"
  in
  let config_type = Typ.user configuration_name in
  (* Build the info map containing the atomicity of each toplevel term *)
  let function_info =
    (* We generate a constructor for non-atomic functions *)
    let is_fresh_constr =
      let all_constructors = Skeleton.get_all_constructors ss in
      Fun.negate
      @@ fun name ->
      List.exists (fun cs -> String.equal cs.cs_name name) all_constructors
    in
    SMap.mapi
      (fun name (c, td) ->
        let _, typ, t_opt = td in
        match (t_opt, typ.tydesc) with
        | Some _, Arrow _ ->
            if
              List.mem (Option.map String.trim c)
                [ Some "__atomic__"
                ; Some "__return__"
                ; Some "__bind__"
                ; Some "__effect__" ]
            then Atomic
            else
              ToTranslate
                (Util.fresh is_fresh_constr @@ String.capitalize_ascii name)
        | _ ->
            Atomic )
      ss.ss_terms
  in
  (* Run the mapper on the skeletal semantics. The state contains the constructors and branches *)
  let ss, s =
    StateS.run_state (run small_step_mapper ss)
      { configuration_name
      ; function_name= ""
      ; function_info
      ; constructors= []
      ; branches= [] }
  in
  (* The step function of type configuration -> configuration *)
  let step_body =
    Term.func
      (Pat.typed @@ Pat.var "config" config_type)
      ( SUtil.flatten_matching
      @@ Skel.matching (Term.letbound "config" config_type) s.branches )
  in
  (* All non-atomic functions are removed *)
  let is_atomic name =
    if SMap.find name function_info = Atomic then true else false
  in
  let ss_order =
    List.filter
      (function ParsedAST.TermDecl name -> is_atomic name | _ -> true)
      ss.ss_order
    @ [ParsedAST.TypeDecl configuration_name; ParsedAST.TermDecl "step"]
  in
  let ss_terms = SMap.filter (fun name _ -> is_atomic name) ss.ss_terms in
  (* The final skeletal semantics *)
  { ss with
    ss_order
  ; ss_types=
      SMap.add configuration_name
        (Some " __protected__ ", TDVariant ([], s.constructors))
        ss.ss_types
  ; ss_terms=
      SMap.add "step"
        ( Some " __main__ "
        , ([], Typ.arrow config_type config_type, Some step_body) )
        ss_terms }
