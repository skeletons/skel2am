(** CPS-Transform First Order Skel Program *)

open Necro.TypedAST
open Necro
open Util
open Ast
module SUtil = Skel2am__Util

(** Read-only state of the mapper *)
module Cont = struct
  (** The continuation starts as [Identity] and then becomes a [Composition].
      This is used to avoid useless let-bindings *)
  type cont = Variable of string | Function of pattern * skeleton

  (** The continuation type used in the transformation *)
  type continuation = {ccont: cont; ctyp_in: typ; ctyp_out: typ}

  let skel_type cont = Typ.arrow cont.ctyp_in cont.ctyp_out

  let identity typ =
    Function (Pat.var "x" typ, Skel.ret @@ Term.letbound "x" typ)

  (** Main state, a continuation, the program to translate and the outpout type
      of the continuation (the result of the __main__ function) *)
  type t = {current_continuation: continuation; atomics: SSet.t; typ_out: typ}

  let type_of_main ss =
    Util.SMap.fold
      (fun _ (c, (_, typ, _)) b ->
        if Option.map String.trim c = Some "__main__" then
          match typ.tydesc with
          | Arrow (_, typ_out) ->
              Ok typ_out
          | _ ->
              Error "Main term is no a function."
        else b )
      ss.ss_terms (Error "Main function is missing.")

  let atomic_flag c = Option.map String.trim c = Some "__atomic__"

  let main_flag c = Option.map String.trim c = Some "__main__"

  let collect_atomics ss =
    SMap.fold
      (fun name (c, (_, _, top)) atomics ->
        if atomic_flag c || top = None then SSet.add name atomics else atomics )
      ss.ss_terms SSet.empty

  (** Some dummy continuation, it will be replaced during the transformation *)
  let initial ss =
    (* Find the main function  *)
    let type_of_main = type_of_main ss in
    let typ_out =
      SUtil.get_ok type_of_main (fun e ->
          Printf.fprintf stderr "Error:\n\t%s\n" e ;
          exit 1 )
    in
    let atomics = collect_atomics ss in
    { (* \():() -> () *)
      current_continuation=
        { ccont= Variable "__dummy_continuation__"
        ; ctyp_in= Typ.product []
        ; ctyp_out= typ_out }
    ; atomics
    ; typ_out }
end

(* Create the Reader Monad over Cont and import useful functions *)
module ReaderCont = Monad.Reader (Cont)

open Monad.Utils (ReaderCont)

open ReaderCont
open Infix
open Cont

let ask_continuation () = asks (fun x -> x.current_continuation)

let with_continuation f m =
  local
    (fun ({current_continuation; _} as t) ->
      {t with current_continuation= f current_continuation} )
    m

let emplace_continuation k m = with_continuation (Fun.const k) m

let ask_ret_type () = asks (fun x -> x.typ_out)

let is_atomic atomics f = SSet.mem f atomics

(* Create a mapper wrapped in the Reader Monad *)
module CPSMapper = Mapper.Make (ReaderCont)
open CPSMapper

(** CPS-transform a term_definition. For Functions of type [ty_in -> ty_out]:
    - Change the return type to [ctyp_out]
    - Add an argument [kont] of type [ty_out -> ctyp_out] *)
let term_definition this (doc_string, td) =
  let type_args, typ, t_opt = td in
  match (t_opt, typ.tydesc) with
  | _ when atomic_flag doc_string ->
      (* Atomic function, leave it unchanged *)
      return (doc_string, td)
  | Some {tdesc= TFunc (p, s); _}, Arrow (_, ty_out) when main_flag doc_string
    ->
      (* Main function, transform with identity continuation *)
      let* p = this.pattern this p in
      let* s =
        emplace_continuation
          {ccont= Cont.identity ty_out; ctyp_in= ty_out; ctyp_out= ty_out}
        @@ this.skeleton this s
      in
      return (doc_string, (type_args, typ, Option.some @@ Term.func p s))
  | Some {tdesc= TFunc (p, s); _}, Arrow (ty_in, ty_out) ->
      (* Top Level function to CPS *)
      let* p = this.pattern this p in
      let used_variables = SUtil.used_variables_skel s in
      let continuation =
        Util.fresh (fun name -> not @@ Util.SMap.mem name used_variables) "k"
      in
      let* ctyp_out = ask_ret_type () in
      let* s =
        emplace_continuation
          {ccont= Variable continuation; ctyp_in= ty_out; ctyp_out}
        @@ this.skeleton this s
      in
      let cont_type = Typ.arrow ty_out ctyp_out in
      return
        ( doc_string
        , ( type_args
          , Typ.arrow (Typ.add_comp ty_in cont_type) ctyp_out
          , Option.some
            @@ Term.func
                 (Pat.add_comp p @@ Pat.typed @@ Pat.var continuation cont_type)
                 s ) )
  | _ ->
      default.term_definition this (doc_string, td)

(** Main work of CPS transform *)
let skeleton this s =
  let* atomics = asks (fun r -> r.atomics) in
  match s.sdesc with
  (* [[Return t]] k = k [[ t ]]*)
  (* k may be inlined for readability *)
  | Return t -> (
      let* cont = ask_continuation () in
      match cont.ccont with
      | Variable continuation ->
          return @@ Skel.apply (Term.letbound continuation (skel_type cont)) t
      | Function (p, body) ->
          return @@ Skel.letin p (Skel.ret t) body )
  (* [[Let p = s1 in s2]] k = [[s1]] (\p -> [[s2]] k) *)
  | LetIn (NoBind, p, s1, s2) ->
      let* p = this.pattern this p in
      let* s2 = this.skeleton this s2 in
      with_continuation (fun r ->
          {r with ccont= Function (p, s2); ctyp_in= s1.styp} )
      @@ this.skeleton this s1
  | LetIn _ ->
      Printf.fprintf stderr "Cannot defunctionalize custom let-bindings\n" ;
      exit 1
  (* [[f arg]] k -> [[f]] ([[arg]], k) when f is not atomic *)
  | Apply (({tdesc= TVar (TopLevel (_, name, _, _)); _} as t1), t2)
    when not (is_atomic atomics (SUtil.without_path name)) ->
      let* cont = ask_continuation () in
      let* t1 = this.term this t1 in
      let* t2 = this.term this t2 in
      (* The extra parameter of the function *)
      let cont_term =
        match cont.ccont with
        | Variable continuation ->
            Term.letbound continuation @@ skel_type cont
        | Function (p, s) ->
            Term.func p s
      in
      return @@ Skel.apply t1 @@ Term.add_comp t2 cont_term
  (* [[f arg]] k = k ([[f]] [[arg]]) when f is atomic *)
  | Apply ({tdesc= TVar (TopLevel _); _}, _) -> (
      let* cont = ask_continuation () in
      let* s = default.skeleton this s in
      match cont.ccont with
      | Variable continuation ->
          (* TODO: Fresh name for res *)
          return
          @@ Skel.letin (Pat.var "res" cont.ctyp_in) s
          @@ Skel.apply
               (Term.letbound continuation (skel_type cont))
               (Term.letbound "res" cont.ctyp_in)
      | Function (p, body) ->
          return @@ Skel.letin p s body )
  (* [[match v with {p_i -> s_i} end]] k = match v with {p_i -> [[s_i]] k} end when v is a term *)
  | Match (({sdesc= Return _; _} as s), pss) ->
      let* pss =
        mapM
          (fun (p, s) ->
            let* p = this.pattern this p in
            let* s = this.skeleton this s in
            return (p, s) )
          pss
      in
      return @@ Skel.matching' s pss
  (* [[match s with {p_i -> s_i} end]] k = [[let v = s in match v with {p_i -> s_i} end]] k*)
  | Match (matched, pss) ->
      let free_vars = Skeleton.free_variables_skel s in
      let var =
        Util.fresh
          (fun name -> not @@ Util.SMap.mem name free_vars)
          "matched_term"
      in
      this.skeleton this
      @@ Skel.letin (Pat.var var s.styp) matched
      @@ Skel.matching (Term.letbound var s.styp) pss
  | _ ->
      default.skeleton this s

let term this t =
  let* atomics = asks (fun r -> r.atomics) in
  match t.tdesc with
  | TVar (TopLevel (Specified, name, typs, {tydesc= Arrow (ty_in, ty_out); _}))
    when not (is_atomic atomics (SUtil.without_path name)) ->
      let* cont = ask_continuation () in
      let ttyp =
        Typ.arrow
          (Typ.add_comp ty_in @@ Typ.arrow ty_out cont.ctyp_out)
          cont.ctyp_out
      in
      return @@ Term.toplevel Specified name typs ttyp
  | _ ->
      default.term this t

let cps_mapper = {default with term_definition; skeleton; term}

let cps_ss ss = ReaderCont.run_reader (run cps_mapper ss) (Cont.initial ss)
