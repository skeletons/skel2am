open Necro
open TypedAST
module SUtil = Skel2am__Util

module R = struct
  type t = {var: string; term: term}
end

open R
module Reader = Monad.Reader (R)
open Reader

open Monad.Utils (Reader)

open Infix

open Mapper.Make (Reader)

(** Apply [f] (the recursive substitution) to [x] if the pattern does not shadow
    the substitued variable. *)
let under p f x =
  let* r = ask () in
  let fv = SUtil.pattern_variables p in
  if Util.SMap.mem r.var fv then return x else f x

let skeleton_desc this sdesc =
  match sdesc with
  | LetIn (b, p, s1, s2) ->
      let* s1 = this.skeleton this s1 in
      let* s2 = under p (this.skeleton this) s2 in
      return @@ LetIn (b, p, s1, s2)
  | Match (s, pss) ->
      let* s = this.skeleton this s in
      let* pss =
        mapM
          (fun (p, s) ->
            let* s = under p (this.skeleton this) s in
            return (p, s) )
          pss
      in
      return @@ Match (s, pss)
  | _ ->
      default.skeleton_desc this sdesc

let term this t =
  let* r = ask () in
  match t.tdesc with
  | TVar (LetBound (name, _)) when name = r.var ->
      return r.term
  | _ ->
      default.term this t

let substitution_mapper = {default with skeleton_desc; term}

let replace_in_term var term t =
  run_reader (substitution_mapper.term substitution_mapper t) {var; term}

let replace_in_skel var term s =
  run_reader (substitution_mapper.skeleton substitution_mapper s) {var; term}
