(** This file defines a monadic mapper for Skel typed AST.*)
(* Two interfaces are exposed: *)
(* - The Default module is useful to create pure mapper, that don't need any context *)
(* - The Make module is parameterized with a monad m to handle complex contexts *)

open Necro
open TypedAST
open Monad

module type MAPPER = sig
  type 'a t

  type mapper =
    { (* main type *)
      skeletal_semantics: mapper -> skeletal_semantics -> skeletal_semantics t
    ; (* terms *)
      term_definition:
           mapper
        -> string option * term_definition
        -> (string option * term_definition) t
    ; term: mapper -> term -> term t
    ; term_desc: mapper -> term_desc -> term_desc t
    ; term_kind: mapper -> term_kind -> term_kind t
    ; variable: mapper -> variable -> variable t
    ; (* skeletons *)
      skeleton: mapper -> skeleton -> skeleton t
    ; skeleton_desc: mapper -> skeleton_desc -> skeleton_desc t
    ; pattern: mapper -> pattern -> pattern t
    ; pattern_desc: mapper -> pattern_desc -> pattern_desc t
    ; (* types *)
      type_definition: mapper -> type_definition -> type_definition t
    ; typ: mapper -> typ -> typ t
    ; typ_desc: mapper -> typ_desc -> typ_desc t
    ; type_kind: mapper -> type_kind -> type_kind t
    ; bind: mapper -> bind -> bind t
    ; (* fields and constructors *)
      constructor_signature:
        mapper -> constructor_signature -> constructor_signature t
    ; constructor: mapper -> constructor -> constructor t
    ; field_signature: mapper -> field_signature -> field_signature t
    ; field: mapper -> field -> field t }

  val default : mapper

  val run : mapper -> skeletal_semantics -> skeletal_semantics t
end

module Make (M : MONAD) : MAPPER with type 'a t = 'a M.t = struct
  module MFuns = Monad.Utils (M)
  open MFuns
  open MFuns.Infix

  type 'a t = 'a M.t

  type mapper =
    { (* main type *)
      skeletal_semantics: mapper -> skeletal_semantics -> skeletal_semantics t
    ; (* terms *)
      term_definition:
           mapper
        -> string option * term_definition
        -> (string option * term_definition) t
    ; term: mapper -> term -> term t
    ; term_desc: mapper -> term_desc -> term_desc t
    ; term_kind: mapper -> term_kind -> term_kind t
    ; variable: mapper -> variable -> variable t
    ; (* skeletons *)
      skeleton: mapper -> skeleton -> skeleton t
    ; skeleton_desc: mapper -> skeleton_desc -> skeleton_desc t
    ; pattern: mapper -> pattern -> pattern t
    ; pattern_desc: mapper -> pattern_desc -> pattern_desc t
    ; (* types *)
      type_definition: mapper -> type_definition -> type_definition t
    ; typ: mapper -> typ -> typ t
    ; typ_desc: mapper -> typ_desc -> typ_desc t
    ; type_kind: mapper -> type_kind -> type_kind t
    ; bind: mapper -> bind -> bind t
    ; (* fields and constructors *)
      constructor_signature:
        mapper -> constructor_signature -> constructor_signature t
    ; constructor: mapper -> constructor -> constructor t
    ; field_signature: mapper -> field_signature -> field_signature t
    ; field: mapper -> field -> field t }

  let skeletal_semantics this ss =
    let* ss_types =
      mmapM
        (fun (c, typ_def) ->
          let* typ_def = this.type_definition this typ_def in
          return (c, typ_def) )
        ss.ss_types
    in
    let* ss_terms =
      mmapM
        (fun c_term_def ->
          let* c_term_def = this.term_definition this c_term_def in
          return c_term_def )
        ss.ss_terms
    in
    let* ss_binders =
      mmapM
        (fun (tk, name) ->
          let* term_kind = this.term_kind this tk in
          return (term_kind, name) )
        ss.ss_binders
    in
    return {ss with ss_types; ss_terms; ss_binders}

  let term_definition this (c, term_def) =
    let typ_args, typ, term_opt = term_def in
    let* typ = this.typ this typ in
    let* term_opt = omapM (this.term this) term_opt in
    return (c, (typ_args, typ, term_opt))

  let term this term =
    let* tdesc = this.term_desc this term.tdesc in
    let* ttyp = this.typ this term.ttyp in
    return {term with tdesc; ttyp}

  let term_desc this tdesc =
    match tdesc with
    | TVar var ->
        let* var = this.variable this var in
        return @@ TVar var
    | TConstr (c, t) ->
        let* c = this.constructor this c in
        let* t = this.term this t in
        return @@ TConstr (c, t)
    | TTuple ts ->
        let* ts = mapM (this.term this) ts in
        return @@ TTuple ts
    | TFunc (p, s) ->
        let* p = this.pattern this p in
        let* s = this.skeleton this s in
        return @@ TFunc (p, s)
    | TField (t, f) ->
        let* t = this.term this t in
        let* f = this.field this f in
        return @@ TField (t, f)
    | TNth (t, typs, i) ->
        let* t = this.term this t in
        let* typs = mapM (this.typ this) typs in
        return @@ TNth (t, typs, i)
    | TRecMake fts ->
        let* fts =
          mapM
            (fun (f, t) ->
              let* f = this.field this f in
              let* t = this.term this t in
              return (f, t) )
            fts
        in
        return @@ TRecMake fts
    | TRecSet (t, fts) ->
        let* t = this.term this t in
        let* fts =
          mapM
            (fun (f, t) ->
              let* f = this.field this f in
              let* t = this.term this t in
              return (f, t) )
            fts
        in
        return @@ TRecSet (t, fts)

  let term_kind this tk =
    ignore this ;
    return tk

  let variable this v =
    match v with
    | LetBound (name, typ) ->
        let* typ = this.typ this typ in
        return @@ LetBound (name, typ)
    | TopLevel (tk, name, typs, typ) ->
        let* tk = this.term_kind this tk in
        let* typs = mapM (this.typ this) typs in
        let* typ = this.typ this typ in
        return @@ TopLevel (tk, name, typs, typ)

  let skeleton this s =
    let* sdesc = this.skeleton_desc this s.sdesc in
    let* styp = this.typ this s.styp in
    return {s with sdesc; styp}

  let skeleton_desc this sd =
    match sd with
    | Branching (typ, ss) ->
        let* typ = this.typ this typ in
        let* ss = mapM (this.skeleton this) ss in
        return @@ Branching (typ, ss)
    | Match (s, pss) ->
        let* s = this.skeleton this s in
        let* pss =
          mapM
            (fun (p, s) ->
              let* p = this.pattern this p in
              let* s = this.skeleton this s in
              return (p, s) )
            pss
        in
        return @@ Match (s, pss)
    | LetIn (b, p, s1, s2) ->
        let* b = this.bind this b in
        let* p = this.pattern this p in
        let* s1 = this.skeleton this s1 in
        let* s2 = this.skeleton this s2 in
        return @@ LetIn (b, p, s1, s2)
    | Exists typ ->
        let* typ = this.typ this typ in
        return @@ Exists typ
    | Return t ->
        let* t = this.term this t in
        return @@ Return t
    | Apply (t1, t2) ->
        let* t1 = this.term this t1 in
        let* t2 = this.term this t2 in
        return @@ Apply (t1, t2)

  let pattern this p =
    let* pdesc = this.pattern_desc this p.pdesc in
    let* ptyp = this.typ this p.ptyp in
    return @@ {p with pdesc; ptyp}

  let pattern_desc this pdesc =
    match pdesc with
    | PWild ->
        return @@ PWild
    | PVar var ->
        return @@ PVar var
    | PConstr (c, p) ->
        let* c = this.constructor this c in
        let* p = this.pattern this p in
        return @@ PConstr (c, p)
    | PTuple ps ->
        let* ps = mapM (this.pattern this) ps in
        return @@ PTuple ps
    | PRecord fpl ->
        let* fpl =
          mapM
            (fun (f, p) ->
              let* f = this.field this f in
              let* p = this.pattern this p in
              return (f, p) )
            fpl
        in
        return @@ PRecord fpl
    | POr (p1, p2) ->
        let* p1 = this.pattern this p1 in
        let* p2 = this.pattern this p2 in
        return @@ POr (p1, p2)
    | PType (p, typ) ->
        let* p = this.pattern this p in
        let* typ = this.typ this typ in
        return @@ PType (p, typ)

  let type_definition this typ_def =
    match typ_def with
    | TDVariant (args, cs) ->
        let* cs = mapM (this.constructor_signature this) cs in
        return @@ TDVariant (args, cs)
    | TDRecord (args, fs) ->
        let* fs = mapM (this.field_signature this) fs in
        return @@ TDRecord (args, fs)
    | TDAlias (args, typ) ->
        let* typ = this.typ this typ in
        return @@ TDAlias (args, typ)
    | _ ->
        return @@ typ_def

  let typ this typ =
    let* tydesc = this.typ_desc this typ.tydesc in
    return @@ {typ with tydesc}

  let typ_desc this tydesc =
    match tydesc with
    | Variable s ->
        return @@ Variable s
    | UserType (name, typs) ->
        let* typs = mapM (this.typ this) typs in
        return @@ UserType (name, typs)
    | Arrow (ty_in, ty_out) ->
        let* ty_in = this.typ this ty_in in
        let* ty_out = this.typ this ty_out in
        return @@ Arrow (ty_in, ty_out)
    | Product typs ->
        let* typs = mapM (this.typ this) typs in
        return @@ Product typs

  let type_kind this tk =
    ignore this ;
    return tk

  let bind this b =
    match b with
    | NoBind ->
        return NoBind
    | TermBind (tk, name, typs, typ) ->
        let* tk = this.term_kind this tk in
        let* typs = mapM (this.typ this) typs in
        let* typ = this.typ this typ in
        return @@ TermBind (tk, name, typs, typ)
    | SymbolBind (symbol, tk, name, typs, typ) ->
        let* tk = this.term_kind this tk in
        let* typs = mapM (this.typ this) typs in
        let* typ = this.typ this typ in
        return @@ SymbolBind (symbol, tk, name, typs, typ)

  let constructor_signature this cs =
    let* cs_input_type = this.typ this cs.cs_input_type in
    return {cs with cs_input_type}

  let constructor this c =
    let name, (type_name, args) = c in
    let* args = mapM (this.typ this) args in
    return (name, (type_name, args))

  let field_signature this fs =
    let* fs_field_type = this.typ this fs.fs_field_type in
    return {fs with fs_field_type}

  let field this f =
    let name, (type_name, args) = f in
    let* args = mapM (this.typ this) args in
    return (name, (type_name, args))

  let default =
    { skeletal_semantics
    ; term_definition
    ; term
    ; term_desc
    ; term_kind
    ; variable
    ; skeleton
    ; skeleton_desc
    ; pattern
    ; pattern_desc
    ; type_definition
    ; typ
    ; typ_desc
    ; type_kind
    ; bind
    ; constructor_signature
    ; constructor
    ; field_signature
    ; field }

  let run mapper ss = mapper.skeletal_semantics mapper ss
end

module Default = struct
  include Make (Monad.Identity)

  let default_mapper = default
end
