module type MONAD = sig
  type 'a t

  val fmap : ('a -> 'b) -> 'a t -> 'b t
  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t
  val return : 'a -> 'a t
  val bind : 'a t -> ('a -> 'b t) -> 'b t
end

module Identity : sig
  type 'a t = 'a

  val fmap : ('a -> 'b) -> 'a t -> 'b t
  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t
  val return : 'a -> 'a t
  val bind : 'a t -> ('a -> 'b t) -> 'b t

  val run_identity : 'a t -> 'a
end

module Reader : functor
  (R : sig
     type t
   end)
  -> sig
  type 'a t

  val fmap : ('a -> 'b) -> 'a t -> 'b t
  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t
  val return : 'a -> 'a t
  val bind : 'a t -> ('a -> 'b t) -> 'b t
  val ask : unit -> R.t t
  val asks : (R.t -> 'b) -> 'b t
  val local : (R.t -> R.t) -> 'a t -> 'a t
  val reader : (R.t -> 'a) -> 'a t
  val run_reader : 'a t -> R.t -> 'a
end

module Writer : functor
  (W : sig
     type t

     val mappend : t -> t -> t
     val mempty : t
   end)
  -> sig
  type 'a t

  val fmap : ('a -> 'b) -> 'a t -> 'b t
  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t
  val return : 'a -> 'a t
  val bind : 'a t -> ('a -> 'b t) -> 'b t

  val tell : W.t -> unit t
  val listen : 'a t -> ('a * W.t) t
  val writer : 'a * W.t -> 'a t
  val run_writer : 'a t -> 'a * W.t
end

module State : functor
  (S : sig
     type t
   end)
  -> sig
  type 'a t

  val fmap : ('a -> 'b) -> 'a t -> 'b t
  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t
  val return : 'a -> 'a t
  val bind : 'a t -> ('a -> 'b t) -> 'b t

  val get : unit -> S.t t
  val gets : (S.t -> 'a) -> 'a t
  val put : S.t -> unit t
  val modify : (S.t -> S.t) -> unit t
  val run_state : 'a t -> S.t -> ('a * S.t)
end

module Utils : functor (M : MONAD) -> sig
  type 'a t = 'a M.t

  val fmap : ('a -> 'b) -> 'a t -> 'b t
  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t
  val return : 'a -> 'a t
  val bind : 'a t -> ('a -> 'b t) -> 'b t

  module Infix : sig
    val ( <$> ) : ('a -> 'b) -> 'a t -> 'b t
    val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
    val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
    val ( >> ) : 'a t -> 'b t -> 'b t
  end

  val join : 'a M.t M.t -> 'a M.t

  val fold_leftM :
    ('a -> 'b -> 'a M.t) -> 'a -> 'b list -> 'a t

  val void : 'a M.t -> unit M.t
  val mapM : ('a -> 'b M.t) -> 'a list -> 'b list M.t
  val mapM_ : ('a -> 'b M.t) -> 'a list -> unit M.t
  val forM : 'a list -> ('a -> 'b M.t) -> 'b list M.t
  val forM_ : 'a list -> ('a -> 'b M.t) -> unit M.t

  val mmapM :
    ('a -> 'b M.t) ->
    'a Necro.Util.SMap.t ->
    'b Necro.Util.SMap.t t

  val omapM : ('a -> 'b M.t) -> 'a option -> 'b option t
  val sequence : 'a M.t list -> 'a list M.t
  val sequence_ : 'a M.t list -> unit M.t
  val whenM : bool -> unit t -> unit t
end
