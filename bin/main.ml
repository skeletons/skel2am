module SMap = Necro.Util.SMap
module SSet = Necro.Util.SSet
open Skel2am

let usage_msg =
  {|skel2am <file1> [<file2> ...] -t <transformation>
     <transformation> is a comma-separated list of derivations
     a derivation is one of: cps, defun, smallstep, desugar, c, refun, ecps
     You can prefix a derivation with a star to print the result.
Example: skel2am main.sk -t "cps,*defun,desugar,*refun"
  |}

type prog_type =
  | SkelProg of Necro.TypedAST.skeletal_semantics
  | CProg of CAST.program

type derivation =
  | CPS
  | Defun
  | SmallStep
  | Desugar
  | C
  | Refun
  | Direct
  | ECPS

let show = function
  | CPS ->
      "cps"
  | Defun ->
      "defun"
  | SmallStep ->
      "smallstep"
  | Desugar ->
      "desugared"
  | C ->
      "c"
  | Refun ->
      "refun"
  | Direct ->
      "direct"
  | ECPS ->
      "ecps"

let read_derivation = function
  | "cps" ->
      CPS
  | "defun" ->
      Defun
  | "smallstep" ->
      SmallStep
  | "desugar" ->
      Desugar
  | "c" ->
      C
  | "refun" ->
      Refun
  | "direct" ->
      Direct
  | "ecps" ->
      ECPS
  | s ->
      Printf.eprintf "Unknown derivation: %s\n" s ;
      Printf.eprintf
        "Please use one of: cps, direct, defun, refun, desugar, smallstep, c\n" ;
      exit 1

type print_flag = Print | Hide

type 'a stack = 'a list ref

type transformation = (print_flag * derivation) list

type worklist = transformation stack

let process_transformation stack s =
  let ts = String.split_on_char ',' s in
  let transformation =
    List.map
      (fun s ->
        let b, s =
          if s.[0] = '*' then (Print, String.sub s 1 (String.length s - 1))
          else (Hide, s)
        in
        (b, read_derivation s) )
      ts
  in
  stack := transformation :: !stack

let parse_args () =
  let files = ref [] in
  let worklist : worklist = ref [] in
  let speclist =
    [ ( "-t"
      , Arg.String (process_transformation worklist)
      , "Specify a transformation" ) ]
  in
  let anon_fun s = files := s :: !files in
  Arg.parse speclist anon_fun usage_msg ;
  (!files, !worklist)

let apply_derivation d ss =
  match d with
  | CPS ->
      SkelProg (CPS.cps_ss ss)
  | Defun ->
      SkelProg (Defun.defun_ss ss)
  | SmallStep ->
      SkelProg (SmallStep.small_step_ss ss)
  | Desugar ->
      SkelProg (Desugar.desugar_ss ss)
  | C ->
      CProg (CBackend.compile ss)
  | Refun ->
      SkelProg (Refun.refun_ss ss)
  | Direct ->
      SkelProg (Direct.direct_ss ss)
  | ECPS ->
      SkelProg (ECPS.ecps_ss ss)

let apply_transformation file_name ds =
  let skel_name = ref @@ Filename.chop_extension file_name in
  let deps = Necro.find_deps file_name in
  let ss = Necro.parse_and_type deps file_name in
  ignore
  @@ List.fold_left
       (fun ss (pf, d) ->
         skel_name := !skel_name ^ "_" ^ show d ;
         match ss with
         | CProg _ ->
             Printf.eprintf "Cannot transform a C program" ;
             exit 1
         | SkelProg ss -> (
             let ss = apply_derivation d ss in
             match pf with
             | Print -> (
               match ss with
               | CProg g ->
                   let channel = open_out (!skel_name ^ ".c") in
                   Printf.fprintf channel "%a" CPrinter.program g ;
                   close_out channel ;
                   ss
               | SkelProg ss' ->
                   let channel = open_out (!skel_name ^ ".sk") in
                   Printf.fprintf channel "%s" @@ Necro.Printer.string_of_ss ss' ;
                   close_out channel ;
                   ss )
             | Hide ->
                 ss ) )
       (SkelProg ss) ds

let main () =
  let files, transformations = parse_args () in
  List.iter
    (fun file ->
      List.iter (fun ds -> apply_transformation file ds) transformations )
    files

let () =
  try main () with
  | Skel2am.Ast.Skel.MkError (sd, ty) ->
      Printf.fprintf stderr "Ill-typed skeleton has been build\n" ;
      Printf.fprintf stderr "%s\n"
      @@ Necro.Printer.string_of_skeleton ~indent:0
           { sdesc= sd
           ; sloc= Util.ghost_loc
           ; styp= {tydesc= Product []; tyloc= Util.ghost_loc} } ;
      Printf.fprintf stderr "%s\n" @@ Necro.Printer.string_of_type ty
  | Skel2am.Ast.Term.MkError (td, ty) ->
      Printf.fprintf stderr "Ill-typed term has been build\n" ;
      Printf.fprintf stderr "%s\n"
      @@ Necro.Printer.string_of_term ~indent:0
           { tdesc= td
           ; tloc= Util.ghost_loc
           ; ttyp= {tydesc= Product []; tyloc= Util.ghost_loc} } ;
      Printf.fprintf stderr "%s\n" @@ Necro.Printer.string_of_type ty
