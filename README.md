# Skel2AM

## Presentation
Skel2AM is a semantics translation tool that converts skeletal semantics to abstract machines.


## Run the project
You'll need necrolib >= 13.0 installed on your machine.

``` sh
$ opam switch create skel2am 4.14.0
$ eval $(opam env)
$ opam repository add necro https://gitlab.inria.fr/skeletons/opam-repository.git#necro
$ opam install necrolib
```

The source code is in the `lib` directory, the tool itself is in `bin`.


To compile the project and run it, do
``` sh
$ dune build
$ dune test # optional, run the test suite
$ dune exec bin/main.exe -- <file.sk> -t "*cps,*defun,*smallstep"
$ dune exec bin/main.exe -- <file.sk> -t "cps,defun,*desugar,*refun"
$ dune exec bin/main.exe -- <file.sk> -t "cps,defun,desugar,*c"
```

The last three commands will generate many files:
- `file_cps.sk` is the result of CPS-tranlsation,
- `file_cps_defun.sk` is the defunctionalized version,
- `file_cps_defun_smallstep.sk` is a small step version of the previous file,
- `file_cps_defun_desugared.sk` has simple pattern matching and is ready to be compiled to C,
- `file_cps_defun_desugared_refun.sk` is the refunctionalized version,
- `file_cps_defun_desugared_c.c` is a C translation of the semantics.

It will **not** check is these files exist, so be sure to rename them if they do.

## Source code flags
### Atomicity (`__atomic__`)
You can flag a specified function as atomic by putting the `__atomic__` flag in its special comment like so

``` ocaml
(** __atomic__ *)
val atomic_specified (():()) : () =
  ()
```

Atomic functions are not transformed during the derivation, they are treated as unspecified functions.

An atomic function must call only atomic functions (otherwise it would need a continuation).

### Main function (`__main__`)
The main function will not be cps converted. It's return type determines the return type of the continuation.
The main function will be used to generate the initial configuration during the small step machine derivation.

The main function cannot be recursive, since it's not cps-transformed.

### Protected types (`__protected__`)
Protected types will not be refunctionalized.
The AST of your language will typically be a protected type.

## Derivations explained
Examples are available in the `skel` directory.

### CPS (`cps`)
The first derivation is a translation to Continuation-Passing-Style (CPS).
It makes the control flow explicit and the functions tail recursive.

Each non-atomic function takes an additional argument representing the continuation of the computation.
We compute the same thing, but call the continuation with the result instead of returning it directly.

### Defunctionalization (`defun`)
Then, we defunctionalize the generated continuations.
To do this, we collect all the functions and class them by type.

For each function type, we generate a new variant type in the semantics, whose constructors correspond to the functions encountered, and an `apply` function, matching the constructor and running the body of the closure.


### Small Step Machine (`smallstep`)
We'd like to be able to see the execution of the abstract machine step by step.
This is a kind of toplevel defunctionalization, with a step function going from one configuration to the next.

### Desugaring (`desugar`)
The goal of the `desugar` transformation is to simplify pattern matching skeletons, to destruct over one constructor at a time.
The following program:

``` ocaml
type int = Z | S int
val n : int

match n with
| Z -> e0
| S Z -> e1
| S (S Z) -> e2
| S m -> e3
end
```

will be translated to:

``` ocaml
type int = Z | S int	
val n : int

match n with
| Z -> e0
| S a -> match a with
  | Z -> e1
  | S b -> match b with
    | Z -> e2
    | m -> e3
    end
  end
end
```

### Refunctionalization (`refun`)
The refunctionalization is an experimental derivation which tries to build a CPS-program from it's defunctionalized form.
The idea is to find types that are destructed in a single place in the program.
The constructors of this type are replaced by functions and the apply function are removed.

## Direct Style (`direct`)
Translate a CPS program back to direct style.
It does not handle complex cases where control operators are needed.

## C Backend (`c`)
The generated abstract machine can be compiled to C code.
You'll need to write an implementation of the unspecified types and terms.
Types must have the same size as `void*`.


### Variant types
Variant types are compiled constructor by construtor. Let's take an example:

``` ocaml
type term =
| Int int
| Add (term, term)
```

``` c
enum skel_term {
	skel_Int,
	skel_Add
};

struct skel_termInt {
	enum skel_term tag;
	void* data0;
};
struct skel_termInt* mk_skel_termInt(void* data0);

struct skel_termAdd {
	enum skel_term tag;
	void* data0;
	void* data1;
};

struct skel_termAdd* mk_skel_termAdd(void* data0, void* data1);
```

The first field of each structure is a tag, reprensenting the data constructor.
The data is stored next to the tag, as represented below:

![](./c_constructor.png)

Cells ares variable-sized.

Then, pattern matching is compiled in the following way:

``` ocaml
match term with
| Add (left, right) → ...
| Int (n) → ...

```


``` c
switch (*((enum skel_term *)term)) {
  case skel_Add: {
    void *left = ((struct skel_termAdd *)term)->data0;
    void *right = ((struct skel_termAdd *)term)->data1;
    ...
  }
  case skel_Int: {
    void *n = ((struct skel_termInt *)term)->data0;
    ...
  }
}
```

