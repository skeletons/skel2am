(* The IMP language *)

type int

val int_add : (int, int) -> int
val int_mul : (int, int) -> int
val int_sub : (int, int) -> int

val int_lt : (int, int) -> bool
val int_eq : (int, int) -> bool

type var


type aexp =
  | Int int
  | Var var
  | BinOp (abinop, aexp, aexp)

type bexp =
  | True
  | False
  | Comp (cbinop, aexp, aexp)
  | BoolOp (bbinop, bexp, bexp)
  | Not bexp

type abinop =
  | Add
  | Mul
  | Sub

(** __atomic__ *)
val apply_aop ((op: abinop), (n0: int), (n1: int)) : int =
  match op with
  | Add -> int_add (n0, n1)
  | Mul -> int_mul (n0, n1)
  | Sub -> int_sub (n0, n1)
  end

type bbinop =
  | Or
  | And

(** __atomic__ *)
val apply_bop ((op: bbinop), (b0: bool), (b1: bool)) : bool =
  match op with
  | Or -> bool_or (b0, b1)
  | And -> bool_and (b0, b1)
  end

type cbinop =
  | LT
  | EQ

(** __atomic__ *)
val apply_cop ((op: cbinop), (n0: int), (n1: int)) : bool =
  match op with
  | LT -> int_lt(n0, n1)
  | EQ -> int_eq(n0, n1)
  end

type com =
  | Skip
  | Assign (var, aexp)
  | Seq (com, com)
  | IfThenElse (bexp, com, com)
  | While (bexp, com)


(* Env *)
type env
val empty : env
val lookup : (env, var) -> int
val extend : (env, var, int) -> env


(* Booleans *)
type bool
val true : bool
val false : bool
val is_true : bool -> ()
val is_false : bool -> ()
val bool_or : (bool, bool) -> bool
val bool_and : (bool, bool) -> bool


val eval_aexp ((a: aexp), (e: env)) : int =
  match a with
  | Int n -> n
  | Var x -> lookup (e, x)
  | BinOp (op, a0, a1) ->
     let n0 = eval_aexp (a0, e) in
     let n1 = eval_aexp (a1, e) in
     apply_aop (op, n0, n1)
  end

val eval_bexp ((b: bexp), (e: env)) : bool =
  match b with
  | True -> true
  | False -> false
  | Comp (op, a0, a1) ->
     let n0 = eval_aexp (a0, e) in
     let n1 = eval_aexp (a1, e) in
     apply_cop (op, n0, n1)
  | BoolOp (op, b0, b1) ->
     let b0' = eval_bexp (b0, e) in
     let b1' = eval_bexp (b1, e) in
     apply_bop (op, b0', b1')
  | Not b ->
     let b' = eval_bexp (b, e) in
     branch is_true b'; false or is_false b'; true end
  end

val eval_com ((c: com), (e: env)) : env =
  match c with
  | Skip -> e
  | Assign (x, a) ->
     let n = eval_aexp (a, e) in
     extend (e, x, n)
  | Seq (c0, c1) ->
     let e' = eval_com (c0, e) in
     eval_com (c1, e')
  | IfThenElse (b, c0, c1) ->
     let b' = eval_bexp (b, e) in
     branch
       is_true b';
       eval_com (c0, e)
     or
       is_false b';
       eval_com (c1, e)
     end
  | While (b, c) ->
     let b' = eval_bexp (b, e) in
     branch
       is_false b';
       e
     or
       is_true b';
       let e' = eval_com (c, e) in
       eval_com (c, e')
     end
  end

(** __main__ *)
val main (c: com) : env =
  eval_com (c, empty)
