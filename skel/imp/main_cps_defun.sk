
type arrow_4 =
| Constr16 (aexp, env, arrow_4, abinop)
| Constr15 (arrow_4, int, abinop)
| Constr11 (aexp, env, arrow_7, cbinop)
| Constr10 (arrow_7, int, cbinop)
| Constr3 (env, arrow_2, var)

val apply_4 (fun_arg: (arrow_4, int)): env =
	match fun_arg with
	| (Constr16 (a1, e, k, op), n0) → eval_aexp (a1, e, Constr15 (k, n0, op))
	| (Constr15 (k, n0, op), n1) →
		let res = apply_aop (op, n0, n1) in
		apply_4 (k, res)
	| (Constr11 (a1, e, k, op), n0) → eval_aexp (a1, e, Constr10 (k, n0, op))
	| (Constr10 (k, n0, op), n1) →
		let res = apply_cop (op, n0, n1) in
		apply_7 (k, res)
	| (Constr3 (e, k, x), n) →
		let res = extend (e, x, n) in
		apply_2 (k, res)
	end

type arrow_2 =
| Constr8 (com, arrow_2)
| Constr5 (com, arrow_2)
| Constr1

val apply_2 (fun_arg: (arrow_2, env)): env =
	match fun_arg with
	| (Constr8 (c, k), e') → eval_com (c, e', k)
	| (Constr5 (c1, k), e') → eval_com (c1, e', k)
	| (Constr1, x) → x
	end

type arrow_7 =
| Constr14 (arrow_7)
| Constr13 (bexp, env, arrow_7, bbinop)
| Constr12 (bool, arrow_7, bbinop)
| Constr9 (com, env, arrow_2)
| Constr6 (com, com, env, arrow_2)

val apply_7 (fun_arg: (arrow_7, bool)): env =
	match fun_arg with
	| (Constr14 (k), b') →
		branch
			is_true b' ;
			apply_7 (k, false)
		or
			is_false b' ;
			apply_7 (k, true)
		end
	| (Constr13 (b1, e, k, op), b0') → eval_bexp (b1, e, Constr12 (b0', k, op))
	| (Constr12 (b0', k, op), b1') →
		let res = apply_bop (op, b0', b1') in
		apply_7 (k, res)
	| (Constr9 (c, e, k), b') →
		branch
			is_false b' ;
			apply_2 (k, e)
		or
			is_true b' ;
			eval_com (c, e, Constr8 (c, k))
		end
	| (Constr6 (c0, c1, e, k), b') →
		branch
			is_true b' ;
			eval_com (c0, e, k)
		or
			is_false b' ;
			eval_com (c1, e, k)
		end
	end

type int

val int_add : (int, int) → int

val int_mul : (int, int) → int

val int_sub : (int, int) → int

val int_lt : (int, int) → bool

val int_eq : (int, int) → bool

type var

type aexp =
| Int int
| Var var
| BinOp (abinop, aexp, aexp)

type bexp =
| True
| False
| Comp (cbinop, aexp, aexp)
| BoolOp (bbinop, bexp, bexp)
| Not bexp

type abinop =
| Add
| Mul
| Sub

(** __atomic__ *)
val apply_aop ((op: abinop), (n0: int), (n1: int)): int =
	match op with
	| Add → int_add (n0, n1)
	| Mul → int_mul (n0, n1)
	| Sub → int_sub (n0, n1)
	end

type bbinop =
| Or
| And

(** __atomic__ *)
val apply_bop ((op: bbinop), (b0: bool), (b1: bool)): bool =
	match op with
	| Or → bool_or (b0, b1)
	| And → bool_and (b0, b1)
	end

type cbinop =
| LT
| EQ

(** __atomic__ *)
val apply_cop ((op: cbinop), (n0: int), (n1: int)): bool =
	match op with
	| LT → int_lt (n0, n1)
	| EQ → int_eq (n0, n1)
	end

type com =
| Skip
| Assign (var, aexp)
| Seq (com, com)
| IfThenElse (bexp, com, com)
| While (bexp, com)

type env

val empty : env

val lookup : (env, var) → int

val extend : (env, var, int) → env

type bool

val true : bool

val false : bool

val is_true : bool → ()

val is_false : bool → ()

val bool_or : (bool, bool) → bool

val bool_and : (bool, bool) → bool

val eval_aexp ((a: aexp), (e: env), (k: arrow_4)): env =
	match a with
	| Int n → apply_4 (k, n)
	| Var x →
		let res = lookup (e, x) in
		apply_4 (k, res)
	| BinOp (op, a0, a1) → eval_aexp (a0, e, Constr16 (a1, e, k, op))
	end

val eval_bexp ((b: bexp), (e: env), (k: arrow_7)): env =
	match b with
	| True → apply_7 (k, true)
	| False → apply_7 (k, false)
	| Comp (op, a0, a1) → eval_aexp (a0, e, Constr11 (a1, e, k, op))
	| BoolOp (op, b0, b1) → eval_bexp (b0, e, Constr13 (b1, e, k, op))
	| Not b → eval_bexp (b, e, Constr14 (k))
	end

val eval_com ((c: com), (e: env), (k: arrow_2)): env =
	match c with
	| Skip → apply_2 (k, e)
	| Assign (x, a) → eval_aexp (a, e, Constr3 (e, k, x))
	| Seq (c0, c1) → eval_com (c0, e, Constr5 (c1, k))
	| IfThenElse (b, c0, c1) → eval_bexp (b, e, Constr6 (c0, c1, e, k))
	| While (b, c) → eval_bexp (b, e, Constr9 (c, e, k))
	end

(** __main__ *)
val main (c: com): env =
	eval_com (c, empty, Constr1)

