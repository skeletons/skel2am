#include <stdlib.h>

enum skel_com {
	skel_Skip,
	skel_Assign,
	skel_Seq,
	skel_IfThenElse,
	skel_While
};

struct skel_comSkip {
	enum skel_com tag;
};


struct skel_comSkip* mk_skel_comSkip(void) {
	struct skel_comSkip* res = malloc(sizeof(struct skel_comSkip));
	res->tag = skel_Skip;
	return res;
}

struct skel_comAssign {
	enum skel_com tag;
	void* data0;
	void* data1;
};


struct skel_comAssign* mk_skel_comAssign(void* data0, void* data1) {
	struct skel_comAssign* res = malloc(sizeof(struct skel_comAssign));
	res->tag = skel_Assign;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

struct skel_comSeq {
	enum skel_com tag;
	void* data0;
	void* data1;
};


struct skel_comSeq* mk_skel_comSeq(void* data0, void* data1) {
	struct skel_comSeq* res = malloc(sizeof(struct skel_comSeq));
	res->tag = skel_Seq;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

struct skel_comIfThenElse {
	enum skel_com tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_comIfThenElse* mk_skel_comIfThenElse(void* data0, void* data1, void* data2) {
	struct skel_comIfThenElse* res = malloc(sizeof(struct skel_comIfThenElse));
	res->tag = skel_IfThenElse;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

struct skel_comWhile {
	enum skel_com tag;
	void* data0;
	void* data1;
};


struct skel_comWhile* mk_skel_comWhile(void* data0, void* data1) {
	struct skel_comWhile* res = malloc(sizeof(struct skel_comWhile));
	res->tag = skel_While;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

enum skel_cbinop {
	skel_LT,
	skel_EQ
};

struct skel_cbinopLT {
	enum skel_cbinop tag;
};


struct skel_cbinopLT* mk_skel_cbinopLT(void) {
	struct skel_cbinopLT* res = malloc(sizeof(struct skel_cbinopLT));
	res->tag = skel_LT;
	return res;
}

struct skel_cbinopEQ {
	enum skel_cbinop tag;
};


struct skel_cbinopEQ* mk_skel_cbinopEQ(void) {
	struct skel_cbinopEQ* res = malloc(sizeof(struct skel_cbinopEQ));
	res->tag = skel_EQ;
	return res;
}

enum skel_bexp {
	skel_True,
	skel_False,
	skel_Comp,
	skel_BoolOp,
	skel_Not
};

struct skel_bexpTrue {
	enum skel_bexp tag;
};


struct skel_bexpTrue* mk_skel_bexpTrue(void) {
	struct skel_bexpTrue* res = malloc(sizeof(struct skel_bexpTrue));
	res->tag = skel_True;
	return res;
}

struct skel_bexpFalse {
	enum skel_bexp tag;
};


struct skel_bexpFalse* mk_skel_bexpFalse(void) {
	struct skel_bexpFalse* res = malloc(sizeof(struct skel_bexpFalse));
	res->tag = skel_False;
	return res;
}

struct skel_bexpComp {
	enum skel_bexp tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_bexpComp* mk_skel_bexpComp(void* data0, void* data1, void* data2) {
	struct skel_bexpComp* res = malloc(sizeof(struct skel_bexpComp));
	res->tag = skel_Comp;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

struct skel_bexpBoolOp {
	enum skel_bexp tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_bexpBoolOp* mk_skel_bexpBoolOp(void* data0, void* data1, void* data2) {
	struct skel_bexpBoolOp* res = malloc(sizeof(struct skel_bexpBoolOp));
	res->tag = skel_BoolOp;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

struct skel_bexpNot {
	enum skel_bexp tag;
	void* data0;
};


struct skel_bexpNot* mk_skel_bexpNot(void* data0) {
	struct skel_bexpNot* res = malloc(sizeof(struct skel_bexpNot));
	res->tag = skel_Not;
	res->data0 = data0;
	return res;
}

enum skel_bbinop {
	skel_Or,
	skel_And
};

struct skel_bbinopOr {
	enum skel_bbinop tag;
};


struct skel_bbinopOr* mk_skel_bbinopOr(void) {
	struct skel_bbinopOr* res = malloc(sizeof(struct skel_bbinopOr));
	res->tag = skel_Or;
	return res;
}

struct skel_bbinopAnd {
	enum skel_bbinop tag;
};


struct skel_bbinopAnd* mk_skel_bbinopAnd(void) {
	struct skel_bbinopAnd* res = malloc(sizeof(struct skel_bbinopAnd));
	res->tag = skel_And;
	return res;
}

enum skel_arrow_7 {
	skel_Constr14,
	skel_Constr13,
	skel_Constr12,
	skel_Constr9,
	skel_Constr6
};

struct skel_arrow_7Constr14 {
	enum skel_arrow_7 tag;
	void* data0;
};


struct skel_arrow_7Constr14* mk_skel_arrow_7Constr14(void* data0) {
	struct skel_arrow_7Constr14* res = malloc(sizeof(struct skel_arrow_7Constr14));
	res->tag = skel_Constr14;
	res->data0 = data0;
	return res;
}

struct skel_arrow_7Constr13 {
	enum skel_arrow_7 tag;
	void* data0;
	void* data1;
	void* data2;
	void* data3;
};


struct skel_arrow_7Constr13* mk_skel_arrow_7Constr13(void* data0, void* data1, void* data2, void* data3) {
	struct skel_arrow_7Constr13* res = malloc(sizeof(struct skel_arrow_7Constr13));
	res->tag = skel_Constr13;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	res->data3 = data3;
	return res;
}

struct skel_arrow_7Constr12 {
	enum skel_arrow_7 tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_arrow_7Constr12* mk_skel_arrow_7Constr12(void* data0, void* data1, void* data2) {
	struct skel_arrow_7Constr12* res = malloc(sizeof(struct skel_arrow_7Constr12));
	res->tag = skel_Constr12;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

struct skel_arrow_7Constr9 {
	enum skel_arrow_7 tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_arrow_7Constr9* mk_skel_arrow_7Constr9(void* data0, void* data1, void* data2) {
	struct skel_arrow_7Constr9* res = malloc(sizeof(struct skel_arrow_7Constr9));
	res->tag = skel_Constr9;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

struct skel_arrow_7Constr6 {
	enum skel_arrow_7 tag;
	void* data0;
	void* data1;
	void* data2;
	void* data3;
};


struct skel_arrow_7Constr6* mk_skel_arrow_7Constr6(void* data0, void* data1, void* data2, void* data3) {
	struct skel_arrow_7Constr6* res = malloc(sizeof(struct skel_arrow_7Constr6));
	res->tag = skel_Constr6;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	res->data3 = data3;
	return res;
}

enum skel_arrow_4 {
	skel_Constr16,
	skel_Constr15,
	skel_Constr11,
	skel_Constr10,
	skel_Constr3
};

struct skel_arrow_4Constr16 {
	enum skel_arrow_4 tag;
	void* data0;
	void* data1;
	void* data2;
	void* data3;
};


struct skel_arrow_4Constr16* mk_skel_arrow_4Constr16(void* data0, void* data1, void* data2, void* data3) {
	struct skel_arrow_4Constr16* res = malloc(sizeof(struct skel_arrow_4Constr16));
	res->tag = skel_Constr16;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	res->data3 = data3;
	return res;
}

struct skel_arrow_4Constr15 {
	enum skel_arrow_4 tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_arrow_4Constr15* mk_skel_arrow_4Constr15(void* data0, void* data1, void* data2) {
	struct skel_arrow_4Constr15* res = malloc(sizeof(struct skel_arrow_4Constr15));
	res->tag = skel_Constr15;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

struct skel_arrow_4Constr11 {
	enum skel_arrow_4 tag;
	void* data0;
	void* data1;
	void* data2;
	void* data3;
};


struct skel_arrow_4Constr11* mk_skel_arrow_4Constr11(void* data0, void* data1, void* data2, void* data3) {
	struct skel_arrow_4Constr11* res = malloc(sizeof(struct skel_arrow_4Constr11));
	res->tag = skel_Constr11;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	res->data3 = data3;
	return res;
}

struct skel_arrow_4Constr10 {
	enum skel_arrow_4 tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_arrow_4Constr10* mk_skel_arrow_4Constr10(void* data0, void* data1, void* data2) {
	struct skel_arrow_4Constr10* res = malloc(sizeof(struct skel_arrow_4Constr10));
	res->tag = skel_Constr10;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

struct skel_arrow_4Constr3 {
	enum skel_arrow_4 tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_arrow_4Constr3* mk_skel_arrow_4Constr3(void* data0, void* data1, void* data2) {
	struct skel_arrow_4Constr3* res = malloc(sizeof(struct skel_arrow_4Constr3));
	res->tag = skel_Constr3;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

enum skel_arrow_2 {
	skel_Constr8,
	skel_Constr5,
	skel_Constr1
};

struct skel_arrow_2Constr8 {
	enum skel_arrow_2 tag;
	void* data0;
	void* data1;
};


struct skel_arrow_2Constr8* mk_skel_arrow_2Constr8(void* data0, void* data1) {
	struct skel_arrow_2Constr8* res = malloc(sizeof(struct skel_arrow_2Constr8));
	res->tag = skel_Constr8;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

struct skel_arrow_2Constr5 {
	enum skel_arrow_2 tag;
	void* data0;
	void* data1;
};


struct skel_arrow_2Constr5* mk_skel_arrow_2Constr5(void* data0, void* data1) {
	struct skel_arrow_2Constr5* res = malloc(sizeof(struct skel_arrow_2Constr5));
	res->tag = skel_Constr5;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

struct skel_arrow_2Constr1 {
	enum skel_arrow_2 tag;
};


struct skel_arrow_2Constr1* mk_skel_arrow_2Constr1(void) {
	struct skel_arrow_2Constr1* res = malloc(sizeof(struct skel_arrow_2Constr1));
	res->tag = skel_Constr1;
	return res;
}

enum skel_aexp {
	skel_Int,
	skel_Var,
	skel_BinOp
};

struct skel_aexpInt {
	enum skel_aexp tag;
	void* data0;
};


struct skel_aexpInt* mk_skel_aexpInt(void* data0) {
	struct skel_aexpInt* res = malloc(sizeof(struct skel_aexpInt));
	res->tag = skel_Int;
	res->data0 = data0;
	return res;
}

struct skel_aexpVar {
	enum skel_aexp tag;
	void* data0;
};


struct skel_aexpVar* mk_skel_aexpVar(void* data0) {
	struct skel_aexpVar* res = malloc(sizeof(struct skel_aexpVar));
	res->tag = skel_Var;
	res->data0 = data0;
	return res;
}

struct skel_aexpBinOp {
	enum skel_aexp tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_aexpBinOp* mk_skel_aexpBinOp(void* data0, void* data1, void* data2) {
	struct skel_aexpBinOp* res = malloc(sizeof(struct skel_aexpBinOp));
	res->tag = skel_BinOp;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

enum skel_abinop {
	skel_Add,
	skel_Mul,
	skel_Sub
};

struct skel_abinopAdd {
	enum skel_abinop tag;
};


struct skel_abinopAdd* mk_skel_abinopAdd(void) {
	struct skel_abinopAdd* res = malloc(sizeof(struct skel_abinopAdd));
	res->tag = skel_Add;
	return res;
}

struct skel_abinopMul {
	enum skel_abinop tag;
};


struct skel_abinopMul* mk_skel_abinopMul(void) {
	struct skel_abinopMul* res = malloc(sizeof(struct skel_abinopMul));
	res->tag = skel_Mul;
	return res;
}

struct skel_abinopSub {
	enum skel_abinop tag;
};


struct skel_abinopSub* mk_skel_abinopSub(void) {
	struct skel_abinopSub* res = malloc(sizeof(struct skel_abinopSub));
	res->tag = skel_Sub;
	return res;
}

void* skel_main(void*);
void* skel_lookup(void*, void*);
void* skel_is_true(void*);
void* skel_is_false(void*);
void* skel_int_sub(void*, void*);
void* skel_int_mul(void*, void*);
void* skel_int_lt(void*, void*);
void* skel_int_eq(void*, void*);
void* skel_int_add(void*, void*);
void* skel_extend(void*, void*, void*);
void* skel_eval_com(void*, void*, void*);
void* skel_eval_bexp(void*, void*, void*);
void* skel_eval_aexp(void*, void*, void*);
void* skel_bool_or(void*, void*);
void* skel_bool_and(void*, void*);
void* skel_apply_cop(void*, void*, void*);
void* skel_apply_bop(void*, void*, void*);
void* skel_apply_aop(void*, void*, void*);
void* skel_apply_7(void*, void*);
void* skel_apply_4(void*, void*);
void* skel_apply_2(void*, void*);

void* skel_main(void* c) {
	return skel_eval_com(c, NULL, mk_skel_arrow_2Constr1());
}


void* skel_eval_com(void* c, void* e, void* k) {
	switch (*((enum skel_com*) c)) {
	case skel_Assign: {
	void* x = ((struct skel_comAssign*) c)->data0;
	void* a = ((struct skel_comAssign*) c)->data1;
	return skel_eval_aexp(a, e, mk_skel_arrow_4Constr3(e, k, x));
	}
	case skel_IfThenElse: {
	void* b = ((struct skel_comIfThenElse*) c)->data0;
	void* c0 = ((struct skel_comIfThenElse*) c)->data1;
	void* c1 = ((struct skel_comIfThenElse*) c)->data2;
	return skel_eval_bexp(b, e, mk_skel_arrow_7Constr6(c0, c1, e, k));
	}
	case skel_Seq: {
	void* c0 = ((struct skel_comSeq*) c)->data0;
	void* c1 = ((struct skel_comSeq*) c)->data1;
	return skel_eval_com(c0, e, mk_skel_arrow_2Constr5(c1, k));
	}
	case skel_Skip: {
	return skel_apply_2(k, e);
	}
	case skel_While: {
	void* b = ((struct skel_comWhile*) c)->data0;
	void* c = ((struct skel_comWhile*) c)->data1;
	return skel_eval_bexp(b, e, mk_skel_arrow_7Constr9(c, e, k));
	}
}

}


void* skel_eval_bexp(void* b, void* e, void* k) {
	switch (*((enum skel_bexp*) b)) {
	case skel_BoolOp: {
	void* op = ((struct skel_bexpBoolOp*) b)->data0;
	void* b0 = ((struct skel_bexpBoolOp*) b)->data1;
	void* b1 = ((struct skel_bexpBoolOp*) b)->data2;
	return skel_eval_bexp(b0, e, mk_skel_arrow_7Constr13(b1, e, k, op));
	}
	case skel_Comp: {
	void* op = ((struct skel_bexpComp*) b)->data0;
	void* a0 = ((struct skel_bexpComp*) b)->data1;
	void* a1 = ((struct skel_bexpComp*) b)->data2;
	return skel_eval_aexp(a0, e, mk_skel_arrow_4Constr11(a1, e, k, op));
	}
	case skel_False: {
	return skel_apply_7(k, NULL);
	}
	case skel_Not: {
	void* b = ((struct skel_bexpNot*) b)->data0;
	return skel_eval_bexp(b, e, mk_skel_arrow_7Constr14(k));
	}
	case skel_True: {
	return skel_apply_7(k, NULL);
	}
}

}


void* skel_eval_aexp(void* a, void* e, void* k) {
	switch (*((enum skel_aexp*) a)) {
	case skel_BinOp: {
	void* op = ((struct skel_aexpBinOp*) a)->data0;
	void* a0 = ((struct skel_aexpBinOp*) a)->data1;
	void* a1 = ((struct skel_aexpBinOp*) a)->data2;
	return skel_eval_aexp(a0, e, mk_skel_arrow_4Constr16(a1, e, k, op));
	}
	case skel_Int: {
	void* n = ((struct skel_aexpInt*) a)->data0;
	return skel_apply_4(k, n);
	}
	case skel_Var: {
	void* x = ((struct skel_aexpVar*) a)->data0;
	void* res = skel_lookup(e, x);
	return skel_apply_4(k, res);
	}
}

}


void* skel_apply_cop(void* op, void* n0, void* n1) {
	switch (*((enum skel_cbinop*) op)) {
	case skel_EQ: {
	return skel_int_eq(n0, n1);
	}
	case skel_LT: {
	return skel_int_lt(n0, n1);
	}
}

}


void* skel_apply_bop(void* op, void* b0, void* b1) {
	switch (*((enum skel_bbinop*) op)) {
	case skel_And: {
	return skel_bool_and(b0, b1);
	}
	case skel_Or: {
	return skel_bool_or(b0, b1);
	}
}

}


void* skel_apply_aop(void* op, void* n0, void* n1) {
	switch (*((enum skel_abinop*) op)) {
	case skel_Add: {
	return skel_int_add(n0, n1);
	}
	case skel_Mul: {
	return skel_int_mul(n0, n1);
	}
	case skel_Sub: {
	return skel_int_sub(n0, n1);
	}
}

}


void* skel_apply_7(void* fun_arg0, void* fun_arg1) {
	switch (*((enum skel_arrow_7*) fun_arg0)) {
	case skel_Constr12: {
	void* b0' = ((struct skel_arrow_7Constr12*) fun_arg0)->data0;
	void* k = ((struct skel_arrow_7Constr12*) fun_arg0)->data1;
	void* op = ((struct skel_arrow_7Constr12*) fun_arg0)->data2;
	void* res = skel_apply_bop(op, b0', fun_arg1);
	return skel_apply_7(k, res);
	}
	case skel_Constr13: {
	void* b1 = ((struct skel_arrow_7Constr13*) fun_arg0)->data0;
	void* e = ((struct skel_arrow_7Constr13*) fun_arg0)->data1;
	void* k = ((struct skel_arrow_7Constr13*) fun_arg0)->data2;
	void* op = ((struct skel_arrow_7Constr13*) fun_arg0)->data3;
	return skel_eval_bexp(b1, e, mk_skel_arrow_7Constr12(fun_arg1, k, op));
	}
	case skel_Constr14: {
	void* k = ((struct skel_arrow_7Constr14*) fun_arg0)->data0;
	}
	case skel_Constr6: {
	void* c0 = ((struct skel_arrow_7Constr6*) fun_arg0)->data0;
	void* c1 = ((struct skel_arrow_7Constr6*) fun_arg0)->data1;
	void* e = ((struct skel_arrow_7Constr6*) fun_arg0)->data2;
	void* k = ((struct skel_arrow_7Constr6*) fun_arg0)->data3;
	}
	case skel_Constr9: {
	void* c = ((struct skel_arrow_7Constr9*) fun_arg0)->data0;
	void* e = ((struct skel_arrow_7Constr9*) fun_arg0)->data1;
	void* k = ((struct skel_arrow_7Constr9*) fun_arg0)->data2;
	}
}

}


void* skel_apply_4(void* fun_arg0, void* fun_arg1) {
	switch (*((enum skel_arrow_4*) fun_arg0)) {
	case skel_Constr10: {
	void* k = ((struct skel_arrow_4Constr10*) fun_arg0)->data0;
	void* n0 = ((struct skel_arrow_4Constr10*) fun_arg0)->data1;
	void* op = ((struct skel_arrow_4Constr10*) fun_arg0)->data2;
	void* res = skel_apply_cop(op, n0, fun_arg1);
	return skel_apply_7(k, res);
	}
	case skel_Constr11: {
	void* a1 = ((struct skel_arrow_4Constr11*) fun_arg0)->data0;
	void* e = ((struct skel_arrow_4Constr11*) fun_arg0)->data1;
	void* k = ((struct skel_arrow_4Constr11*) fun_arg0)->data2;
	void* op = ((struct skel_arrow_4Constr11*) fun_arg0)->data3;
	return skel_eval_aexp(a1, e, mk_skel_arrow_4Constr10(k, fun_arg1, op));
	}
	case skel_Constr15: {
	void* k = ((struct skel_arrow_4Constr15*) fun_arg0)->data0;
	void* n0 = ((struct skel_arrow_4Constr15*) fun_arg0)->data1;
	void* op = ((struct skel_arrow_4Constr15*) fun_arg0)->data2;
	void* res = skel_apply_aop(op, n0, fun_arg1);
	return skel_apply_4(k, res);
	}
	case skel_Constr16: {
	void* a1 = ((struct skel_arrow_4Constr16*) fun_arg0)->data0;
	void* e = ((struct skel_arrow_4Constr16*) fun_arg0)->data1;
	void* k = ((struct skel_arrow_4Constr16*) fun_arg0)->data2;
	void* op = ((struct skel_arrow_4Constr16*) fun_arg0)->data3;
	return skel_eval_aexp(a1, e, mk_skel_arrow_4Constr15(k, fun_arg1, op));
	}
	case skel_Constr3: {
	void* e = ((struct skel_arrow_4Constr3*) fun_arg0)->data0;
	void* k = ((struct skel_arrow_4Constr3*) fun_arg0)->data1;
	void* x = ((struct skel_arrow_4Constr3*) fun_arg0)->data2;
	void* res = skel_extend(e, x, fun_arg1);
	return skel_apply_2(k, res);
	}
}

}


void* skel_apply_2(void* fun_arg0, void* fun_arg1) {
	switch (*((enum skel_arrow_2*) fun_arg0)) {
	case skel_Constr1: {
	return fun_arg1;
	}
	case skel_Constr5: {
	void* c1 = ((struct skel_arrow_2Constr5*) fun_arg0)->data0;
	void* k = ((struct skel_arrow_2Constr5*) fun_arg0)->data1;
	return skel_eval_com(c1, fun_arg1, k);
	}
	case skel_Constr8: {
	void* c = ((struct skel_arrow_2Constr8*) fun_arg0)->data0;
	void* k = ((struct skel_arrow_2Constr8*) fun_arg0)->data1;
	return skel_eval_com(c, fun_arg1, k);
	}
}

}
