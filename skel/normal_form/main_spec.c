#include <stdlib.h>

#include "unspec.h"

enum skel_term {
	skel_Var,
	skel_App,
	skel_Abs
};

struct skel_termVar {
	enum skel_term tag;
	void* data0;
};

struct skel_termVar* mk_skel_termVar(void* data0) {
	struct skel_termVar* res = malloc(sizeof(struct skel_termVar));
	res->tag = skel_Var;
	res->data0 = data0;
	return res;
}

struct skel_termApp {
	enum skel_term tag;
	void* data0;
	void* data1;
};

struct skel_termApp* mk_skel_termApp(void* data0, void* data1) {
	struct skel_termApp* res = malloc(sizeof(struct skel_termApp));
	res->tag = skel_App;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

struct skel_termAbs {
	enum skel_term tag;
	void* data0;
	void* data1;
};

struct skel_termAbs* mk_skel_termAbs(void* data0, void* data1) {
	struct skel_termAbs* res = malloc(sizeof(struct skel_termAbs));
	res->tag = skel_Abs;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

enum skel_arrow_1 {
	skel_Constr5,
	skel_Constr4,
	skel_Constr3,
	skel_Constr2
};

struct skel_arrow_1Constr5 {
	enum skel_arrow_1 tag;
};

struct skel_arrow_1Constr5* mk_skel_arrow_1Constr5() {
	struct skel_arrow_1Constr5* res = malloc(sizeof(struct skel_arrow_1Constr5));
	res->tag = skel_Constr5;
	return res;
}

struct skel_arrow_1Constr4 {
	enum skel_arrow_1 tag;
	void* data0;
	void* data1;
};

struct skel_arrow_1Constr4* mk_skel_arrow_1Constr4(void* data0, void* data1) {
	struct skel_arrow_1Constr4* res = malloc(sizeof(struct skel_arrow_1Constr4));
	res->tag = skel_Constr4;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

struct skel_arrow_1Constr3 {
	enum skel_arrow_1 tag;
	void* data0;
	void* data1;
};

struct skel_arrow_1Constr3* mk_skel_arrow_1Constr3(void* data0, void* data1) {
	struct skel_arrow_1Constr3* res = malloc(sizeof(struct skel_arrow_1Constr3));
	res->tag = skel_Constr3;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

struct skel_arrow_1Constr2 {
	enum skel_arrow_1 tag;
	void* data0;
	void* data1;
};

struct skel_arrow_1Constr2* mk_skel_arrow_1Constr2(void* data0, void* data1) {
	struct skel_arrow_1Constr2* res = malloc(sizeof(struct skel_arrow_1Constr2));
	res->tag = skel_Constr2;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}
