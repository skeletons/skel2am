(** This file was automatically generated using necroml
	See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type _ env
  type string
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: unit -> 'a t
  val apply: ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  (** __protected__ *)
  type value =
  | VAR of string
  | LAM of (string * term)
  (** __protected__ *)
  and term =
  | VALUE of value
  | COMP of comp
  (** __protected__ *)
  and expval =
  | CLOSURE of (string * term * expval env)
  (** __protected__ *)
  and comp =
  | APP of (term * term)

  val empty: 'a env
  val extend: string * 'a * 'a env -> ('a env) M.t
  val lookup: 'a env * string -> 'a M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type value =
  | VAR of string
  | LAM of (string * term)
  and term =
  | VALUE of value
  | COMP of comp
  and expval =
  | CLOSURE of (string * term * expval env)
  and comp =
  | APP of (term * term)

  let extend _ = raise (NotImplemented "extend")
  let lookup _ = raise (NotImplemented "lookup")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type _ env
  type string

  type value =
  | VAR of string
  | LAM of (string * term)
  and term =
  | VALUE of value
  | COMP of comp
  and expval =
  | CLOSURE of (string * term * expval env)
  and comp =
  | APP of (term * term)

  val empty: 'a env
  val eval: term * expval env * (expval -> expval M.t) -> expval M.t
  (** __atomic__ *)
  val eval_value: value * expval env -> expval M.t
  val extend: string * 'a * 'a env -> ('a env) M.t
  val lookup: 'a env * string -> 'a M.t
  (** __main__ *)
  val main: term -> expval M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  let rec eval =
    function (t, e, k) ->
    let _tmp = t in
    begin match _tmp with
    | COMP tmp1 ->
        let _tmp = tmp1 in
        begin match _tmp with
        | APP (t0, t1) ->
            M.apply eval (t0, e, function arg2 ->
              let _tmp = arg2 in
              begin match _tmp with
              | CLOSURE (x, t, e_) ->
                  M.apply eval (t1, e, function arg3 ->
                    let* e__ = M.apply extend (x, arg3, e_) in
                    M.apply eval (t, e__, k))
              end)
        end
    | VALUE v ->
        let* res = M.apply eval_value (v, e) in
        M.apply k res
    end
  (** __atomic__ *)
  and eval_value =
    function (v, e) ->
    let _tmp = v in
    begin match _tmp with
    | LAM (x, t) -> M.ret (CLOSURE (x, t, e))
    | VAR x -> M.apply lookup (e, x)
    end
  (** __main__ *)
  and main t =
    M.apply eval (t, empty, function arg1 ->
      M.ret arg1)
end
