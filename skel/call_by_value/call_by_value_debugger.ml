open Necromonads
module M = ContPoly
module SMap = Map.Make (String)

module rec Types : sig
  type 'a env = 'a SMap.t

  type string = String.t
end =
  Types

open Main_cps_defun_smallstep_bis

module Spec = struct
  include Unspec (M) (Types)

  let empty = SMap.empty

  let extend (k, v, e) = M.ret @@ SMap.add k v e

  let lookup (e, k) = M.ret @@ SMap.find k e
end

open MakeInterpreter (Spec)

module PP = struct
  let rec config fmt c =
    match c with
    | Main t ->
        Printf.fprintf fmt "Main: <%a>\n" term t
    | Apply_2 (k, v) ->
        Printf.fprintf fmt "Apply_2: %a\n  %a\n" expval v arrow_2 k
    | Eval (t, e, k) ->
        Printf.fprintf fmt "Eval: <%a>\n  { %a }\n  %a\n" term t env e arrow_2 k
    | Stop v ->
        Printf.fprintf fmt "Stop: %a" expval v

  and term fmt t =
    match t with
    | VALUE (VAR s) ->
        Printf.fprintf fmt "%s" s
    | VALUE (LAM (s, t)) ->
        Printf.fprintf fmt "(\\%s -> %a)" s term t
    | COMP (APP (t1, t2)) ->
        Printf.fprintf fmt "(%a %a)" term t1 term t2

  and expval fmt (CLOSURE (s, t, e)) =
    Printf.fprintf fmt "<\\%s -> %a | {%a}>" s term t env e

  and env' ?(void = "") fmt l =
    match l with
    | [] ->
        Printf.fprintf fmt "%s" void
    | [(s, t)] ->
        Printf.fprintf fmt "%s: %a" s expval t
    | (s, t) :: elts ->
        Printf.fprintf fmt "%s: %a; %a" s expval t (env' ~void:"") elts

  and env fmt e = env' fmt (SMap.bindings e)

  and arrow_2 fmt k =
    match k with
    | Constr1 ->
        Printf.fprintf fmt "[]"
    | Constr3 (e, k, t, s) ->
        Printf.fprintf fmt "<f \\%s -> %a | {%a}> :: %a" s term t env e arrow_2
          k
    | Constr4 (e, k, t) ->
        Printf.fprintf fmt "<a %a | {%a}> :: %a" term t env e arrow_2 k
end

let rec iterate c =
  Printf.printf "%a\n" PP.config c ;
  match c with Stop x -> M.ret () | _ -> M.bind (step c) iterate

let main t = iterate (Main t)

let () =
  let ( $ ) t1 t2 = COMP (APP (t1, t2)) in
  let (-->) s t = VALUE (LAM (s, t)) in
  let v s = VALUE (VAR s) in
  let one = "f" --> ("x" --> ((v "f") $ (v "x"))) in
  let two = "f" --> ("x" --> ((v "f") $ ((v "f") $ (v "x")))) in
  let add = "n" --> ("m" --> ("f" --> ("x" --> ((v "m" $ v "f") $ ((v"n" $ v"f") $ v"x"))))) in
  let id = "x" --> v"x" in
  let term = (((add $ one) $ two) $ id) $ id in
  M.extract (main term)
