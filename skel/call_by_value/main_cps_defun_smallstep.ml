(** This file was automatically generated using necroml
	See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type _ env
  type string
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: unit -> 'a t
  val apply: ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type value =
  | VAR of string
  | LAM of (string * term)
  and term =
  | VALUE of value
  | COMP of comp
  and expval =
  | CLOSURE of (string * term * expval env)
  (** Abstract machine state *)
  and configuration =
  | Apply_2 of (arrow_2 * expval)
  | Eval of (term * expval env * arrow_2)
  | Main of term
  | Stop of expval
  and comp =
  | APP of (term * term)
  and arrow_2 =
  | Constr4 of (expval env * arrow_2 * term)
  | Constr3 of (expval env * arrow_2 * term * string)
  | Constr1

  val empty: 'a env
  val extend: string * 'a * 'a env -> ('a env) M.t
  val lookup: 'a env * string -> 'a M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type value =
  | VAR of string
  | LAM of (string * term)
  and term =
  | VALUE of value
  | COMP of comp
  and expval =
  | CLOSURE of (string * term * expval env)
  and configuration =
  | Apply_2 of (arrow_2 * expval)
  | Eval of (term * expval env * arrow_2)
  | Main of term
  | Stop of expval
  and comp =
  | APP of (term * term)
  and arrow_2 =
  | Constr4 of (expval env * arrow_2 * term)
  | Constr3 of (expval env * arrow_2 * term * string)
  | Constr1

  let extend _ = raise (NotImplemented "extend")
  let lookup _ = raise (NotImplemented "lookup")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type _ env
  type string

  type value =
  | VAR of string
  | LAM of (string * term)
  and term =
  | VALUE of value
  | COMP of comp
  and expval =
  | CLOSURE of (string * term * expval env)
  and configuration =
  | Apply_2 of (arrow_2 * expval)
  | Eval of (term * expval env * arrow_2)
  | Main of term
  | Stop of expval
  and comp =
  | APP of (term * term)
  and arrow_2 =
  | Constr4 of (expval env * arrow_2 * term)
  | Constr3 of (expval env * arrow_2 * term * string)
  | Constr1

  val empty: 'a env
  (** __atomic__ *)
  val eval_value: value * expval env -> expval M.t
  val extend: string * 'a * 'a env -> ('a env) M.t
  val lookup: 'a env * string -> 'a M.t
  val step: configuration -> configuration M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  (** __atomic__ *)
  let rec eval_value =
    function (v, e) ->
    let _tmp = v in
    begin match _tmp with
    | VAR x -> M.apply lookup (e, x)
    | LAM (x, t) -> M.ret (CLOSURE (x, t, e))
    end
  and step config =
    let _tmp = config in
    begin match _tmp with
    | Apply_2 (Constr4 (e, k, t1), CLOSURE (x, t, e_)) -> M.ret (Eval (t1, e, Constr3 (e_, k, t, x)))
    | Apply_2 (Constr3 (e_, k, t, x), w) ->
        let* e__ = M.apply extend (x, w, e_) in
        M.ret (Eval (t, e__, k))
    | Apply_2 (Constr1, x) -> M.ret (Stop x)
    | Eval (VALUE v, e, k) ->
        let* res = M.apply eval_value (v, e) in
        M.ret (Apply_2 (k, res))
    | Eval (COMP (APP (t0, t1)), e, k) -> M.ret (Eval (t0, e, Constr4 (e, k, t1)))
    | Main t -> M.ret (Eval (t, empty, Constr1))
    | Stop t -> M.ret config
    end
end
