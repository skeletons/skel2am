#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node {
  char *key;
  void *value;
  struct node *next;
};

void *skel_lookup(struct node *env, void *key) {
  while (env != NULL && strcmp((char *)key, env->key))
	env = env->next;
  return env->value;
}

void *skel_extend(char *key, void *value, struct node *env) {
  struct node *node = malloc(sizeof(struct node));
  node->next = env;
  node->key = key;
  node->value = value;
  return node;
}

void *skel_main(void *t);
struct skel_valueVAR *mk_skel_valueVAR(void *data0);
struct skel_valueLAM *mk_skel_valueLAM(void *data0, void *data1);

struct skel_termVALUE *mk_skel_termVALUE(void *data0);
struct skel_termCOMP *mk_skel_termCOMP(void *data0);

struct skel_compAPP *mk_skel_compAPP(void *data0, void *data1);

enum skel_expval { skel_CLOSURE };
struct skel_expvalCLOSURE {
  enum skel_expval tag;
  void *data0;
  void *data1;
  void *data2;
};

int main(int argc, char *argv[]) {
  /* (\x -> x) y */
  char* x = "x";
  char* y = "y";
  void *term = mk_skel_termCOMP(mk_skel_compAPP(
	  mk_skel_termVALUE(
		  mk_skel_valueLAM(x, mk_skel_termVALUE(mk_skel_valueVAR(x)))),
	  mk_skel_termVALUE(
		  mk_skel_valueLAM(y, mk_skel_termVALUE(mk_skel_valueVAR(y))))));

  struct skel_expvalCLOSURE* res = skel_main(term);

  printf("\\%s -> ...\n", (char*) res->data0);

  return 0;
}
