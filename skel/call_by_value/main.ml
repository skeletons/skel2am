(** This file was automatically generated using necroml
	See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type _ env
  type string
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: unit -> 'a t
  val apply: ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type value =
  | VAR of string
  | LAM of (string * term)
  and term =
  | VALUE of value
  | COMP of comp
  and expval =
  | CLOSURE of (string * term * expval env)
  and comp =
  | APP of (term * term)

  val empty: 'a env
  val extend: string * 'a * 'a env -> ('a env) M.t
  val lookup: 'a env * string -> 'a M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type value =
  | VAR of string
  | LAM of (string * term)
  and term =
  | VALUE of value
  | COMP of comp
  and expval =
  | CLOSURE of (string * term * expval env)
  and comp =
  | APP of (term * term)

  let extend _ = raise (NotImplemented "extend")
  let lookup _ = raise (NotImplemented "lookup")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type _ env
  type string

  type value =
  | VAR of string
  | LAM of (string * term)
  and term =
  | VALUE of value
  | COMP of comp
  and expval =
  | CLOSURE of (string * term * expval env)
  and comp =
  | APP of (term * term)

  val empty: 'a env
  val eval: term * expval env -> expval M.t
  (** __atomic__ *)
  val eval_value: value * expval env -> expval M.t
  val extend: string * 'a * 'a env -> ('a env) M.t
  val lookup: 'a env * string -> 'a M.t
  (** __main__ *)
  val main: term -> expval M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  let rec eval =
    function (t, e) ->
    let _tmp = t in
    begin match _tmp with
    | VALUE v -> M.apply eval_value (v, e)
    | COMP (APP (t0, t1)) ->
        let* CLOSURE (x, t, e_) = M.apply eval (t0, e) in
        let* w = M.apply eval (t1, e) in
        let* e__ = M.apply extend (x, w, e_) in
        M.apply eval (t, e__)
    end
  (** __atomic__ *)
  and eval_value =
    function (v, e) ->
    let _tmp = v in
    begin match _tmp with
    | VAR x -> M.apply lookup (e, x)
    | LAM (x, t) -> M.ret (CLOSURE (x, t, e))
    end
  (** __main__ *)
  and main t = M.apply eval (t, empty)
end
