#include <stdlib.h>

enum skel_value {
	skel_VAR,
	skel_LAM
};

struct skel_valueVAR {
	enum skel_value tag;
	void* data0;
};


struct skel_valueVAR* mk_skel_valueVAR(void* data0) {
	struct skel_valueVAR* res = malloc(sizeof(struct skel_valueVAR));
	res->tag = skel_VAR;
	res->data0 = data0;
	return res;
}

struct skel_valueLAM {
	enum skel_value tag;
	void* data0;
	void* data1;
};


struct skel_valueLAM* mk_skel_valueLAM(void* data0, void* data1) {
	struct skel_valueLAM* res = malloc(sizeof(struct skel_valueLAM));
	res->tag = skel_LAM;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

enum skel_term {
	skel_VALUE,
	skel_COMP
};

struct skel_termVALUE {
	enum skel_term tag;
	void* data0;
};


struct skel_termVALUE* mk_skel_termVALUE(void* data0) {
	struct skel_termVALUE* res = malloc(sizeof(struct skel_termVALUE));
	res->tag = skel_VALUE;
	res->data0 = data0;
	return res;
}

struct skel_termCOMP {
	enum skel_term tag;
	void* data0;
};


struct skel_termCOMP* mk_skel_termCOMP(void* data0) {
	struct skel_termCOMP* res = malloc(sizeof(struct skel_termCOMP));
	res->tag = skel_COMP;
	res->data0 = data0;
	return res;
}

enum skel_expval {
	skel_CLOSURE
};

struct skel_expvalCLOSURE {
	enum skel_expval tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_expvalCLOSURE* mk_skel_expvalCLOSURE(void* data0, void* data1, void* data2) {
	struct skel_expvalCLOSURE* res = malloc(sizeof(struct skel_expvalCLOSURE));
	res->tag = skel_CLOSURE;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

enum skel_comp {
	skel_APP
};

struct skel_compAPP {
	enum skel_comp tag;
	void* data0;
	void* data1;
};


struct skel_compAPP* mk_skel_compAPP(void* data0, void* data1) {
	struct skel_compAPP* res = malloc(sizeof(struct skel_compAPP));
	res->tag = skel_APP;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

enum skel_arrow_2 {
	skel_Constr4,
	skel_Constr3,
	skel_Constr1
};

struct skel_arrow_2Constr4 {
	enum skel_arrow_2 tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_arrow_2Constr4* mk_skel_arrow_2Constr4(void* data0, void* data1, void* data2) {
	struct skel_arrow_2Constr4* res = malloc(sizeof(struct skel_arrow_2Constr4));
	res->tag = skel_Constr4;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

struct skel_arrow_2Constr3 {
	enum skel_arrow_2 tag;
	void* data0;
	void* data1;
	void* data2;
	void* data3;
};


struct skel_arrow_2Constr3* mk_skel_arrow_2Constr3(void* data0, void* data1, void* data2, void* data3) {
	struct skel_arrow_2Constr3* res = malloc(sizeof(struct skel_arrow_2Constr3));
	res->tag = skel_Constr3;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	res->data3 = data3;
	return res;
}

struct skel_arrow_2Constr1 {
	enum skel_arrow_2 tag;
};


struct skel_arrow_2Constr1* mk_skel_arrow_2Constr1(void) {
	struct skel_arrow_2Constr1* res = malloc(sizeof(struct skel_arrow_2Constr1));
	res->tag = skel_Constr1;
	return res;
}

void* skel_main(void*);
void* skel_lookup(void*, void*);
void* skel_extend(void*, void*, void*);
void* skel_eval_value(void*, void*);
void* skel_eval(void*, void*, void*);
void* skel_apply_2(void*, void*);

void* skel_main(void* t) {
	return skel_eval(t, NULL, mk_skel_arrow_2Constr1());
}


void* skel_eval_value(void* v, void* e) {
	switch (*((enum skel_value*) v)) {
	case skel_LAM: {
	void* x = ((struct skel_valueLAM*) v)->data0;
	void* t = ((struct skel_valueLAM*) v)->data1;
	return mk_skel_expvalCLOSURE(x, t, e);
	}
	case skel_VAR: {
	void* x = ((struct skel_valueVAR*) v)->data0;
	return skel_lookup(e, x);
	}
}

}


void* skel_eval(void* t, void* e, void* k) {
	switch (*((enum skel_term*) t)) {
	case skel_COMP: {
	void* tmp1 = ((struct skel_termCOMP*) t)->data0;
	switch (*((enum skel_comp*) tmp1)) {
	case skel_APP: {
	void* t0 = ((struct skel_compAPP*) tmp1)->data0;
	void* t1 = ((struct skel_compAPP*) tmp1)->data1;
	return skel_eval(t0, e, mk_skel_arrow_2Constr4(e, k, t1));
	}
}

	}
	case skel_VALUE: {
	void* v = ((struct skel_termVALUE*) t)->data0;
	void* res = skel_eval_value(v, e);
	return skel_apply_2(k, res);
	}
}

}


void* skel_apply_2(void* fun_arg0, void* fun_arg1) {
	switch (*((enum skel_arrow_2*) fun_arg0)) {
	case skel_Constr1: {
	return fun_arg1;
	}
	case skel_Constr3: {
	void* e_ = ((struct skel_arrow_2Constr3*) fun_arg0)->data0;
	void* k = ((struct skel_arrow_2Constr3*) fun_arg0)->data1;
	void* t = ((struct skel_arrow_2Constr3*) fun_arg0)->data2;
	void* x = ((struct skel_arrow_2Constr3*) fun_arg0)->data3;
	void* e__ = skel_extend(x, fun_arg1, e_);
	return skel_eval(t, e__, k);
	}
	case skel_Constr4: {
	void* e = ((struct skel_arrow_2Constr4*) fun_arg0)->data0;
	void* k = ((struct skel_arrow_2Constr4*) fun_arg0)->data1;
	void* t1 = ((struct skel_arrow_2Constr4*) fun_arg0)->data2;
	switch (*((enum skel_expval*) fun_arg1)) {
	case skel_CLOSURE: {
	void* x = ((struct skel_expvalCLOSURE*) fun_arg1)->data0;
	void* t = ((struct skel_expvalCLOSURE*) fun_arg1)->data1;
	void* e_ = ((struct skel_expvalCLOSURE*) fun_arg1)->data2;
	return skel_eval(t1, e, mk_skel_arrow_2Constr3(e_, k, t, x));
	}
}

	}
}

}
