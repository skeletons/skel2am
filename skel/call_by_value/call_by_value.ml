open Necromonads
module M = ContPoly
module SMap = Map.Make (String)

module rec Types : sig
  type 'a env = 'a SMap.t

  type string = String.t
end =
  Types

let _ =
  Printf.printf "#######################################################\n" ;
  Printf.printf "#    CALL BY VALUE                                    #\n" ;
  Printf.printf "#######################################################\n"

module Test_Main = struct
  open Main

  module Spec = struct
    include Unspec (M) (Types)

    let empty = SMap.empty

    let extend (k, v, e) = M.ret @@ SMap.add k v e

    let lookup (e, k) = M.ret @@ SMap.find k e
  end

  open MakeInterpreter (Spec)

  let () =
    let term =
      COMP
        (APP
           ( VALUE (LAM ("x", VALUE (VAR "x")))
           , VALUE (LAM ("y", VALUE (VAR "y"))) ) )
    in
    let (CLOSURE (x, VALUE (VAR y), _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\%s . %s) #\n" "main" x y
end

module Test_Main_cps = struct
  open Main_cps

  module Spec = struct
    include Unspec (M) (Types)

    let empty = SMap.empty

    let extend (k, v, e) = M.ret @@ SMap.add k v e

    let lookup (e, k) = M.ret @@ SMap.find k e
  end

  open MakeInterpreter (Spec)

  let () =
    let term =
      COMP
        (APP
           ( VALUE (LAM ("x", VALUE (VAR "x")))
           , VALUE (LAM ("y", VALUE (VAR "y"))) ) )
    in
    let (CLOSURE (x, VALUE (VAR y), _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\%s . %s) #\n" "main_cps" x y
end

module Test_Main_cps_defun = struct
  open Main_cps_defun

  module Spec = struct
    include Unspec (M) (Types)

    let empty = SMap.empty

    let extend (k, v, e) = M.ret @@ SMap.add k v e

    let lookup (e, k) = M.ret @@ SMap.find k e
  end

  open MakeInterpreter (Spec)

  let () =
    let term =
      COMP
        (APP
           ( VALUE (LAM ("x", VALUE (VAR "x")))
           , VALUE (LAM ("y", VALUE (VAR "y"))) ) )
    in
    let (CLOSURE (x, VALUE (VAR y), _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\%s . %s) #\n" "main_cps_defun" x y
end

module Test_Main_cps_defun_desugared = struct
  open Main_cps_defun_desugared

  module Spec = struct
    include Unspec (M) (Types)

    let empty = SMap.empty

    let extend (k, v, e) = M.ret @@ SMap.add k v e

    let lookup (e, k) = M.ret @@ SMap.find k e
  end

  open MakeInterpreter (Spec)

  let () =
    let term =
      COMP
        (APP
           ( VALUE (LAM ("x", VALUE (VAR "x")))
           , VALUE (LAM ("y", VALUE (VAR "y"))) ) )
    in
    let (CLOSURE (x, VALUE (VAR y), _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\%s . %s) #\n"
      "main_cps_defun_desugared" x y
end

module Test_Main_cps_defun_smallstep = struct
  open Main_cps_defun_smallstep

  module Spec = struct
    include Unspec (M) (Types)

    let empty = SMap.empty

    let extend (k, v, e) = M.ret @@ SMap.add k v e

    let lookup (e, k) = M.ret @@ SMap.find k e
  end

  open MakeInterpreter (Spec)

  let rec iterate c =
    match c with Stop x -> M.ret x | _ -> M.bind (step c) iterate

  let main t = iterate (Main t)

  let () =
    let term =
      COMP
        (APP
           ( VALUE (LAM ("x", VALUE (VAR "x")))
           , VALUE (LAM ("y", VALUE (VAR "y"))) ) )
    in
    let (CLOSURE (x, VALUE (VAR y), _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\%s . %s) #\n"
      "main_cps_defun_smallstep" x y
end

module Test_Main_cps_defun_desugared_refun = struct
  open Main_cps_defun_desugared_refun

  module Spec = struct
    include Unspec (M) (Types)

    let empty = SMap.empty

    let extend (k, v, e) = M.ret @@ SMap.add k v e

    let lookup (e, k) = M.ret @@ SMap.find k e
  end

  open MakeInterpreter (Spec)

  let () =
    let term =
      COMP
        (APP
           ( VALUE (LAM ("x", VALUE (VAR "x")))
           , VALUE (LAM ("y", VALUE (VAR "y"))) ) )
    in
    let (CLOSURE (x, VALUE (VAR y), _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\%s . %s) #\n"
      "main_cps_defun_desugared_refun" x y
end

let _ = Printf.printf "#######################################################\n"
