
type int

type list<_>

val nil<a> : list<a>

val cons<a> : (a, list<a>) → list<a>

val nth<a> : (list<a>, int) → a

(** __protected__ *)
type term =
| IND int
| ABS term
| APP (term, term)

(** __protected__ *)
type denval =
| THUNK (term, list<denval>)

(** __protected__ *)
type expval =
| FUNCT (term, list<denval>)

val eval ((t: term), (e: list<denval>), (k: expval → expval)): expval =
	match t with
	| IND n →
		let THUNK (t, e') = nth<denval> (e, n) in
		eval (t, e', k)
	| ABS t' → k (FUNCT (t', e))
	| APP (t0, t1) →
		eval (t0, e, λ FUNCT (t', e') : expval →
			let e'' = cons<denval> (THUNK (t1, e), e') in
			eval (t', e'', k))
	end

(** __main__ *)
val main (t: term): expval =
	eval (t, nil<denval>, λ x : expval → x)

