(** This file was automatically generated using necroml
	See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type int
  type _ list
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: unit -> 'a t
  val apply: ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type term =
  | IND of int
  | ABS of term
  | APP of (term * term)
  and expval =
  | FUNCT of (term * denval list)
  and denval =
  | THUNK of (term * denval list)

  val cons: 'a * 'a list -> ('a list) M.t
  val nil: 'a list
  val nth: 'a list * int -> 'a M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type term =
  | IND of int
  | ABS of term
  | APP of (term * term)
  and expval =
  | FUNCT of (term * denval list)
  and denval =
  | THUNK of (term * denval list)

  let cons _ = raise (NotImplemented "cons")
  let nth _ = raise (NotImplemented "nth")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type int
  type _ list

  type term =
  | IND of int
  | ABS of term
  | APP of (term * term)
  and expval =
  | FUNCT of (term * denval list)
  and denval =
  | THUNK of (term * denval list)

  val cons: 'a * 'a list -> ('a list) M.t
  val eval: term * denval list -> expval M.t
  (** __main__ *)
  val main: term -> expval M.t
  val nil: 'a list
  val nth: 'a list * int -> 'a M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  let rec eval =
    function (t, e) ->
    let _tmp = t in
    begin match _tmp with
    | IND n ->
        let* THUNK (t, e') = M.apply nth (e, n) in
        M.apply eval (t, e')
    | ABS t' -> M.ret (FUNCT (t', e))
    | APP (t0, t1) ->
        let* FUNCT (t', e') = M.apply eval (t0, e) in
        let* e'' = M.apply cons (THUNK (t1, e), e') in
        M.apply eval (t', e'')
    end
  (** __main__ *)
  and main t = M.apply eval (t, nil)
end
