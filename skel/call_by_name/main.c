#include <stdlib.h>
#include <stdio.h>

struct node {
  void *value;
  struct node *next;
};

void *skel_cons(void *head, void *tail) {
  struct node *res = malloc(sizeof(struct node));
  res->value = head;
  res->next = tail;
  return res;
}

void *skel_nth(struct node *l, void *_n) {
  long n = (long)_n;
  while (n > 0) {
	l = l->next;
	n--;
  }
  return l->value;
}


enum skel_expval { skel_FUNCT };
struct skel_expvalFUNCT {
  enum skel_expval tag;
  void *data0;
  void *data1;
};

enum skel_term { skel_IND, skel_ABS, skel_APP };
struct skel_termABS {
  enum skel_term tag;
  void *data0;
};

struct skel_termIND *mk_skel_termIND(void *data0);
struct skel_termABS *mk_skel_termABS(void *data0);
struct skel_termAPP *mk_skel_termAPP(void *data0, void *data1);
void *skel_main(void *);

int main(int argc, char *argv[]) {
  void *term = mk_skel_termAPP(mk_skel_termABS(mk_skel_termIND(0)),
							   mk_skel_termABS(mk_skel_termIND((void*) 42)));

  struct skel_expvalFUNCT *res = skel_main(term);

  struct skel_termABS* ind = res->data0;
  long n = (long) ind->data0;


  printf("42 = %ld\n", n);
  return 0;
}
