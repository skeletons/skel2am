(** This file was automatically generated using necroml
	See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type int
  type _ list
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: unit -> 'a t
  val apply: ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type term =
  | IND of int
  | ABS of term
  | APP of (term * term)
  and expval =
  | FUNCT of (term * denval list)
  and denval =
  | THUNK of (term * denval list)
  (** Abstract machine state *)
  and configuration =
  | Apply_2 of (arrow_2 * expval)
  | Eval of (term * denval list * arrow_2)
  | Main of term
  | Stop of expval
  and arrow_2 =
  | Constr3 of (denval list * arrow_2 * term)
  | Constr1

  val cons: 'a * 'a list -> ('a list) M.t
  val nil: 'a list
  val nth: 'a list * int -> 'a M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type term =
  | IND of int
  | ABS of term
  | APP of (term * term)
  and expval =
  | FUNCT of (term * denval list)
  and denval =
  | THUNK of (term * denval list)
  and configuration =
  | Apply_2 of (arrow_2 * expval)
  | Eval of (term * denval list * arrow_2)
  | Main of term
  | Stop of expval
  and arrow_2 =
  | Constr3 of (denval list * arrow_2 * term)
  | Constr1

  let cons _ = raise (NotImplemented "cons")
  let nth _ = raise (NotImplemented "nth")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type int
  type _ list

  type term =
  | IND of int
  | ABS of term
  | APP of (term * term)
  and expval =
  | FUNCT of (term * denval list)
  and denval =
  | THUNK of (term * denval list)
  and configuration =
  | Apply_2 of (arrow_2 * expval)
  | Eval of (term * denval list * arrow_2)
  | Main of term
  | Stop of expval
  and arrow_2 =
  | Constr3 of (denval list * arrow_2 * term)
  | Constr1

  val cons: 'a * 'a list -> ('a list) M.t
  val nil: 'a list
  val nth: 'a list * int -> 'a M.t
  val step: configuration -> configuration M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  let rec step config =
    let _tmp = config in
    begin match _tmp with
    | Apply_2 (Constr3 (e, k, t1), FUNCT (t', e')) ->
        let* e'' = M.apply cons (THUNK (t1, e), e') in
        M.ret (Eval (t', e'', k))
    | Apply_2 (Constr1, x) -> M.ret (Stop x)
    | Eval (IND n, e, k) ->
        let* THUNK (t, e') = M.apply nth (e, n) in
        M.ret (Eval (t, e', k))
    | Eval (ABS t', e, k) -> M.ret (Apply_2 (k, FUNCT (t', e)))
    | Eval (APP (t0, t1), e, k) -> M.ret (Eval (t0, e, Constr3 (e, k, t1)))
    | Main t -> M.ret (Eval (t, nil, Constr1))
    | Stop t -> M.ret config
    end
end
