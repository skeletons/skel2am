open Necromonads
module M = ContPoly
module SMap = Map.Make (String)

module rec Types : sig
  type int = Int.t

  type 'a list = 'a List.t
end =
  Types

let _ =
  Printf.printf "#######################################################\n" ;
  Printf.printf "#    CALL BY NAME                                     #\n" ;
  Printf.printf "#######################################################\n"

module Test_Main = struct
  open Main

  module Spec = struct
    include Unspec (M) (Types)

    let nil = []

    let cons (hd, tl) = M.ret @@ (hd :: tl)

    let nth (l, n) = M.ret @@ Stdlib.List.nth l n
  end

  open MakeInterpreter (Spec)

  let () =
    let term = APP (ABS (IND 0), ABS (IND 0)) in
    let (FUNCT (IND n, _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\0 . %d) #\n" "main" n
end

module Test_Main_cps = struct
  open Main_cps

  module Spec = struct
    include Unspec (M) (Types)

    let nil = []

    let cons (hd, tl) = M.ret @@ (hd :: tl)

    let nth (l, n) = M.ret @@ Stdlib.List.nth l n
  end

  open MakeInterpreter (Spec)

  let () =
    let term = APP (ABS (IND 0), ABS (IND 0)) in
    let (FUNCT (IND n, _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\0 . %d) #\n" "main_cps" n
end

module Test_Main_cps_defun = struct
  open Main_cps_defun

  module Spec = struct
    include Unspec (M) (Types)

    let nil = []

    let cons (hd, tl) = M.ret @@ (hd :: tl)

    let nth (l, n) = M.ret @@ Stdlib.List.nth l n
  end

  open MakeInterpreter (Spec)

  let () =
    let term = APP (ABS (IND 0), ABS (IND 0)) in
    let (FUNCT (IND n, _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\0 . %d) #\n" "main_cps_defun" n
end

module Test_Main_cps_defun_desugared = struct
  open Main_cps_defun_desugared

  module Spec = struct
    include Unspec (M) (Types)

    let nil = []

    let cons (hd, tl) = M.ret @@ (hd :: tl)

    let nth (l, n) = M.ret @@ Stdlib.List.nth l n
  end

  open MakeInterpreter (Spec)

  let () =
    let term = APP (ABS (IND 0), ABS (IND 0)) in
    let (FUNCT (IND n, _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\0 . %d) #\n"
      "main_cps_defun_desugared" n
end

module Test_Main_cps_defun_smallstep = struct
  open Main_cps_defun_smallstep

  module Spec = struct
    include Unspec (M) (Types)

    let nil = []

    let cons (hd, tl) = M.ret @@ (hd :: tl)

    let nth (l, n) = M.ret @@ Stdlib.List.nth l n
  end

  open MakeInterpreter (Spec)

  let rec iterate c =
    match c with Stop x -> M.ret x | _ -> M.bind (step c) iterate

  let main t = iterate (Main t)

  let () =
    let term = APP (ABS (IND 0), ABS (IND 0)) in
    let (FUNCT (IND n, _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\0 . %d) #\n"
      "main_cps_defun_smallstep" n
end

module Test_Main_cps_defun_desugared_refun = struct
  open Main_cps_defun_desugared_refun

  module Spec = struct
    include Unspec (M) (Types)

    let nil = []

    let cons (hd, tl) = M.ret @@ (hd :: tl)

    let nth (l, n) = M.ret @@ Stdlib.List.nth l n
  end

  open MakeInterpreter (Spec)

  let () =
    let term = APP (ABS (IND 0), ABS (IND 0)) in
    let (FUNCT (IND n, _)) = M.extract (main term) in
    Printf.printf "# %30s: (\\y . y) = (\\0 . %d) #\n"
      "main_cps_defun_desugared_refun" n
end

let _ = Printf.printf "#######################################################\n"
