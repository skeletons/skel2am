#include <stdlib.h>

enum skel_term {
	skel_IND,
	skel_ABS,
	skel_APP
};

struct skel_termIND {
	enum skel_term tag;
	void* data0;
};


struct skel_termIND* mk_skel_termIND(void* data0) {
	struct skel_termIND* res = malloc(sizeof(struct skel_termIND));
	res->tag = skel_IND;
	res->data0 = data0;
	return res;
}

struct skel_termABS {
	enum skel_term tag;
	void* data0;
};


struct skel_termABS* mk_skel_termABS(void* data0) {
	struct skel_termABS* res = malloc(sizeof(struct skel_termABS));
	res->tag = skel_ABS;
	res->data0 = data0;
	return res;
}

struct skel_termAPP {
	enum skel_term tag;
	void* data0;
	void* data1;
};


struct skel_termAPP* mk_skel_termAPP(void* data0, void* data1) {
	struct skel_termAPP* res = malloc(sizeof(struct skel_termAPP));
	res->tag = skel_APP;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

enum skel_expval {
	skel_FUNCT
};

struct skel_expvalFUNCT {
	enum skel_expval tag;
	void* data0;
	void* data1;
};


struct skel_expvalFUNCT* mk_skel_expvalFUNCT(void* data0, void* data1) {
	struct skel_expvalFUNCT* res = malloc(sizeof(struct skel_expvalFUNCT));
	res->tag = skel_FUNCT;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

enum skel_denval {
	skel_THUNK
};

struct skel_denvalTHUNK {
	enum skel_denval tag;
	void* data0;
	void* data1;
};


struct skel_denvalTHUNK* mk_skel_denvalTHUNK(void* data0, void* data1) {
	struct skel_denvalTHUNK* res = malloc(sizeof(struct skel_denvalTHUNK));
	res->tag = skel_THUNK;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

enum skel_arrow_2 {
	skel_Constr3,
	skel_Constr1
};

struct skel_arrow_2Constr3 {
	enum skel_arrow_2 tag;
	void* data0;
	void* data1;
	void* data2;
};


struct skel_arrow_2Constr3* mk_skel_arrow_2Constr3(void* data0, void* data1, void* data2) {
	struct skel_arrow_2Constr3* res = malloc(sizeof(struct skel_arrow_2Constr3));
	res->tag = skel_Constr3;
	res->data0 = data0;
	res->data1 = data1;
	res->data2 = data2;
	return res;
}

struct skel_arrow_2Constr1 {
	enum skel_arrow_2 tag;
};


struct skel_arrow_2Constr1* mk_skel_arrow_2Constr1(void) {
	struct skel_arrow_2Constr1* res = malloc(sizeof(struct skel_arrow_2Constr1));
	res->tag = skel_Constr1;
	return res;
}

void* skel_nth(void*, void*);
void* skel_main(void*);
void* skel_eval(void*, void*, void*);
void* skel_cons(void*, void*);
void* skel_apply_2(void*, void*);

void* skel_main(void* t) {
	return skel_eval(t, NULL, mk_skel_arrow_2Constr1());
}


void* skel_eval(void* t, void* e, void* k) {
	switch (*((enum skel_term*) t)) {
	case skel_ABS: {
	void* t' = ((struct skel_termABS*) t)->data0;
	return skel_apply_2(k, mk_skel_expvalFUNCT(t', e));
	}
	case skel_APP: {
	void* t0 = ((struct skel_termAPP*) t)->data0;
	void* t1 = ((struct skel_termAPP*) t)->data1;
	return skel_eval(t0, e, mk_skel_arrow_2Constr3(e, k, t1));
	}
	case skel_IND: {
	void* n = ((struct skel_termIND*) t)->data0;
	void* tmp = skel_nth(e, n);
	void* t = ((struct skel_denvalTHUNK*) tmp)->data0;
	void* e' = ((struct skel_denvalTHUNK*) tmp)->data1;
	return skel_eval(t, e', k);
	}
}

}


void* skel_apply_2(void* fun_arg0, void* fun_arg1) {
	switch (*((enum skel_arrow_2*) fun_arg0)) {
	case skel_Constr1: {
	return fun_arg1;
	}
	case skel_Constr3: {
	void* e = ((struct skel_arrow_2Constr3*) fun_arg0)->data0;
	void* k = ((struct skel_arrow_2Constr3*) fun_arg0)->data1;
	void* t1 = ((struct skel_arrow_2Constr3*) fun_arg0)->data2;
	switch (*((enum skel_expval*) fun_arg1)) {
	case skel_FUNCT: {
	void* t' = ((struct skel_expvalFUNCT*) fun_arg1)->data0;
	void* e' = ((struct skel_expvalFUNCT*) fun_arg1)->data1;
	void* e'' = skel_cons(mk_skel_denvalTHUNK(t1, e), e');
	return skel_eval(t', e'', k);
	}
}

	}
}

}
