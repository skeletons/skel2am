(** This file was automatically generated using necroml
	See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type int
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: unit -> 'a t
  val apply: ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type term =
  | Int of int
  | Add of (term * term)
  (** Abstract machine state *)
  and configuration =
  | Apply_2 of (arrow_2 * int)
  | Eval of (term * arrow_2)
  | Main of term
  | Stop of int
  and arrow_2 =
  | Constr4 of (arrow_2 * term)
  | Constr3 of (arrow_2 * int)
  | Constr1

  val add: int * int -> int M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type term =
  | Int of int
  | Add of (term * term)
  and configuration =
  | Apply_2 of (arrow_2 * int)
  | Eval of (term * arrow_2)
  | Main of term
  | Stop of int
  and arrow_2 =
  | Constr4 of (arrow_2 * term)
  | Constr3 of (arrow_2 * int)
  | Constr1

  let add _ = raise (NotImplemented "add")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type int

  type term =
  | Int of int
  | Add of (term * term)
  and configuration =
  | Apply_2 of (arrow_2 * int)
  | Eval of (term * arrow_2)
  | Main of term
  | Stop of int
  and arrow_2 =
  | Constr4 of (arrow_2 * term)
  | Constr3 of (arrow_2 * int)
  | Constr1

  val add: int * int -> int M.t
  val step: configuration -> configuration M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  let rec step config =
    let _tmp = config in
    begin match _tmp with
    | Apply_2 (Constr4 (k, t2), n1) -> M.ret (Eval (t2, Constr3 (k, n1)))
    | Apply_2 (Constr3 (k, n1), n2) ->
        let* res = M.apply add (n1, n2) in
        M.ret (Apply_2 (k, res))
    | Apply_2 (Constr1, x) -> M.ret (Stop x)
    | Eval (Int n, k) -> M.ret (Apply_2 (k, n))
    | Eval (Add (t1, t2), k) -> M.ret (Eval (t1, Constr4 (k, t2)))
    | Main term -> M.ret (Eval (term, Constr1))
    | Stop term -> M.ret config
    end
end
