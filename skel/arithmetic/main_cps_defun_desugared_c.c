#include <stdlib.h>

enum skel_term {
	skel_Int,
	skel_Add
};

struct skel_termInt {
	enum skel_term tag;
	void* data0;
};


struct skel_termInt* mk_skel_termInt(void* data0) {
	struct skel_termInt* res = malloc(sizeof(struct skel_termInt));
	res->tag = skel_Int;
	res->data0 = data0;
	return res;
}

struct skel_termAdd {
	enum skel_term tag;
	void* data0;
	void* data1;
};


struct skel_termAdd* mk_skel_termAdd(void* data0, void* data1) {
	struct skel_termAdd* res = malloc(sizeof(struct skel_termAdd));
	res->tag = skel_Add;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

enum skel_arrow_2 {
	skel_Constr4,
	skel_Constr3,
	skel_Constr1
};

struct skel_arrow_2Constr4 {
	enum skel_arrow_2 tag;
	void* data0;
	void* data1;
};


struct skel_arrow_2Constr4* mk_skel_arrow_2Constr4(void* data0, void* data1) {
	struct skel_arrow_2Constr4* res = malloc(sizeof(struct skel_arrow_2Constr4));
	res->tag = skel_Constr4;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

struct skel_arrow_2Constr3 {
	enum skel_arrow_2 tag;
	void* data0;
	void* data1;
};


struct skel_arrow_2Constr3* mk_skel_arrow_2Constr3(void* data0, void* data1) {
	struct skel_arrow_2Constr3* res = malloc(sizeof(struct skel_arrow_2Constr3));
	res->tag = skel_Constr3;
	res->data0 = data0;
	res->data1 = data1;
	return res;
}

struct skel_arrow_2Constr1 {
	enum skel_arrow_2 tag;
};


struct skel_arrow_2Constr1* mk_skel_arrow_2Constr1(void) {
	struct skel_arrow_2Constr1* res = malloc(sizeof(struct skel_arrow_2Constr1));
	res->tag = skel_Constr1;
	return res;
}

void* skel_main(void*);
void* skel_eval(void*, void*);
void* skel_apply_2(void*, void*);
void* skel_add(void*, void*);

void* skel_main(void* term) {
	return skel_eval(term, mk_skel_arrow_2Constr1());
}


void* skel_eval(void* term, void* k) {
	switch (*((enum skel_term*) term)) {
	case skel_Add: {
	void* t1 = ((struct skel_termAdd*) term)->data0;
	void* t2 = ((struct skel_termAdd*) term)->data1;
	return skel_eval(t1, mk_skel_arrow_2Constr4(k, t2));
	}
	case skel_Int: {
	void* n = ((struct skel_termInt*) term)->data0;
	return skel_apply_2(k, n);
	}
}

}


void* skel_apply_2(void* fun_arg0, void* fun_arg1) {
	switch (*((enum skel_arrow_2*) fun_arg0)) {
	case skel_Constr1: {
	return fun_arg1;
	}
	case skel_Constr3: {
	void* k = ((struct skel_arrow_2Constr3*) fun_arg0)->data0;
	void* n1 = ((struct skel_arrow_2Constr3*) fun_arg0)->data1;
	void* res = skel_add(n1, fun_arg1);
	return skel_apply_2(k, res);
	}
	case skel_Constr4: {
	void* k = ((struct skel_arrow_2Constr4*) fun_arg0)->data0;
	void* t2 = ((struct skel_arrow_2Constr4*) fun_arg0)->data1;
	return skel_eval(t2, mk_skel_arrow_2Constr3(k, fun_arg1));
	}
}

}
