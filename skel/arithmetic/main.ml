(** This file was automatically generated using necroml
	See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type int
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: unit -> 'a t
  val apply: ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type term =
  | Int of int
  | Add of (term * term)

  val add: int * int -> int M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type term =
  | Int of int
  | Add of (term * term)

  let add _ = raise (NotImplemented "add")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type int

  type term =
  | Int of int
  | Add of (term * term)

  val add: int * int -> int M.t
  val eval: term -> int M.t
  (** __main__ *)
  val main: term -> int M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  let rec eval term =
    let _tmp = term in
    begin match _tmp with
    | Int n -> M.ret n
    | Add (t1, t2) ->
        let* n1 = M.apply eval t1 in
        let* n2 = M.apply eval t2 in
        M.apply add (n1, n2)
    end
  (** __main__ *)
  and main term = M.apply eval term
end
