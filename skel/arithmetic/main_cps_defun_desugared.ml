(** This file was automatically generated using necroml
	See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type int
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: unit -> 'a t
  val apply: ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type term =
  | Int of int
  | Add of (term * term)
  and arrow_2 =
  | Constr4 of (arrow_2 * term)
  | Constr3 of (arrow_2 * int)
  | Constr1

  val add: int * int -> int M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type term =
  | Int of int
  | Add of (term * term)
  and arrow_2 =
  | Constr4 of (arrow_2 * term)
  | Constr3 of (arrow_2 * int)
  | Constr1

  let add _ = raise (NotImplemented "add")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type int

  type term =
  | Int of int
  | Add of (term * term)
  and arrow_2 =
  | Constr4 of (arrow_2 * term)
  | Constr3 of (arrow_2 * int)
  | Constr1

  val add: int * int -> int M.t
  val apply_2: arrow_2 * int -> int M.t
  val eval: term * arrow_2 -> int M.t
  (** __main__ *)
  val main: term -> int M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  let rec apply_2 =
    function (fun_arg0, fun_arg1) ->
    let _tmp = fun_arg0 in
    begin match _tmp with
    | Constr1 -> M.ret fun_arg1
    | Constr3 (k, n1) ->
        let* res = M.apply add (n1, fun_arg1) in
        M.apply apply_2 (k, res)
    | Constr4 (k, t2) -> M.apply eval (t2, Constr3 (k, fun_arg1))
    end
  and eval =
    function (term, k) ->
    let _tmp = term in
    begin match _tmp with
    | Add (t1, t2) -> M.apply eval (t1, Constr4 (k, t2))
    | Int n -> M.apply apply_2 (k, n)
    end
  (** __main__ *)
  and main term = M.apply eval (term, Constr1)
end
