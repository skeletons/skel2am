open Main_cps_defun_smallstep_bis
open Necromonads
module M = ContPoly

module rec Types : sig
  type int = Int.t
end =
  Types

module Spec = struct
  include Unspec (M) (Types)

  let add (x, y) = M.ret @@ (x + y)
end

open MakeInterpreter (Spec)

module PP = struct
  let rec config fmt c =
    match c with
    | Apply_2 (k, n) ->
        Printf.fprintf fmt "Apply_2: %d\n  %a\n" n arrow_2 k
    | Eval (t, k) ->
        Printf.fprintf fmt "Eval: <%a>\n  %a\n" term t arrow_2 k
    | Main t ->
        Printf.fprintf fmt "Main: <%a>\n" term t
    | Stop n ->
        Printf.fprintf fmt "Stop: %d" n

  and term fmt t =
    match t with
    | Int n -> Printf.fprintf fmt "%d" n
    | Add (t1, t2) -> Printf.fprintf fmt "(%a + %a)" term t1 term t2

  and arrow_2 fmt k =
    match k with
    | Constr1 -> Printf.fprintf fmt "[]"
    | Constr3 (k, n) ->
      Printf.fprintf fmt "%d :: %a" n arrow_2 k
    | Constr4 (k, t) ->
      Printf.fprintf fmt "<%a> :: %a" term t arrow_2 k
end

let rec iterate c =
  Printf.printf "%a\n" PP.config c ;
  match c with Stop x -> M.ret () | _ -> M.bind (step c) iterate

let main t = iterate (Main t)

let () =
  let (+) l r = Add (l, r) in
  let (<+) l r = Add (Int l, r) in
  let (<+>) l r = Add (Int l, Int r) in
  let (+>) l r = Add (l, Int r) in
  let _term = (1 <+> 2) + (3 <+ (4 <+> 5)) +> 6 in
  let term = (1 <+> 2) +> 3 in
  M.extract (main term)
