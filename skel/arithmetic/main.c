#include <stdio.h>
#include <stdlib.h>

void *skel_main(void *);

void* skel_add(void* left, void* right) {
  return (void*) (((long) left) + ((long) right));
}

struct skel_termInt *mk_skel_termInt(void *data0);
struct skel_termAdd *mk_skel_termAdd(void *data0, void *data1);

void* some_big_term(int n) {
  if (n == 0) {
    void *one = (void *)1;
    return mk_skel_termInt(one);
  } else {
    void* subterm = some_big_term(n-1);
    return mk_skel_termAdd(subterm, subterm);
  }
}

int main(int argc, char *argv[]) {
  void *one = (void *)1;
  void *two = (void *)2;
  void *three = (void *)3;
  struct skel_termAdd *term = mk_skel_termAdd(
      mk_skel_termAdd(mk_skel_termInt(one), mk_skel_termInt(two)),
      mk_skel_termInt(three));

  void* res = skel_main(term);
  long result = (long) res;
  printf("1 + 2 + 3 = %ld\n", result);
  return 0;
}
