open Necromonads
module M = ContPoly

let print_result = Printf.printf "# %30s: 1 + 2 + 3 = %d #\n"

module rec Types : sig
  type int = Int.t
end =
  Types

let _ =
  Printf.printf "#################################################\n" ;
  Printf.printf "#    ARITHMETIC                                 #\n" ;
  Printf.printf "#################################################\n"

module Test_Main = struct
  open Main

  module Spec = struct
    include Unspec (M) (Types)

    let add (x, y) = M.ret @@ (x + y)
  end

  open MakeInterpreter (Spec)

  let () =
    let term = Add (Add (Int 1, Int 2), Int 3) in
    print_result "main" @@ M.extract (main term)
end

module Test_Main_cps = struct
  open Main_cps

  module Spec = struct
    include Unspec (M) (Types)

    let add (x, y) = M.ret @@ (x + y)
  end

  open MakeInterpreter (Spec)

  let () =
    let term = Add (Add (Int 1, Int 2), Int 3) in
    print_result "main_cps" @@ M.extract (main term)
end

module Test_Main_cps_defun = struct
  open Main_cps_defun

  module Spec = struct
    include Unspec (M) (Types)

    let add (x, y) = M.ret @@ (x + y)
  end

  open MakeInterpreter (Spec)

  let () =
    let term = Add (Add (Int 1, Int 2), Int 3) in
    print_result "main_cps_defun" @@ M.extract (main term)
end

module Test_Main_cps_defun_desugared = struct
  open Main_cps_defun_desugared

  module Spec = struct
    include Unspec (M) (Types)

    let add (x, y) = M.ret @@ (x + y)
  end

  open MakeInterpreter (Spec)

  let () =
    let term = Add (Add (Int 1, Int 2), Int 3) in
    print_result "main_cps_defun_desugared" @@ M.extract (main term)
end

module Test_Main_cps_defun_smallstep = struct
  open Main_cps_defun_smallstep

  module Spec = struct
    include Unspec (M) (Types)

    let add (x, y) = M.ret @@ (x + y)
  end

  open MakeInterpreter (Spec)

  let rec iterate c =
    match c with Stop x -> M.ret x | _ -> M.bind (step c) iterate

  let main t = iterate (Main t)

  let () =
    let term = Add (Add (Int 1, Int 2), Int 3) in
    print_result "main_cps_defun_smallstep" @@ M.extract (main term)
end

module Test_Main_cps_defun_desugared_refun = struct
  open Main_cps_defun_desugared_refun

  module Spec = struct
    include Unspec (M) (Types)

    let add (x, y) = M.ret @@ (x + y)
  end

  open MakeInterpreter (Spec)

  let () =
    let term = Add (Add (Int 1, Int 2), Int 3) in
    print_result "main_cps_defun_desugared_refun" @@ M.extract (main term)
end

let _ = Printf.printf "#################################################\n"
