(** This file was automatically generated using necroml version 0.3.5.
See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations. *)

(** The unspecified types *)
module type TYPES = sig
  type env
  type var
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: unit -> 'a t
  val apply: ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type value =
  | Clos of (var * term * env)
  and term =
  | Var of var
  | Abs of (var * term)
  | App of (term * term)
  and 'a m = env -> 'a M.t

  val empty: env
  val insert: env * var * value -> env M.t
  val lookup: env * var -> value M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type value =
  | Clos of (var * term * env)
  and term =
  | Var of var
  | Abs of (var * term)
  | App of (term * term)
  and 'a m = env -> 'a M.t

  let insert _ = raise (NotImplemented "insert")
  let lookup _ = raise (NotImplemented "lookup")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type env
  type var

  type value =
  | Clos of (var * term * env)
  and term =
  | Var of var
  | Abs of (var * term)
  | App of (term * term)
  and 'a m = env -> 'a M.t

  (** __effect__ **)
  val ask: unit * (env -> ('a m) M.t) -> ('a m) M.t
  (** __bind__ **)
  val bind: 'a m * ('a -> ('b m) M.t) -> ('b m) M.t
  val empty: env
  val eval: term -> (value m) M.t
  val insert: env * var * value -> env M.t
  (** __effect__ **)
  val local: env * (unit -> ('a m) M.t) -> ('a m) M.t
  val lookup: env * var -> value M.t
  (** __main__ *)
  val main: term -> value M.t
  (** __return__ **)
  val return: 'a -> ('a m) M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  (** __return__ **)
  let return: 'a. 'a -> ('a m) M.t  =
    function x ->
    M.ret (function _ ->
      M.ret x)

  (** __effect__ **)
  let local: 'a. env * (unit -> ('a m) M.t) -> ('a m) M.t  =
    function (e', k) ->
    M.ret (function _ ->
      let* k_ = M.apply k () in
      M.apply k_ e')

  (** __bind__ **)
  let bind: 'a 'b. 'a m * ('a -> ('b m) M.t) -> ('b m) M.t  =
    function (w, k) ->
    M.ret (function e ->
      let* we = M.apply w e in
      let* kwe = M.apply k we in
      M.apply kwe e)

  (** __effect__ **)
  let ask: 'a. unit * (env -> ('a m) M.t) -> ('a m) M.t  =
    function ((), k) ->
    M.ret (function e ->
      let* ke = M.apply k e in
      M.apply ke e)

  let rec eval t =
    begin match t with
    | Var x ->
        ask ((), (function e ->
          let* v = M.apply lookup (e, x) in
          M.apply return v))
    | Abs (x, t) ->
        ask ((), (function e ->
          M.apply return (Clos (x, t, e))))
    | App (t1, t2) ->
        let* _tmp = M.apply eval t1 in
        bind (_tmp, (function | (Clos (x, t, e')) ->
          let* _tmp = M.apply eval t2 in
          bind (_tmp, (function v ->
            let* e = M.apply insert (e', x, v) in
            local (e, (function () ->
              M.apply eval t))))
          ))
    end

  (** __main__ *)
  let main t =
    let* w = M.apply eval t in
    M.apply w empty
end
