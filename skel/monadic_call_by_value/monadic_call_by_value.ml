module M = Necromonads.ID
module SMap = Map.Make (String)

module rec Types : sig
  type var = string

  type env = Main.Unspec(M)(Types).value SMap.t
end =
  Types

module Printer = struct
  include Main.Unspec (M) (Types)

  let pair pp1 pp2 fmt (l, r) = Printf.fprintf fmt "(%a, %a)" pp1 l pp2 r

  let string fmt s = Printf.fprintf fmt "%s" s

  let rec many pp fmt xs =
    match xs with
    | [] ->
        ()
    | [x] ->
        pp fmt x
    | x :: xs ->
        Printf.fprintf fmt "%a, %a" pp x (many pp) xs

  let list pp fmt xs = Printf.fprintf fmt "[%a]" (many pp) xs

  let rec value fmt (Clos (x, t, e)) =
    Printf.fprintf fmt "{%s, %a, %a}" x term t env e

  and term fmt t =
    match t with
    | Var x ->
        Printf.fprintf fmt "%s" x
    | Abs (x, t) ->
        Printf.fprintf fmt "(λ %s . %a)" x term t
    | App (t1, t2) ->
        Printf.fprintf fmt "(@ %a %a)" term t1 term t2

  and env fmt e = list (pair string value) fmt (SMap.to_list e)
end

module Unspec = struct
  include Main.Unspec (M) (Types)

  let insert (e, x, v) = M.ret @@ SMap.add x v e

  let lookup (e, x) = M.ret @@ SMap.find x e

  let empty = SMap.empty
end

module Interpreter = Main.MakeInterpreter (Unspec)
open Interpreter

let tid x = Abs (x, Var x)

let t = App (App (tid "x", tid "y"), App (Abs ("x", tid "z"), tid "t"))

let monadic () =
  let v = eval t SMap.empty in
  Printf.printf "%a\n" Printer.value v

let () = monadic ()
